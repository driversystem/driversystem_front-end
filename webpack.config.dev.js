const path = require('path'); // lấy đường dẫn tuyệt đối của thư mục
const webpack = require('webpack');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const env = require('./settings/development.json');
const envKeys = Object.keys(env).reduce((prev, next) => {
	prev[`process.env.${next}`] = JSON.stringify(env[next]);
	return prev;
}, {});

const config = {
	mode: 'development',
	devtool: 'cheap-module-eval-source-map',
  entry: [
		'babel-polyfill',
		'webpack-hot-middleware/client',
    path.resolve(__dirname, 'src'),
  ],
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'bundle.min.js',
    publicPath: '/',
  },
  plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.DefinePlugin(envKeys),
		new BundleAnalyzerPlugin({
			openAnalyzer: true,
			analyzerPort: 3000,
		}),
		new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /ja|it/),
	],
	optimization: {
		noEmitOnErrors: true,
		minimizer: [
			new UglifyJsPlugin({
				cache: true,
				parallel: true,
				uglifyOptions: {
					compress: false,
					ecma: 6,
					mangle: true,
				},
			}),
		],
	},
  module: {
    rules: [
      {
				test: /\.js$/,
				use: 'babel-loader',
				include: [
					path.resolve(__dirname, 'src'),
				]
			},
			{
        test: /\.((c|sa|sc)ss)$/,
        use: [
					'style-loader',
					'css-loader',
					// {
					// 	loader: 'postcss-loader',
					// 	options: {
					// 		ident: 'postcss',
					// 		plugins: loader => [
					// 			require('postcss-import')({ root: loader.resourcePath }),
					// 			require('postcss-preset-env')(),
					// 			require('cssnano')()
					// 		]
					// 	},
					// },
					'sass-loader',
				],
      },
      {
				test: /\.(png|jpg|gif)$/,
				use: [
					{
						loader: 'file-loader',
						options: {}
					}
				]
      },
      {
				test: /\.svg$/,
				use: [
					'svg-inline-loader'
				]
      },
      {
				test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
				loader: 'url-loader?limit=100000',
				options: {
					name: '[name].[ext]',
					outputPath: 'fonts/'
				}
			},
    ],
	},
	resolve: {
		alias: {
			'@fortawesome/fontawesome-free$': '@fortawesome/fontawesome-free-solid/shakable.es.js',
			src: path.resolve(__dirname, 'src/'),
			assets: path.resolve(__dirname, 'assets/'),
			components: path.resolve(__dirname, 'src/components/'),
			pages: path.resolve(__dirname, 'src/pages'),
			screens: path.resolve(__dirname, 'src/screens'),
			constants: path.resolve(__dirname, 'src/constants/'),
			helpers: path.resolve(__dirname, 'src/helpers/'),
			actions: path.resolve(__dirname, 'src/redux/actions/'),
			reducers: path.resolve(__dirname, 'src/redux/reducers/'),
			selectors: path.resolve(__dirname, 'src/redux/selectors/'),
			utils: path.resolve(__dirname, 'src/utils/'),
			navigation: path.resolve(__dirname, 'src/navigation/'),
			axiosConfig: path.resolve(__dirname, 'src/axios/'),
			lib: path.resolve(__dirname, 'lib'),
			firebaseConfig: path.resolve(__dirname, 'src/firebase/'),
			socketIOConfig: path.resolve(__dirname, 'src/socketio/')
		}
	}
}

module.exports = config;