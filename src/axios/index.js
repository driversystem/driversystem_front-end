import axios from 'axios';
import { AXIOS_BASEURL } from 'constants/jsFile/urls';

const axiosInstance = axios.create({
    baseURL: AXIOS_BASEURL
});

axiosInstance.interceptors.request.use(config => {
    let authorization;
    if(localStorage.getItem('token')) {
        authorization = `${localStorage.getItem('token')}`;
    }
    // config.headers.common['Authorization'] = authorization;
    config.headers.common['x-access-token'] = authorization;
    return config;
});

export default {
    axiosInstance
};