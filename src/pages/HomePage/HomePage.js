import React, { useEffect } from 'react';

import "./HomePage.scss";

// import 
import Header from 'components/Header';
import Footer from 'components/Footer';
import HomeScreen from 'screens/HomeScreen';
import MobileDownloadPage from 'pages/MobileDownloadPage';

const MainPage = (props) => {
  useEffect(() => {
    window.document.title = props.pageTitle;
  });

  return (
    <div className="HomePage">
      {/* HEADER */}
      <Header {...props} />
      {/* HomeScreen */}
      <HomeScreen {...props}/>
      {/* FOOTER */}
      <Footer {...props}/>
      {/* Mobile Download */}
      <MobileDownloadPage />
    </div>
  )
}

export default MainPage;