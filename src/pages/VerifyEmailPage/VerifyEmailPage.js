import React from 'react'

import './VerifyEmailPage.scss';

// import Material 
import { Button } from '@material-ui/core';

const VerifyEmailPage = (props) => {
  return (
    <div className='VerifyEmailPage'>
      <img className="VerifyEmailPage__logo" src="/assets/ico/icon_logo.ico" alt="App Logo"/>
      <h2 className="VerifyEmailPage__Title">Xác thực Email thành công</h2>
      <Button className='Back__btn' color="primary" onClick={() => props.history.push('/')} >
        Trở Về Trang Chủ
      </Button>
    </div>
  )
}

export default VerifyEmailPage;