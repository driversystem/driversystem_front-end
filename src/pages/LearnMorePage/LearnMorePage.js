import React, { useEffect } from 'react'

import './LearnMorePage.scss';

// import components
import Header from 'components/Header';
import NavbarHeader from 'components/NavbarHeader';
import Footer from 'components/Footer';
import LearnMoreScreen from 'screens/LearnMoreScreen';
import MobileDownload from 'components/MobileDownload';
import MobileDownloadPage from 'pages/MobileDownloadPage';

const LearnMorePage = (props) => {

  useEffect(() => {
    window.document.title = props.pageTitle;
  });

  return (
    <div className='LearnMorePage'>
      {/* HEADER */}
      <Header {...props} />
      {/* Navbar */}
      <NavbarHeader {...props} />
      {/* LearnMoreScreen */}
      <LearnMoreScreen {...props}/>
      {/* Mobile Download */}
      <MobileDownload />
      {/* FOOTER */}
      <Footer {...props}/>
      {/* Mobile Download */}
      <MobileDownloadPage />
    </div>
  )
}

export default LearnMorePage;
