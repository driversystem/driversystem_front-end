import React, { useEffect } from 'react'

import './ContactPage.scss';

// import components
import Header from 'components/Header';
import NavbarHeader from 'components/NavbarHeader';
import Footer from 'components/Footer';
import ContactScreen from 'screens/ContactScreen';
import MobileDownloadPage from 'pages/MobileDownloadPage';

const ContactPage = (props) =>{

  useEffect(() => {
    window.document.title = props.pageTitle;
  });

  return (
    <div className='ContactPage'>
      {/* HEADER */}
      <Header {...props} />
      {/* Navbar */}
      <NavbarHeader {...props} />
      {/* ContactScreen */}
      <ContactScreen {...props}/>
      {/* FOOTER */}
      <Footer {...props}/>
      {/* Mobile Download */}
      <MobileDownloadPage />
    </div>
  )
}

export default ContactPage;
