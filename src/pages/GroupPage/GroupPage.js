import React from 'react'

import './GroupPage.scss';

import GroupScreen from 'screens/GroupScreen';
import MobileDownloadPage from 'pages/MobileDownloadPage';

const GroupPage = (props) => {
  return (
    <React.Fragment>
      <GroupScreen {...props} />
      {/* Mobile Download */}
      <MobileDownloadPage />
    </React.Fragment>
  ) 
}

export default GroupPage;