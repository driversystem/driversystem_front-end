import React, { useEffect } from 'react'

import './AboutPage.scss';

// import components
import Header from 'components/Header';
import NavbarHeader from 'components/NavbarHeader';
import Footer from 'components/Footer';
import AboutScreen from 'screens/AboutScreen';
import MobileDownloadPage from 'pages/MobileDownloadPage';

const AboutPage = (props) => {

  useEffect(() => {
    window.document.title = props.pageTitle;
  });

  return (
    <div className='AboutPage'>
      {/* HEADER */}
      <Header {...props} />
      {/* Navbar */}
      <NavbarHeader {...props} />
      {/* LearnMoreScreen */}
      <AboutScreen {...props}/>
      {/* FOOTER */}
      <Footer {...props}/>
      {/* Mobile Download */}
      <MobileDownloadPage />
    </div>
  )
}

export default AboutPage;