import React, { useEffect } from 'react';

import './AuthenticationPage.scss';

// import react-router-dom
import { Route, Switch } from 'react-router-dom';
// import react-router-dom
import { Link } from 'react-router-dom';
// import material-ui
import { ArrowBackIos } from '@material-ui/icons';

// import component
import MobileDownloadPage from 'pages/MobileDownloadPage';

import Loadable from 'react-loadable';

const SignUpScreenComponent = Loadable({
  loader: () => import(/* webpackChunkName: "Sign Up Screen" */ './../../screens/SignUpScreen'),
  loading: () => null
});

const SignInScreenComponent = Loadable({
  loader: () => import(/* webpackChunkName: "Sign In Screen" */ './../../screens/SignInScreen'),
  loading: () => null
});

const UserInfoScreenComponent = Loadable({
  loader: () => import(/* webpackChunkName: "User Info Screen" */ './../../screens/UserInfoScreen'),
  loading: () => null
});

const ForgotPasswordScreenComponent = Loadable({
  loader: () => import(/* webpackChunkName: "Forgot Password Screen" */ './../../screens/ForgotPasswordScreen'),
  loading: () => null
});

const AuthenticationPage = (props) => {
  const { pageTitle, history } = props;

  useEffect(() => {

    // check location /authentication
    if (history.location.pathname === '/authentication') {
      history.push('/authentication/signin');
    }
  });

  return (
    <React.Fragment>
      <div className="AuthenPage">
        <div className="AuthenPage__background">
          {/* back to homepage */}
          <Link to='/' className="AuthenPage__link">
            <ArrowBackIos className='AuthenPage__backIcon' />
            <img className="AuthenPage__Logo" src="/assets/ico/icon_GOVN.ico" alt="App Logo"/>
          </Link>
          {/* route in authentication page */}
          <Switch>
            <Route exact path='/authentication/signup' render={(props) => <SignUpScreenComponent {...props} pageTitle={pageTitle} />} ></Route>
            <Route exact path='/authentication/signin' render={(props) => <SignInScreenComponent {...props} pageTitle={pageTitle} />} ></Route>
            <Route exact path='/authentication/profile' render={(props) => <UserInfoScreenComponent {...props} pageTitle={pageTitle} />} ></Route>
            <Route exact path='/authentication/forgotpassword' render={(props) => <ForgotPasswordScreenComponent {...props} pageTitle={pageTitle} />} ></Route>
          </Switch>
        </div>
      </div>
      {/* Mobile Download */}
      <MobileDownloadPage />
    </React.Fragment>
  )
}

export default AuthenticationPage;