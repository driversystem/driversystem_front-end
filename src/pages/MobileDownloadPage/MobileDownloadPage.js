import React from 'react'

import './MobileDownloadPage.scss';

import { isMobile, isIOS, isAndroid } from 'constants/jsFile/device';

// import fontAwesome & Material
import { Modal, Backdrop, Fade } from '@material-ui/core';

const MobileDownloadPage = (props) => {
  return (
    <React.Fragment>
      {isMobile() && (isIOS || isAndroid) && 
        (
          <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className="MobileDownloadPage__Modal"
          open={true}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={true}>
            <div id="Modal__box" className="Modal__Box">
              <img className='App__Logo' src='/assets/ico/icon_logo.ico' alt='App logo'/>
              <h2 id="transition-modal-title" className="Box__Title">Tải GO VN Tại</h2>
              {isIOS && <a aria-label='App store' rel="noreferrer" href='https://itunes.apple.com/vn' target='_blank'>
                <img className='Store__btn' src='/assets/svg/btn_appstore.svg' alt='App store logo'></img>
              </a>}
              {isAndroid && <a  aria-label='Play store' rel="noreferrer" href='https://play.google.com/store' target='_blank'>
                <img className='Store__btn' src='/assets/svg/btn_playstore.svg' alt='Play store logo'></img>
              </a>}
            </div>
          </Fade>
        </Modal>
      )}
    </React.Fragment>
  )
}

export default MobileDownloadPage;