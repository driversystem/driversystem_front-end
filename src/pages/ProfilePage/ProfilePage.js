import React from 'react'

import './ProfilePage.scss';

// import 
import Header from 'components/Header';
import Footer from 'components/Footer';
import NavbarHeader from 'components/NavbarHeader';
import ProfileScreen from 'screens/ProfileScreen';
import MobileDownloadPage from 'pages/MobileDownloadPage';

const ProfilePage = (props) => {
  return (
    <React.Fragment>
      <div className="ProfilePage">
        {/* HEADER */}
        <Header {...props} />
        {/* Navbar */}
        <NavbarHeader {...props} />
        {/* Profile Screen */}
        <ProfileScreen />
        {/* FOOTER */}
        <Footer {...props}/>
        {/* Mobile Download */}
        <MobileDownloadPage />
      </div>
    </React.Fragment>
  )
}

export default ProfilePage;
