import React, { useEffect } from 'react'

import './MapsPage.scss'

// import keys
import { GOOGLE_MAP_KEY } from 'constants/jsFile/urls';

// import components
import GoogleMapComponent from 'components/GoogleMapComponent';
import MobileDownloadPage from 'pages/MobileDownloadPage';

const MapsPage = (props) => {
  useEffect(() => {
    window.document.title = props.pageTitle;
  });

  return (
    <React.Fragment>
      <div className='MapsPage'>
        {/* Key */}
        {/* `AIzaSyC4R6AN7SmujjPUIGKdyao2Kqitzr1kiRg` */}
        
        {/* MAP */}
        <GoogleMapComponent 
          googleMapURL={`https://maps.googleapis.com/maps/api/js?key=`+ GOOGLE_MAP_KEY + `&v=3.exp&libraries=geometry,drawing,places`}
          loadingElement={<div style={{ height: `100vh` }} />}
          containerElement={<div style={{ height: `100vh` }} />}
          mapElement={<div style={{ height: `100vh` }} />}
        />
      </div>
      {/* Mobile Download */}
      <MobileDownloadPage />
    </React.Fragment>
  )
}

export default MapsPage;