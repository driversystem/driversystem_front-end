import React, { PureComponent } from 'react'

// import router
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import RouterConfig from '../RouterConfig';
// import redux
import { connect } from 'react-redux';
import actions from 'actions';
import selectors from 'selectors';
// import hot reload
import { hot } from 'react-hot-loader';
// import recompose
import { compose } from 'recompose';
// import axios
import Axios from 'axiosConfig';
// import components
import App from 'navigation/App';
import AuthenRoute from 'navigation/AuthenRoute';

class Routers extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  componentDidCatch(error, errorInfos) {
    console.log('error', error);
    console.log('errorInfos', errorInfos);
  }

  componentDidMount() {
    const localToken = localStorage.getItem('token');
    if (!localToken) {
      this.setState({ loading: false });
      return;
    }
    // verify token or refresh token
    Axios.axiosInstance.post('/auth/refresh_token')
    .then(res => {
      const { token, user} = res.data;
      const { setCurrentUser } = this.props;
      localStorage.setItem('token', token);
      setCurrentUser(user);
      this.setState({ loading: false });
    })
    .catch(err => {
      localStorage.removeItem('token');
    });
  }

  render() {
    return this.state.loading ? (
      <div />
    ) : (
      <Router>
        <App>
          <React.Fragment>
            <Switch>
              {RouterConfig.map((route, i) => {                
                const RenderComponent = route.auth ? (
                  <AuthenRoute
                    key={i}
                    component={route.component}
                    exact={route.exact}
                    path={route.path}
                    pageTitle={route.pageTitle}
                  />
                ) : (
                    <Route
                      exact={route.exact}
                      path={route.path}
                      render={(props) => 
                        {                          
                          return <route.component {...props} pageTitle={route.pageTitle} />
                        }
                      }
                      key={i}
                    />
                  );
                return RenderComponent;
              })}
            </Switch>
          </React.Fragment>
        </App>
      </Router>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  dispatch,
  setCurrentUser: currentUser => dispatch(actions.setCurrentUser(currentUser)),
});

const mapStateToProps = state => ({
  currentUser: selectors.getCurrentUser(state),
});

export default hot(module)(compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Routers));