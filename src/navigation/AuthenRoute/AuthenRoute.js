import React from 'react';
import { Route, Redirect } from 'react-router-dom';

function AuthenRoute({component: Component, pageTitle, ...rest}) {
  if (pageTitle) {
    window.document.title = pageTitle;
  }
  
  return (
    <React.Fragment>
      <Route {...rest} render={
        props => {          
          return ((localStorage.getItem('token'))
          ?
          (<React.Fragment>
            <Component {...props} />
          </React.Fragment>)
          : (<Redirect
            to={{
              pathname: '/authentication/signin',
              state: { from: props.location }
            }}
          />))
        }
      } />
    </React.Fragment>
  );
}

export default AuthenRoute;
