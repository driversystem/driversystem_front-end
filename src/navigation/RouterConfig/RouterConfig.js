import Loadable from 'react-loadable';

const HomePageLoadableComponent = Loadable({
  loader: () => import(/* webpackChunkName: "Home Page" */ './../../pages/HomePage'),
  loading: () => null
});

const AuthenticationPageLoadableComponent = Loadable({
  loader: () => import(/* webpackChunkName: "Authentication Page" */ './../../pages/AuthenticationPage'),
  loading: () => null
});

const NotFoundPageLoadableComponent = Loadable({
  loader: () => import(/* webpackChunkName: "Not Found Page" */ './../../pages/NotFoundPage'),
  loading: () => null
});

const MapsPageLoadableComponent = Loadable({
  loader: () => import(/* webpackChunkName: "Map Page" */ './../../pages/MapsPage'),
  loading: () => null
});

const GroupPageLoadableComponent = Loadable({
  loader: () => import(/* webpackChunkName: "Group Page" */ './../../pages/GroupPage'),
  loading: () => null
});

const ProfilePageLoadableComponent = Loadable({
  loader: () => import(/* webpackChunkName: "Profile Page" */ './../../pages/ProfilePage'),
  loading: () => null
});

const LearnMorePageLoadableComponent = Loadable({
  loader: () => import(/* webpackChunkName: "Learn More Page" */ './../../pages/LearnMorePage'),
  loading: () => null
});

const AboutPageLoadableComponent = Loadable({
  loader: () => import(/* webpackChunkName: "About Page" */ './../../pages/AboutPage'),
  loading: () => null
});

const ContactPageLoadableComponent = Loadable({
  loader: () => import(/* webpackChunkName: "Contact Page" */ './../../pages/ContactPage'),
  loading: () => null
});

const VerifyEmailPageLoadableComponent = Loadable({
  loader: () => import(/* webpackChunkName: "Contact Page" */ './../../pages/VerifyEmailPage'),
  loading: () => null
});

const routes = [
  {
    path: '/',
    component: HomePageLoadableComponent,
    exact: true,
    auth: false,
    pageTitle: 'GO VN - Trang Chủ'
  },
  {
    path: '/verify-email',
    component: VerifyEmailPageLoadableComponent,
    exact: true,
    auth: false,
    pageTitle: 'GO VN'
  },
  {
    path: '/learnmore',
    component: LearnMorePageLoadableComponent,
    exact: true,
    auth: false,
    pageTitle: 'GO VN - Tìm Hiểu Thêm'
  },
  {
    path: '/about',
    component: AboutPageLoadableComponent,
    exact: true,
    auth: false,
    pageTitle: 'GO VN - Về Chúng Tôi'
  },
  {
    path: '/contact',
    component: ContactPageLoadableComponent,
    exact: true,
    auth: false,
    pageTitle: 'GO VN - Liên Hệ'
  },
  {
    path: '/maps',
    component: MapsPageLoadableComponent,
    exact: true,
    auth: false,
    pageTitle: 'GO VN - Bản Đồ Trực Tuyến'
  },
  {
    path: '/group',
    component: GroupPageLoadableComponent,
    exact: true,
    auth: true,
    pageTitle: 'GO VN - Quản Lí Nhóm'
  },
  {
    path: '/profile',
    component: ProfilePageLoadableComponent,
    exact: true,
    auth: true,
    pageTitle: 'GO VN - Hồ Sơ Cá Nhân'
  },
  {
    path: '/authentication',
    exact: false,
    component: AuthenticationPageLoadableComponent,
    pageTitle: 'GO VN'
  },
  {
    component: NotFoundPageLoadableComponent,
    pageTitle: 'Lỗi 404'
  }
];

export default routes;