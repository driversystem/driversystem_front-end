import socketIOClient from "socket.io-client";
import { AXIOS_BASEURL } from 'constants/jsFile/urls';

const socketIO = socketIOClient(AXIOS_BASEURL);

export {
  socketIO
}