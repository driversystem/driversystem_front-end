import firebase from 'firebase/app';
import 'firebase/storage';

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDWCaf44qeEsbWsvW2sHC73seS-39NtKTI",
  authDomain: "super-map-4556d.firebaseapp.com",
  databaseURL: "https://super-map-4556d.firebaseio.com",
  projectId: "super-map-4556d",
  storageBucket: "super-map-4556d.appspot.com",
  messagingSenderId: "896077110687",
  appId: "1:896077110687:web:9eb4bae01d04558443b1aa"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// //analytics is optional for this tutoral 
// firebase.analytics();

const storage = firebase.storage();

const storageRef = storage.ref();

export {
  storage, 
  storageRef,
  firebase
}