export default {
  viewer: {
    // Mở modal box thêm lịch hẹn
    openAddDateModal: false,
  },
  auth: {
    currentUser: null,
  },
  group: {
    currentGroupID: '',
    selectedAppointment: {
      location: null,
      locationAddress: ''
    },
  },
  map: {
    bounds: null,
    clickMarker: [],
    // searchBox
    searchMarkers: [],
    currentCenter: {
      lat: 10.762887,
      lng: 106.6800684,
    },
    places: [],

    // options marker
    parkingMarkers: [],
    gasMarkers: [],

    // reports marker
    reportMarkers: [],

    // direction guide
    startingPlace: '',
    arrivePlace: '',

    // marker được chọn để tạo lịch hẹn
    chooseMarker: '',

    // Mở sidebar & các tùy chọn
    openSidebar: false,
    openTrafficLayer: false,

    groupMarker: [],
  }
};
