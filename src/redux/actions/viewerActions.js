
// set open add date modal modal
export const setOpenAddDateModal = () => dispatch => {
  dispatch({type: 'SET_OPEN_ADD_DATE_MODAL'});
}

export const setDisableAddDateModal = () => dispatch => {
  dispatch({type: 'SET_DISABLE_ADD_DATE_MODAL'});
}
