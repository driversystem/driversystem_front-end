export const setCurrentGroupID = currentGroupID => dispatch => {
    dispatch({ type: 'SET_CURRENT_GROUP_ID', payload: { currentGroupID } });
};

export const setSelectedAppointment = selectedAppointment => dispatch => {
    dispatch({ type: 'SET_SELECTED_APPOINTMENT', payload: { selectedAppointment }});
};