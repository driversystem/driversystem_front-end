
// map action
export const setCurrentCenter = currentCenter => dispatch => {
  dispatch({ type: 'SET_CURRENT_CENTER', payload: { currentCenter } });
}
// ------------------------------------------------------------------------------------------
// search box
export const setSearchMarkers = searchMarkers => dispatch => {
  dispatch({ type: 'SET_SEARCH_MARKER', payload: { searchMarkers } });
}

export const setPlaces = places => dispatch => {
  dispatch({ type: 'SET_PLACES', payload: { places } });
}
// ------------------------------------------------------------------------------------------
// set options marker
export const setGasMarkers = gasMarkers => dispatch => {
  dispatch({ type: 'SET_GAS_MARKERS', payload: { gasMarkers } });
}

export const setParkingMarkers = parkingMarkers => dispatch => {
  dispatch({ type: 'SET_PARKING_MARKERS', payload: { parkingMarkers } });
}
// ------------------------------------------------------------------------------------------
// set rp marker
export const setReportMarkers = reportMarkers => dispatch => {
  dispatch({ type: 'SET_REPORT_MARKERS', payload: { reportMarkers } });
}
// ------------------------------------------------------------------------------------------
// set direction guide
export const setStartingPoint = startingPlace => dispatch => {
  dispatch({ type: 'SET_STARTING_POINT', payload: { startingPlace } });
}

export const setArrivePoint = arrivePlace => dispatch => {
  dispatch({ type: 'SET_ARRIVE_POINT', payload: { arrivePlace } });
}
// ------------------------------------------------------------------------------------------
// set click marker on map
export const setClickMarker = clickMarker => dispatch => {
  dispatch({ type: 'SET_CLICK_MARKER', payload: { clickMarker } });
}
// ------------------------------------------------------------------------------------------
// set choose marker to create a date
export const setChooseMarker = chooseMarker => dispatch => {
  dispatch({ type: 'SET_CHOOSE_MARKER', payload: { chooseMarker } });
}
// ------------------------------------------------------------------------------------------
// set open sidebar & Option
export const setOpenSidebar = openSidebar => dispatch => {
  dispatch({ type: 'SET_OPEN_SIDEBAR', payload: { openSidebar } });
}

export const setOpenTrafficLayer = openTrafficLayer => dispatch => {
  dispatch({ type: 'SET_OPEN_TRAFFIC_LAYER', payload: { openTrafficLayer } });
}
// ------------------------------------------------------------------------------------------
// set group marker
export const setGroupMarker = groupMarker => dispatch => {
  dispatch({ type: 'SET_GROUP_MARKER', payload: { groupMarker } });
}
// ------------------------------------------------------------------------------------------
// clear state in redux
export const clearStartingPoint = () => dispatch => {
  dispatch({ type: 'CLEAR_STARTING_POINT' });
}

export const clearArrivePoint = () => dispatch => {
  dispatch({ type: 'CLEAR_ARRIVE_POINT' });
}

export const clearClickMarker = () => dispatch => {
  dispatch({ type: 'CLEAR_CLICK_MARKER' });
}

export const clearChooseMarker = () => dispatch => {
  dispatch({ type: 'CLEAR_CHOOSE_MARKER' });
}

export const clearGasMarkers = () => dispatch => {
  dispatch({ type: 'CLEAR_GAS_MARKERS' });
}

export const clearParkingMarkers = () => dispatch => {
  dispatch({ type: 'CLEAR_PARKING_MARKERS' });
}

export const clearCurrentCenter = () => dispatch => {
  dispatch({ type: 'CLEAR_CURRENT_CENTER' });
}

export const clearPlaces = () => dispatch => {
  dispatch({ type: 'CLEAR_PLACES' });
}

export const clearSearchMarkers = () => dispatch => {
  dispatch({ type: 'CLEAR_SEARCH_MARKER' });
}

export const clearGroupMarker = () => dispatch => {
  dispatch({ type: 'CLEAR_GROUP_MARKER' });
}

export const clearOpenSidebar = () => dispatch => {
  dispatch({ type: 'CLEAR_OPEN_SIDEBAR' });
}

export const clearOpenTrafficLayer = () => dispatch => {
  dispatch({ type: 'CLEAR_OPEN_TRAFFIC_LAYER' });
}
// ------------------------------------------------------------------------------------------
