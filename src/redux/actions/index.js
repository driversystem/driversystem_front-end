import * as customActions from 'actions/customActions';
import * as authActions from 'actions/authActions';
import * as mapActions from 'actions/mapActions';
import * as groupActions from 'actions/groupActions';
import * as viewerActions from 'actions/viewerActions';

export default { ...customActions, ...authActions, ...mapActions, ...viewerActions, ...groupActions };
