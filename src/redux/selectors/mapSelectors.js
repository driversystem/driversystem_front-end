
export const getSearchMarkers = state => state.map.searchMarkers;
export const getCurrentCenter = state => state.map.currentCenter;
export const getPlaces= state => state.map.places;

export const getParkingMarkers = state => state.map.parkingMarkers;
export const getGasMarkers = state => state.map.gasMarkers;

export const getReportMarkers = state => state.map.reportMarkers;

export const getStartingPoint = state => state.map.startingPlace;
export const getArrivePoint = state => state.map.arrivePlace;

export const getClickMarker = state => state.map.clickMarker;

export const getChooseMarker = state => state.map.chooseMarker;

export const getGroupMarker = state => state.map.groupMarker;

export const getOpenSidebar = state => state.map.openSidebar;
export const getOpenTrafficLayer = state => state.map.openTrafficLayer;

