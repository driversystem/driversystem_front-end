export const getCurrentGroupID = state => state.group.currentGroupID;
export const getSelectedAppointment = state => state.group.selectedAppointment;