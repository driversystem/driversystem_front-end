import * as customSelectors from 'selectors/customSelectors';
import * as mapSelectors from 'selectors/mapSelectors';
import * as groupSelectors from 'selectors/groupSelectors';
import * as viewerSelectors from 'selectors/viewerSelectors';

export default { ...customSelectors, ...mapSelectors, ...viewerSelectors, ...groupSelectors };
