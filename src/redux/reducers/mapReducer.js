export default initialState => (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {      
      // search box
    case 'SET_SEARCH_MARKER':
      return {
        ...state,
        searchMarkers: {
          ...payload.searchMarkers,
        }
      };
    case 'SET_CURRENT_CENTER':
      return {
        ...state,
        currentCenter: {
          ...payload.currentCenter,
        }
      };
    case 'SET_PLACES':
      return {
        ...state,
        places: {
          ...payload.places,
        }
      };

      // options marker
    case 'SET_GAS_MARKERS':
      return {
        ...state,
        gasMarkers: {
          ...payload.gasMarkers,
        }
      };
    case 'SET_PARKING_MARKERS':
      return {
        ...state,
        parkingMarkers: {
          ...payload.parkingMarkers,
        }
      };
   
      // Report marker
    case 'SET_REPORT_MARKERS':
      return {
        ...state,
        reportMarkers: {
          ...payload.reportMarkers,
        }
      };

      // direction guide
    case 'SET_STARTING_POINT':
      return {
        ...state,
        startingPlace: {
          ...payload.startingPlace,
        }
      };
    case 'SET_ARRIVE_POINT':
      return {
        ...state,
        arrivePlace: {
          ...payload.arrivePlace,
        }
      };
  
      // click marker
    case 'SET_CLICK_MARKER': 
      return {
        ...state,
        clickMarker: {
          ...payload.clickMarker,
        }
      };

      // choose marker to create a date
    case 'SET_CHOOSE_MARKER':
      return {
        ...state,
        chooseMarker: {
          ...payload.chooseMarker,
        }
      }

      // open side bar
    case 'SET_OPEN_SIDEBAR':
      return {
        ...state,
        openSidebar: payload.openSidebar,
      }
    case 'SET_OPEN_TRAFFIC_LAYER':
      return {
        ...state,
        openTrafficLayer: payload.openTrafficLayer,
      }

      // set group marker
    case 'SET_GROUP_MARKER': 
      return {
        ...state,
        groupMarker: {
          ...payload.groupMarker,
        }
      };  

      // Clear state in redux
    case 'CLEAR_STARTING_POINT':
      return {
        ...state,
        startingPlace: null,
      };
    case 'CLEAR_ARRIVE_POINT':
      return {
        ...state,
        arrivePlace: null,
      };
    case 'CLEAR_CLICK_MARKER':
      return {
        ...state,
        clickMarker: null,
      };
    case 'CLEAR_CHOOSE_MARKER':
      return {
        ...state,
        chooseMarker: null,
      };
    case 'CLEAR_GAS_MARKERS':
      return {
        ...state,
        gasMarkers: null,
      };
    case 'CLEAR_PARKING_MARKERS':
      return {
        ...state,
        parkingMarkers: null,
      };
    case 'CLEAR_CURRENT_CENTER':
      return {
        ...state,
        currentCenter: {
          lat: 10.762887,
          lng: 106.6800684,
        },
      };
    case 'CLEAR_PLACES':
      return {
        ...state,
        places: null,
      };
    case 'CLEAR_SEARCH_MARKER':
      return {
        ...state,
        searchMarkers: null,
      };
    case 'CLEAR_GROUP_MARKER':
      return {
        ...state,
        groupMarker: null,
      };
    case 'CLEAR_OPEN_SIDEBAR':
      return {
        ...state,
        openSidebar: false,
      }
    case 'CLEAR_OPEN_TRAFFIC_LAYER':
      return {
        ...state,
        openTrafficLayer: false,
      }

    default:
      return state;
  }
};