export default initialState => (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    // Đóng mở add date modal
    case 'SET_OPEN_ADD_DATE_MODAL': 
      return {
        ...state,
        openAddDateModal: true,
      }
    case 'SET_DISABLE_ADD_DATE_MODAL': 
      return {
        ...state,
        openAddDateModal: false,
      }

    default:
      return state;
  }
};