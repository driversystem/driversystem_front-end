import { combineReducers } from 'redux';
// initialState
import initialState from 'src/redux/initialState';

// Import Reducers
import viewerReducer from 'reducers/viewerReducer';
import authReducer from 'reducers/authReducer';
import mapReducer from 'reducers/mapReducer';
import groupReducer from 'reducers/groupReducer';

// export State
export default combineReducers({
  viewer: viewerReducer(initialState.viewer),
  auth: authReducer(initialState.auth),
  map: mapReducer(initialState.map),
  group: groupReducer(initialState.group),
});