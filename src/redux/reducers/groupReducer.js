export default initialState => (state = initialState, action) => {
    const { type, payload } = action;
    switch (type) {
        case 'SET_CURRENT_GROUP_ID':
            return {
                ...state,
                currentGroupID: {
                    ...payload.currentGroupID,
                }
            };
        case 'SET_SELECTED_APPOINTMENT':
            return {
                ...state,
                selectedAppointment: {
                    ...payload.selectedAppointment,
                }
            };
        default:
            return state;
    }
};