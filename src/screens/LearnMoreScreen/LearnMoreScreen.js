import React from 'react'

import './LearnMoreScreen.scss';

const LearnMoreScreen = (props) => {
  return (
    <div className='LearnMoreScreen'>
      <div className='LearnMoreScreen__content'>
        {/* Đoạn giới thiệu */}
        <div className='LearnMore__Intro'>
          <img className='Intro__logo' src="/assets/ico/icon_logo.ico" alt="Logo"/>
          <div className='Intro__description'>GO VN MANG ĐẾN CHO BẠN NHỮNG GÌ ?</div>
        </div>

        {/* Tính năng Lộ trình */}
        <div className='LearnMore__detail learnMore__left'>
          <div className='Detail__icon left__detail'>
            <img className='Info__icon icon__left' src="/assets/png/way.png" alt="direction icon"/>
          </div>
          <div className='Detail__description left__description'>Xem tình hình về lộ trình chuẩn bị di chuyển</div>
        </div>

        {/* Tính năng Marker */}
        <div className='LearnMore__detail learnMore__right'>
          <div className='Detail__description right__description'>Cập nhật tình hình thực tế trên cung đường như tai nạn, kẹt xe, thời tiết,...</div>

          <div className='Detail__icon right__detail'>
            <img className='Info__icon icon__right' src="/assets/png/marker.png" alt="marker icon"/>
          </div>
        </div>

        {/* Tính năng liên lạc */}
        <div className='LearnMore__detail learnMore__left'>
          <div className='Detail__icon left__detail'>
            <img className='Info__icon icon__left' src="/assets/png/call.png" alt="call icon"/>
          </div>
          <div className='Detail__description left__description'>Trao đổi, gọi diện cho bạn bè</div>
        </div>

        {/* Tính năng Group */}
        <div className='LearnMore__detail learnMore__right'>
          <div className='Detail__description right__description'>Lên lịch hẹn, gặp gỡ bạn bè</div>

          <div className='Detail__icon right__detail'>
            <img className='Info__icon icon__right' src="/assets/png/employee.png" alt="group icon"/>
          </div>
        </div>

      </div>
    </div>
  )
}

export default LearnMoreScreen;