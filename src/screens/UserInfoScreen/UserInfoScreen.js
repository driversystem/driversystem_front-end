import React, { Component } from 'react';

import './UserInfoScreen.scss';
// import fontAwesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen, faCalendarAlt, faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
// import axios
import Axios from 'axiosConfig';
// import components
import AvatarUploader from 'components/AvatarUploader';
// import redux
import { connect } from 'react-redux';
import actions from 'actions';
import selectors from 'selectors';
// import firebase
import { storageRef, firebase } from 'firebaseConfig';
// import components
import NotificationModal from 'components/NotificationModal';

class UserInfoScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      birthDate: '',
      phoneNumber: '',
      avatar: '',

      error: '',
      success: false,
      openNotificationModal: false,
    };
  }

  componentDidMount() {
    const { history, currentUser } =  this.props;
    if (!localStorage.getItem('token')) {
      history.push('/authentication/signin');
    }
    if(currentUser) {
      if(((currentUser.authProvider === 'google') || (currentUser.authProvider === 'facebook'))) {
        if(currentUser.avatar) {
          history.push('/');
        }
      }
    }
    window.document.title = this.props.pageTitle + ' - Hồ Sơ Cá Nhân';
  }

  // Set Open or Close Notification Modal
  setOpenNotificationModal = () => {
    this.setState({ openNotificationModal: true });
  }

  setDisableNotificationModal = () => {
    this.setState({ openNotificationModal: false });
  }
  // ----------------------------------------------------

  handleTextChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  // Up ảnh lên firebase
  uploadImageToFirebase = async () => {
    // ----------------------------------------------------------------------------------
    return new Promise((resolve, reject) => {
      const { avatar } = this.state;
  
      const metadata = { 
        contentType: avatar.type,
      }
      
      const uploadTask = storageRef.child('images/User/UserAvatar/' + avatar.name).put(avatar, metadata);
  
      // ---------------------------------------------------------------
      // Listen for state changes, errors, and completion of the upload.
      uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
        (snapshot) => {
          // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
          const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          switch (snapshot.state) {
            case firebase.storage.TaskState.PAUSED: // or 'paused'
              break;
            case firebase.storage.TaskState.RUNNING: // or 'running'
              break;
          }
        }, (error) => {
          // A full list of error codes is available at
          // https://firebase.google.com/docs/storage/web/handle-errors
          switch (error.code) {
            case 'storage/unauthorized': {
              // User doesn't have permission to access the object
              break;
            }
            case 'storage/canceled': {
              // User canceled the upload
              break;
            }
            case 'storage/unknown': {
              // Unknown error occurred, inspect error.serverResponse
              break;
            }
          }
        }, () => {
          // Upload completed successfully, now we can get the download URL
          uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
            resolve(downloadURL)
          });
        });
      // ---------------------------------------------------------------
    })
  }

  handleSubmitProfile = async(e) => {
    // ----------------------------------------------------------------------------------
    // Gọi api về backend để update profile của user
    const { name, birthDate, phoneNumber } = this.state;
    const { history, setCurrentUser } = this.props;

    const avatarURL = await this.uploadImageToFirebase();

    const user = { 
      name: name,
      birthDate: birthDate,
      phoneNumber: phoneNumber,
      avatar: avatarURL
    }

    Axios.axiosInstance.put('/user/profile', user)
    .then((res) => {
      this.setState({ success: true });
      this.setOpenNotificationModal();
      setCurrentUser(res.data.user);
    })
    .catch((err) => {
      this.setState({
        error: {
          response: err.response,
        },
      });
      this.setOpenNotificationModal();
    });
  }

  handleSetAvatar = (avatar) => {
    this.setState({ avatar: avatar })
  }

  render() {
    const { name, birthDate, phoneNumber, openNotificationModal, success, error } = this.state;
    const { currentUser, history } = this.props;
    console.log("UserInfoScreen -> render -> currentUser", currentUser)
    return (
      <React.Fragment>
        <div className='UserInfoForm'>
          <div className="UserInfoForm__title">Hồ Sơ Cá Nhân</div>

            {/* Hộp nội dung */}
            <div className="UserInfoForm__content">
              <div className="UserInfoForm__form">
                {/* <AvatarUploader /> */}
                <AvatarUploader handleSetAvatar={this.handleSetAvatar} currentUser={currentUser} userAvatar={currentUser.avatar}/>
                
                {/* Form nhập */}
                <div className="UserInfoForm__input--form">
                  <FontAwesomeIcon className="UserInfoForm__icon" icon={faPen} />
                  <input
                    className="UserInfoForm__input"
                    type="text"
                    placeholder="Họ tên ..."
                    name="name"
                    onChange={this.handleTextChange}
                    value={name}
                    required
                  />
                </div>
                <div className="UserInfoForm__input--form">
                  <FontAwesomeIcon
                    className="UserInfoForm__icon"
                    icon={faCalendarAlt}
                  />
                  <input
                    className="UserInfoForm__input"
                    type="date"
                    placeholder="Ngày sinh ..."
                    name="birthDate"
                    onChange={this.handleTextChange}
                    value={birthDate}
                    required
                  />
                </div>
                <div className="UserInfoForm__input--form">
                  <FontAwesomeIcon className="UserInfoForm__icon" icon={faPhoneAlt} />
                  <input
                    className="UserInfoForm__input"
                    type="tel"
                    placeholder="Số điện thoại ..."
                    name="phoneNumber"
                    onChange={this.handleTextChange}
                    value={phoneNumber}
                    required
                  />
                </div>

                {/* Button Đăng ký */}
                <button className="UserInfoForm__btn" onClick={this.handleSubmitProfile}> Hoàn tất</button>
              </div>
            </div>

            { openNotificationModal && 
              <NotificationModal 
                success={success} 
                error={error} 
                history={history}
                typeCheck='UserInfo'
                openNotificationModal={openNotificationModal}
                setDisableNotificationModal={this.setDisableNotificationModal}
              />
            }
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  currentUser: selectors.getCurrentUser(state),
});


const mapDispatchToProps = dispatch => ({
  setCurrentUser: currentUser => dispatch(actions.setCurrentUser(currentUser)),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserInfoScreen);
