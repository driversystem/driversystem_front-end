import React, { Component } from 'react'

import './ForgotPasswordScreen.scss';

// // import redux
// import { connect } from 'react-redux';
// import actions from 'actions';

// import material-UI & FontAwesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
// import axios
import Axios from 'axiosConfig';
// import react-router-dom
import { Link } from 'react-router-dom';

class ForgotPasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: ''
    }
  }

  componentDidMount() { 
    const { history } = this.props;
    // check if user was log in
    if (localStorage.getItem('token')) {
      history.push('/');
    }
    window.document.title = this.props.pageTitle + ' - Quên Mật Khẩu';
  }

  handleTextChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleKeyUp = (e) => {

  }

  handleBlurError = (e) => {

  }

  handleSubmitForgot = (e) => {

  }

  render() {
    const { email } = this.state;
    return (
      <React.Fragment>
        <div className='ForgotPasswordForm'>
          <div className="ForgotPasswordForm__title">Quên Mật Khẩu</div>
          {/* Hộp nội dung */}
          <div className="ForgotPasswordForm__content">
            {/* Logo */}
            <div className="ForgotPasswordForm__logo--icon">
              <img className="ForgotPasswordForm__logo" src="/assets/ico/icon_GOVN.ico" alt="Logo"/>
            </div>
            {/* Form */}
            <div className="ForgotPasswordForm__Form">
              {/* Label */}
              <span className="ForgotPasswordForm__label">Nhập Email của bạn</span>
              
              {/* Form nhập */}
              <form onSubmit={this.handleSubmitForgot}>
                <div className="ForgotPasswordForm__input--form">
                  <FontAwesomeIcon className="ForgotPasswordForm__icon" icon={faEnvelope}/>
                  <input 
                    className="ForgotPasswordForm__input" 
                    type="email" 
                    placeholder="Địa chỉ Email ..." 
                    name="email" 
                    onChange={this.handleTextChange}
                    onKeyUp={this.handleKeyUp}
                    onBlur={this.handleBlurError}
                    value={email} 
                    required
                  />
                </div>
                {/* Button Hoàn Tất */}
                <button className="ForgotPasswordForm__btn" type='submit'>Hoàn Tất</button>
              </form>
              {/* Quên mật khẩu & Đăng Kí */}
              <div className="ForgotPasswordForm__link">
                <span className="Link__btn">Tôi đã nhớ mật khẩu</span>
                <Link to='/authentication/signin' className="Link__btn btn--back">Trở về</Link>
              </div>
            </div>   
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default ForgotPasswordScreen;