import React from 'react'

import './AboutScreen.scss';

const AboutScreen = () => {
  return (
    <div className='AboutScreen'>
      <div className='AboutScreen__content'>
        <div className='About__Info'>
          <div className='About__description'>
            <div className='description__title'>Về Chúng Tôi</div>
            <div className='description__Info'>GO VN là một ứng dụng đa nền tảng hỗ trợ người tham gia giao thông trong các lĩnh vực sau:
              <div className='Info__each'> - Theo dõi tình hình giao thông theo thời gian thực.</div>
              <div className='Info__each'> - Gia tăng độ hiệu quả của việc giao tiếp giữa các tài xế với nhau khi di chuyển.</div>
              <div className='Info__each'> - Kết nối các tài xế với nhau. </div>
            </div>
            <div className='description__Info'>Bên cạnh đó GO VN còn tối ưu hóa tương tác giữa người dùng và thiết bị di động trong khi đang di chuyển, tham gia giao thông.</div>
            <div className='description__Info'>GO VN mong nhận được sự ủng hộ và những phản hồi tích cực của mọi người.</div>
            <div className='description__Info'>GO VN được phát triển đội ngũ “Driver System”.</div>
          </div>
          <div className='About__image'>
            <img className='Image__logo' alt='logo' src='assets/png/logo.png'/>
          </div>
        </div>
      </div>
    </div>
  )
}

export default AboutScreen;
