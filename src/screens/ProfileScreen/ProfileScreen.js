import React, { Component } from 'react'

import './ProfileScreen.scss'

// import material-ui & fontawesome
import { Button } from '@material-ui/core';

// import redux
import { connect } from 'react-redux';
import actions from 'actions';
import selectors from 'selectors';
// import axios
import Axios from 'axiosConfig';
// import components
import AvatarUploader from 'components/AvatarUploader';

// import firebase
import { storageRef, firebase } from 'firebaseConfig';
import NotificationModal from 'components/NotificationModal';

class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      phoneNumber: '',
      avatar: '',
      openAvatarAction: false,

      disableNameField: true,

      disablePhoneNumberField: true,
      openAddPhoneNumberInput: false,

      openChangePasswordField: true,

      error: '',
      success: false,
      openNotificationModal: false,
    };
  }

  // ----------------- React lifecycles ----------------------------------
  componentDidMount() {
    const { currentUser } = this.props;
    this.setState({
      name: currentUser.name,
      phoneNumber: currentUser.phoneNumber,
      email: currentUser.email,
      avatar: currentUser.avatar,
    })
  }

  componentDidUpdate(prevProps, prevState) {
    const { disableNameField, disablePhoneNumberField, openAddPhoneNumberInput } = this.state;
    const { currentUser } = this.props;

    if(disableNameField !== prevState.disableNameField) {
      this.setState({name: currentUser.name})
    }
    if((disablePhoneNumberField !== prevState.disablePhoneNumberField) || (openAddPhoneNumberInput !== prevState.openAddPhoneNumberInput)) {
      this.setState({phoneNumber: currentUser.phoneNumber})
    }
  }
  // ---------------------------------------------------------------------
    // Set Open or Close Notification Modal
    setOpenNotificationModal = () => {
      this.setState({ openNotificationModal: true });
    }
  
    setDisableNotificationModal = () => {
      this.setState({ openNotificationModal: false });
    }
  // ---------------------------------------------------------------------
  handleTextChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  // Hiển thị khung thay đổi password
  onOpenChangePasswordField = (e) => {
    this.setState({ openChangePasswordField: true})
  }

  handleSetAvatar = (avatar) => {
    this.setState({ 
      openAvatarAction: true,
      avatar: avatar 
    })
  }

  // -------------------------------------USERNAME----------------------------------------
  // Mở input field name để user thay đổi tên
  onOpenChangeNameField = (e) => {
    this.setState({ disableNameField: false });
  }

  // Hủy bỏ thay đổi tên
  onDisableNameField = (e) => {
    this.setState({
      disableNameField: true,
    });
  }

  // Update tên cho user
  onUpdateNameField = (e) => {
    // Gửi api update tên cho user
    const { name } = this.state;
    const { setCurrentUser } = this.props;

    const user = {
      name: name
    }
    
    Axios.axiosInstance.post('/user/profile/name', user)
    .then((res) => {
      const { user } = res.data;
      setCurrentUser(user)
      // set new avatar sau khi gọi api xong 
      this.setState({
        disableNameField: true,
        name: user.name,
      });
      this.setState({ success: true });
      this.setOpenNotificationModal();
      this.setState({ success: false });
    })
    .catch((err) => {
      this.setState({
        error: {
          response: err.response,
        },
      });
      this.setOpenNotificationModal();
    });
  }
  // -------------------------------------------------------------------------------------
  
  // ------------------------------------AVATAR-------------------------------------------
  // Hủy bỏ thay đổi avatar
  onCancelUpdateUserAvatar = (e) => {
    const { currentUser } = this.props;
    this.setState({
      openAvatarAction: false,
      avatar: currentUser.avatar,
    });
  }

  // Up ảnh lên firebase
  uploadImageToFirebase = async () => {
    // ----------------------------------------------------------------------------------
    return new Promise((resolve, reject) => {
      const { avatar } = this.state;
  
      const metadata = { 
        contentType: avatar.type,
      }
      
      const uploadTask = storageRef.child('images/User/UserAvatar/' + avatar.name).put(avatar, metadata);
  
      // ---------------------------------------------------------------
      // Listen for state changes, errors, and completion of the upload.
      uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
        (snapshot) => {
          // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
          const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          switch (snapshot.state) {
            case firebase.storage.TaskState.PAUSED: // or 'paused'
              break;
            case firebase.storage.TaskState.RUNNING: // or 'running'
              break;
          }
        }, (error) => {
          // A full list of error codes is available at
          // https://firebase.google.com/docs/storage/web/handle-errors
          switch (error.code) {
            case 'storage/unauthorized': {
              // User doesn't have permission to access the object
              break;
            }
            case 'storage/canceled': {
              // User canceled the upload
              break;
            }
            case 'storage/unknown': {
              // Unknown error occurred, inspect error.serverResponse
              break;
            }
          }
        }, () => {
          // Upload completed successfully, now we can get the download URL
          uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
            resolve(downloadURL)
          });
        });
      // ---------------------------------------------------------------
    })
  }

  // Update avatar cho user
  onUpdateUserAvatar = async (e) => {
    const { setCurrentUser } = this.props;

    // Gửi ảnh lên firebase rồi update dưới back-end
    const uploadAvatarURL = await this.uploadImageToFirebase();
    // ----------------------------------------------------------------------------------
    // Gọi api về backend để update avatar của user
    const user = { 
      avatar: uploadAvatarURL
    }

    await Axios.axiosInstance.post('/user/profile/picture', user)
    .then((res) => {
      setCurrentUser(res.data.user)
      // set new avatar sau khi gọi api xong 
      this.setState({
        openAvatarAction: false,
      });
      this.setState({ success: true });
      this.setOpenNotificationModal();
      this.setState({ success: false });
    })
    .catch((err) => {
      this.setState({
        error: {
          response: err.response,
        },
      });
      this.setOpenNotificationModal();
    });
  }
  // ------------------------------------SĐT-----------------------------------------------

  // Hiển thị thanh input Thêm Số điện thoại
  onOpenAddPhoneNumber = (e) => {
    this.setState({ openAddPhoneNumberInput: true});
  }

  onOpenChangePhoneNumberField = (e) => {
    this.setState({
      disablePhoneNumberField: false,
    });
  }

  onDisablePhoneNumberField = (e) => { 
    this.setState({
      disablePhoneNumberField: true,
      openAddPhoneNumberInput: false,
    });
  }

  onUpdatePhoneNumberField = (e) => {
    // Gửi api update Sđt cho user
    const { phoneNumber } = this.state;
    const { setCurrentUser } = this.props;
        
    const user = {
      phone: phoneNumber
    }

    Axios.axiosInstance.post('/user/profile/phone', user)
    .then((res) => {
      const { user } = res.data;
      setCurrentUser(user)
      // set new avatar sau khi gọi api xong 
      this.setState({
        disablePhoneNumberField: true,
        openAddPhoneNumberInput: false,
        phoneNumber: user.phoneNumber,
      });
      this.setState({ success: true });
      this.setOpenNotificationModal();
      this.setState({ success: false });
    })
    .catch((err) => {
      this.setState({
        error: {
          response: err.response,
        },
      });
      this.setOpenNotificationModal();
    });
  }

  render() {
    const { currentUser } = this.props;
    const { name, 
            email,
            phoneNumber, 
            avatar, 
            openAvatarAction, 
            disableNameField,  
            disablePhoneNumberField, 
            openAddPhoneNumberInput,
            openNotificationModal, 
            success, 
            error,

            openChangePasswordField } = this.state;
      
    return (
      <React.Fragment>
        <div className='ProfileScreen'>
          <div className='ProfileScreen__content'> 
            <h2 className='Content__title'>Hồ Sơ Cá Nhân</h2>

            {/* Thông tin user */}
            <div className='Profile__info'>
              {/* avatar upload */}
              <div className='Profile__avatar'>
                <AvatarUploader handleSetAvatar={this.handleSetAvatar} userAvatar={avatar} currentUser={currentUser}/>
                { openAvatarAction && 
                  (
                    <React.Fragment>
                      <div className='Actions'>
                        <Button className='Action__btn Btn__cancel' onClick={this.onCancelUpdateUserAvatar}>Hủy Bỏ</Button>
                        <Button className='Action__btn Btn__confirm' onClick={this.onUpdateUserAvatar}>Cập Nhật</Button>
                      </div>
                    </React.Fragment>
                  )
                }
                <hr className='divider'/>
              </div>
            
              <div className='Title__text'>Thông Tin Người Dùng</div>

              {/* email field */}
              <div className='MarginField Profile__email'>
                <span className='Email__title'>Email: </span>
                <span className='Email'>{email}</span>
              </div>

              {/* name field */}
              <div className='MarginField Profile__name'>
                <div className='Name__title'>
                  <span className='Title'>Họ Tên</span>
                  {disableNameField && (<button className='Name__btn' onClick={this.onOpenChangeNameField}>Đổi tên</button>)}
                </div>
                <input 
                  className='Name__input'
                  type="text"
                  name="name"
                  onChange={this.handleTextChange}
                  value={name}
                  disabled={disableNameField}
                />
                { disableNameField || 
                  (
                    <React.Fragment>
                      <div className='Actions'>
                        <Button className='Action__btn Btn__cancel' onClick={this.onDisableNameField}>Hủy Bỏ</Button>
                        <Button className='Action__btn Btn__confirm' onClick={this.onUpdateNameField} 
                          disabled={!name || name.length < 2 || name === currentUser.name}
                        >Cập Nhật</Button>
                      </div>
                      <hr className='divider'/>
                    </React.Fragment>
                  )
                }
              </div>

              {/* phone number field */}
              <div className='MarginField Profile__phoneNumber'>
                <div className='PhoneNumber__title'>
                  <span className='Title'>Số điện thoại</span>
                  {currentUser.phoneNumber && disablePhoneNumberField && (<button className='PhoneNumber__btn' onClick={this.onOpenChangePhoneNumberField}>Đổi số điện thoại</button>)}
                </div>
                { (currentUser.phoneNumber) && 
                  (
                    <input 
                      className='PhoneNumber__input'
                      type="tel"
                      name="phoneNumber"
                      onChange={this.handleTextChange}
                      value={phoneNumber}
                      disabled={disablePhoneNumberField}
                      />
                  )
                }
                { !currentUser.phoneNumber && !openAddPhoneNumberInput && 
                  (
                    <Button className='PhoneNumber__addBtn' onClick={this.onOpenAddPhoneNumber}>Thêm số điện thoại</Button>
                  )
                }
                 
                { openAddPhoneNumberInput && (
                    <input 
                      className='PhoneNumber__input'
                      type="tel"
                      name="phoneNumber"
                      onChange={this.handleTextChange}
                      value={phoneNumber}
                    />
                  )
                }
                { (disablePhoneNumberField && !openAddPhoneNumberInput)||
                  (
                    <React.Fragment>
                      <div className='Actions'>
                        <Button className='Action__btn Btn__cancel' onClick={this.onDisablePhoneNumberField}>Hủy Bỏ</Button>
                        <Button className='Action__btn Btn__confirm' onClick={this.onUpdatePhoneNumberField} 
                          disabled={!phoneNumber || phoneNumber.length < 7 || phoneNumber.length > 11 || phoneNumber === currentUser.phoneNumber}
                        >Cập Nhật</Button>
                      </div>
                      <hr className='divider'/>
                    </React.Fragment>
                  )
                }
              </div>

              {/* Change password field */}
              {/* <div className='MarginField Profile__password'>
                <Button className='Password__btn' onClick={this.onOpenChangePasswordField}>Đổi mật khẩu</Button>
              </div> */}
            
              { openNotificationModal && 
                <NotificationModal 
                  success={success} 
                  error={error} 
                  history={history}
                  typeCheck='Profile'
                  openNotificationModal={openNotificationModal}
                  setDisableNotificationModal={this.setDisableNotificationModal}
                />
              }
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  currentUser: selectors.getCurrentUser(state),
});


const mapDispatchToProps = dispatch => ({
  setCurrentUser: currentUser => dispatch(actions.setCurrentUser(currentUser)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);