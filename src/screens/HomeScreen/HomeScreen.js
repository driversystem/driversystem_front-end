import React, { Component } from 'react'

import './HomeScreen.scss';

// import components
import NavbarHeader from 'components/NavbarHeader';
import Introduction from 'components/Introduction';
import MobileDownload from 'components/MobileDownload';

class HomeScreen extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="HomeScreen">
          {/* Navbar */}
          <NavbarHeader {...this.props} />
          {/* Introduction */}
          <Introduction />
          {/* Mobile Download */}
          <MobileDownload />
        </div>
      </React.Fragment>
    )
  }
}

export default HomeScreen;