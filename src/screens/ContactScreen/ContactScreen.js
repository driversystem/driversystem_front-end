import React, { Component } from 'react'

import './ContactScreen.scss';

// import material-UI & FontAwesome
import { Icon } from '@material-ui/core';
import { Drafts, Phone, LocationOn } from '@material-ui/icons';

class ContactScreen extends Component {
  render() {
    return (
      <div className='ContactScreen'>
        <div className='ContactScreen__content'>
          <div className='ContactScreen__Info'>
            <div className='Description__title'>Liên Hệ Với Chúng Tôi</div>
            <div className='Description__info'>
              <div className='Info__each'>
                <Icon className='Info__icon'><Drafts className='Info__icon' /></Icon>
                <span className='content'>govngroup@gmail.com</span>
              </div>
              <div className='Info__each'>
                <Icon className='Info__icon'><Phone className='Info__icon'/></Icon>
                <span className='content'>0785402309</span>
              </div>
              <div className='Info__each'>
                <Icon className='Info__icon'><LocationOn className='Info__icon'/></Icon>
                <span className='content'>227 Nguyễn Văn Cừ, Phường 3, Quận 5, TP.Hồ Chí Minh</span>
              </div>
            </div>
          </div>
         
        </div>
      </div>
    )
  }
}

export default ContactScreen;

