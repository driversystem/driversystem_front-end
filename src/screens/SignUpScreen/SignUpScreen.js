import React, { Component } from 'react';

import './SignUpScreen.scss';
// import fontAwesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope, faLock, faPen, faCalendarAlt, faPhoneAlt, faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
// import axios
import Axios from 'axiosConfig';
// import react-router-dom
import { Link } from 'react-router-dom';
// import components
import FacebookAuthButton from 'components/FacebookAuthButton';
import GoogleAuthButton from 'components/GoogleAuthButton';
import NotificationModal from 'components/NotificationModal';

class SignUpScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      birthDate: '',
      phoneNumber: '',
      email: '',
      password: '',
      rePassword: '',
      showPass: false,
      showConfirm: false,

      error: '',
      success: false,
      openNotificationModal: false,
    };
  }

  componentDidMount() {
    const { history } = this.props;
    // check if user was log in
    if (localStorage.getItem('token')) {
      history.push('/');
    }
    window.document.title = this.props.pageTitle + ' - Đăng Ký';
  }

  // Set Open or Close Notification Modal
  setOpenNotificationModal = () => {
    this.setState({ openNotificationModal: true });
  }

  setDisableNotificationModal = () => {
    this.setState({ openNotificationModal: false });
  }
  // ----------------------------------------------------
  
  handleTextChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  showPassword = (e) => {
    const { showPass } = this.state;
    this.setState({showPass: !showPass});
  }

  showConfirm = (e) => {
    const { showConfirm } = this.state;
    this.setState({showConfirm: !showConfirm});
  }

  handleSubmitAccount = (e) => {
    e.preventDefault();
    
    const { name, birthDate, phoneNumber, email, password, rePassword } = this.state;

    if (password !== rePassword) {
      this.setState({ 
        error: {
          response: {
            status: '',
            data: {
              message: 'Mật khẩu không trùng khớp',
            }
          }
        }
      });
      this.setOpenNotificationModal();
      return;
    } else {
      const user = {
        email: email,
        password: password,
        name: name,
        birthDate: birthDate,
        phoneNumber: phoneNumber,
      };
      Axios.axiosInstance.post('/register/email/validate', user)
      .then((res) => {
        this.setState({ success: true });
        this.setOpenNotificationModal();
      })
      .catch((err) => {
        this.setState({
          error: {
            response: err.response,
          },
        });
        this.setOpenNotificationModal();
      });
    }
  };

  render() {
    const { history } = this.props;
    const { name, 
            birthDate, 
            phoneNumber, 
            email, 
            password, 
            rePassword, 
            showPass, 
            showConfirm,
            error,
            success,
            openNotificationModal
            } = this.state;

    return (
      <React.Fragment>
        <div className="SignUpForm">
          <div className="SignUpForm__title">Đăng Ký</div>

          {/* Hộp nội dung */}
          <div className="SignUpForm__content">
            {/* Logo */}
            <div className="SignUpForm__logo--icon">
              <img className="SignUpForm__logo" src="/assets/ico/icon_GOVN.ico" alt="Logo"/>
            </div>

            <div className="SignUpForm__form">
              {/* Đăng ký bằng facebook, google , twitter */}
              <div className="SignUpForm__social">
                <div className="SignUpForm__social--title">Đăng nhập bằng : </div>
                <div className="SignUpForm__social--btn">
                  <FacebookAuthButton history={history}/>
                  <GoogleAuthButton history={history}/>
                </div>
              </div>

              {/* Thanh chia Or nằm giữa  */}
              <div className="SignUpForm__separate">
                <hr />
                <span>Hoặc</span>
                <hr />
              </div>

              {/* Form nhập */}
              <form onSubmit={this.handleSubmitAccount}>
                <div className="SignUpForm__input--form">
                  <FontAwesomeIcon className="SignUpForm__icon" icon={faPen}/>
                  <input
                    className="SignUpForm__input"
                    type="text"
                    placeholder="Họ tên ..."
                    name="name"
                    onChange={this.handleTextChange}
                    value={name}
                    required
                  />
                </div>
                <div className="SignUpForm__input--form">
                  <FontAwesomeIcon className="SignUpForm__icon" icon={faCalendarAlt}/>
                  <div className='Input__Description'>Ngày Sinh: </div>
                  <input
                    className="SignUpForm__input Date__input"
                    type="date"
                    placeholder="Ngày sinh ..."
                    name="birthDate"
                    onChange={this.handleTextChange}
                    value={birthDate}
                    required
                  />
                </div>
                <div className="SignUpForm__input--form">
                  <FontAwesomeIcon className="SignUpForm__icon" icon={faPhoneAlt} />
                  <input
                    className="SignUpForm__input"
                    type="tel"
                    placeholder="Số điện thoại ..."
                    name="phoneNumber"
                    onChange={this.handleTextChange}
                    value={phoneNumber}
                    required
                  />
                </div>
                <div className="SignUpForm__input--form">
                  <FontAwesomeIcon className="SignUpForm__icon" icon={faEnvelope} />
                  <input
                    className="SignUpForm__input"
                    type="email"
                    placeholder="Địa chỉ Email ..."
                    name="email"
                    onChange={this.handleTextChange}
                    value={email}
                    required
                  />
                </div>
                <div className="SignUpForm__input--form">
                  <FontAwesomeIcon className="SignUpForm__icon" icon={faLock} />
                  <input
                    className="SignUpForm__input"
                    type={`${showPass ? "text":"password"}`}
                    placeholder="Mật khẩu ..."
                    name="password"
                    onChange={this.handleTextChange}
                    value={password}
                    required
                  />
                  <FontAwesomeIcon className="SignUpForm__icon" icon={showPass ? faEye:faEyeSlash} onClick={this.showPassword}/>
                </div>
                <div className="SignUpForm__input--form">
                  <FontAwesomeIcon className="SignUpForm__icon" icon={faLock} />
                  <input
                    className="SignUpForm__input"
                    type={`${showConfirm ? "text":"password"}`}
                    placeholder="Xác nhận mật khẩu ..."
                    name="rePassword"
                    onChange={this.handleTextChange}
                    value={rePassword}
                    required
                  />
                  <FontAwesomeIcon className="SignUpForm__icon" icon={showConfirm ? faEye:faEyeSlash} onClick={this.showConfirm}/>
                </div>

                {/* Button Đăng ký */}
                <button className="SignUpForm__btn" type='submit'> Đăng ký </button>
              </form>

              {/* Chuyển màn hình đăng nhập */}
              <div className="SignUpForm__redirect--login">
                Bạn đã có tài khoản?
                <Link to="/authentication/signin" className="SignUpForm__redirect--btn">
                  Đăng nhập ngay
                </Link>
              </div>

              {/* Modal Thông Báo Lỗi */}
              { openNotificationModal && 
                <NotificationModal 
                  success={success} 
                  error={error} 
                  history={history}
                  typeCheck='SignUp'
                  openNotificationModal={openNotificationModal}
                  setDisableNotificationModal={this.setDisableNotificationModal}
                />
              }
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default SignUpScreen;
