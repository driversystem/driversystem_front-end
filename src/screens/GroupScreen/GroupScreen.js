import React from 'react';
import './GroupScreen.scss';
import "./GroupScreen.scss";

// components
import GroupBar from 'components/GroupBar';
import GroupBarEmpty from 'components/GroupBarEmpty';
import NewGroupDialog from 'components/NewGroupDialog';
import InviteDialog from 'components/InviteDialog';
import GroupInfoDialog from 'components/GroupInfoDialog';
import GroupImageContent from 'components/GroupImageContent';
import GroupAppointmentsContent from 'components/GroupAppointmentsContent';
import GroupManagementContent from 'components/GroupManagementContent';
import UserCard from 'components/UserCard';
import PendingInvitationDialog from 'components/PendingInvitationDialog';

// import redux
import { connect } from 'react-redux';
import selectors from 'selectors';
import actions from 'actions';

// import axios
import Axios from 'axiosConfig';

// firebase storage
import { storageRef, firebase } from 'firebaseConfig';

// material ui
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';

class GroupScreen extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			groups: [],
            authority: 3,
            currentGroupInfo: null,
            newGroup: {
                name: '',
                description: '',
                member: []
            },
            alertObject: {
                message: '',
                severity: "error"
            },
            invites: [],
            candidates: [],
            pendingInvitations: [],
			currentGroup: 0,
            currentTab: 0,
            newGroupDialogActive: false,
            newMemberDialogActive: false,
            memberInspectActive: false,
            groupInfoChangeActive: false,
            uploadAvatarActive: false,
            invitationDialogActive: false,
            isLoading: true,
            alerting: false
		}
    }

    componentDidMount() {
        Axios.axiosInstance.get('/user/groups')
            .then(res => {
                const { groups } = res.data;

                this.setState({
                    groups: groups,
                });
            })
            .then(() => {
                Axios.axiosInstance.get('/user/invitation')
                    .then(res => {
                        const {invitations} = res.data;

                        this.setState({
                            pendingInvitations: invitations,
                            isLoading: false
                        })
                    })
            })
    }

    getUserAuthority = (group) => {
        const { currentUser } = this.props;
        let authority = 1;

        if (group.groupOwner._id === currentUser._id) {
            authority = 3;
        }

        group.groupModerators.forEach(mod => {
            if (mod._id == currentUser._id) {
                authority = 2;
            }
        });

        return authority;
    }

    getMemberAuthority = (group, member) => {
        const memberID = member._id;
        let authority = 1;

        if (group.groupOwner === memberID) {
            authority = 3;
        }

        group.groupModerators.forEach(mod => {
            if (mod._id == memberID) {
                authority = 2;
            }
        });

        return authority;
    }

    // chuyển đổi giứa các group với nhau, gọi api để lấy info của group được focus
	onGroupSelect = (index) => {
        if (this.state.groups.length > index) {
            this.setState({
                isLoading: true
            })

            const { currentUser } = this.props;
            const currentGroupID = this.state.groups[index]._id;
            Axios.axiosInstance.get('/group/' + currentGroupID)
                .then(res => {
                    const { group } = res.data;
                    const authority = this.getUserAuthority(group);

                    this.props.setCurrentGroupID(group._id);

                    this.setState({
                        currentGroupInfo: group,
                        currentGroup: index,
                        authority: authority,
                        isLoading: false
                    });
                });
        }
	}

    // chuyển giữa các tab content
	onTabSelect = (index) => {
		this.setState({
			currentTab: index
		})
    }

    // mở dialog new group
    onNewGroup = () => {
        this.setState({
            newGroupDialogActive: true
        })
    }

    // mở dialog mời thành viên
    onInvite = () => {
        this.setState({
            newMemberDialogActive: true
        })
    }

    onInvitations = () => {
        this.setState({
            invitationDialogActive: true
        })
    }

    onChangeGroupInfo = () => {
        this.setState({
            groupInfoChangeActive: true
        })
    }

    // upload avatar on
    onStartUpload = () => {
        this.setState({
            uploadAvatarActive: true
        })
    }

    // đóng tất cả các dialog
    closeDialog = () => {
        this.setState({
            newGroupDialogActive: false,
            newMemberDialogActive: false,
            memberInspectActive: false,
            groupInfoChangeActive: false,
            uploadAvatarActive: false,
            invitationDialogActive: false
        })
    }

    newGroupName = (e) => {
        this.setState({
            newGroup: {
                ...this.state.newGroup,
                name: e.target.value
            }
        });
    }

    newGroupDescription = (e) => {
        this.setState({
            newGroup: {
                ...this.state.newGroup,
                description: e.target.value
            }
        });
    }

    // dùng để handle thêm user vào this.state.newGroup.member
    handleAddMember = (members) => {
        let group = this.state.newGroup;
        group.member = members;

        this.setState({
            newGroup: group
        });
    }

    // tạo nhóm mới
    // body của request dựa vào field newGroup của state
    createGroup = () => {
        let members = [];
        this.state.newGroup.member.map(member => {
            members.push(member._id);
        })

        const group = {
            groupName: this.state.newGroup.name,
            description: this.state.newGroup.description,
            members: members
        }

        if (group.groupName.length < 6) {
            const message = "Tên nhóm không được nhỏ hơn 6 ký tự";
            
            this.closeDialog();
            this.handleAlert(false, message);
        } else {
            Axios.axiosInstance.post('/group', group)
                .then(res => {
                    const {group} = res.data;

                    let groups = this.state.groups.concat(group);
                    const index = groups.indexOf(group);

                    let focusMember = group.groupOwner;
                    focusMember.authority = 3;
                    group.focusMember = focusMember;
                    
                    this.setState({
                        groups: groups,
                        newGroupDialogActive: false,
                        currentGroup: index,
                        currentGroupInfo: group,
                    })

                    const message = "Tạo nhóm thành công!";
                    this.handleAlert(true, message);
                });
        }
    }

    // Used for alerting shits
    handleAlert = (success, message) => {
        const severity = success ? "success" : "error";
        this.setState({
            alerting: true,
            alertObject: {
                ...this.state.alertObject,
                message: message,
                severity: severity
            }
        })
    }

    // close alert
    closeAlert = () => {
        this.setState({
            alerting: false
        })
    }

    renderAlert = () => {
        return (
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                open = {this.state.alerting}
                autoHideDuration={6000}
                onClose = {this.closeAlert}
            >
                <Alert 
                    onClose = {this.closeAlert} 
                    severity = {this.state.alertObject.severity}
                    elevation = {6}
                    variant = "filled"
                >
                    {this.state.alertObject.message}
                </Alert>
            </Snackbar>
        )
    }

    // modify mảng user cần được mời của state
    handleInvite = (users) => {
        this.setState({
            invites: users
        })
    }

    // Gửi request deer mời user cho nhóm mới được tạo
    // Số lần gửi request tương ứng với số user được add vào lúc điền form
    sendInvites = () => {
        const users = this.state.invites;
        const newGroupID = this.state.currentGroupInfo._id;

        users.forEach(member => {
            Axios.axiosInstance.put('/group/' + newGroupID + '/invite' + '?id=' + member._id)
                .then(res => {
                    const { pendingInvitations } = res.data;

                    this.setState({
                        currentGroupInfo: {
                            ...this.state.currentGroupInfo,
                            pendingInvitations: pendingInvitations
                        }
                    })
                })
        })

        this.setState({
            invites: [],
            newMemberDialogActive: false
        });
    }

    // Dùng đề focus 1 user của nhóm
    // Trigger -> onClick của thẻ user ở sidebar
    onMemberFocus = (member) => {
        let group = this.state.currentGroupInfo;
        group.focusMember = member;
        group.focusMember.authority = 1;

        if (member._id == this.state.currentGroupInfo.groupOwner._id) {
            group.focusMember.authority = 3;
        }

        this.state.currentGroupInfo.groupModerators.forEach(mod => {
            if (mod._id == member._id) {
                group.focusMember.authority = 2;
            }
        })

        this.setState({
            currentGroupInfo: group,
            memberInspectActive: true
        });
    }

    // Lấy các user phù hợp với kết quả search
    getCandidateUsers = (e, groupID) => {
        const string = e.target.value;

        if (string.length != 0) {
            Axios.axiosInstance.get('/user/search?q=' + string + '&excludeGroup=' + groupID)
                .then(res => {
                    const { users } = res.data;

                    this.setState({
                        candidates: users
                    });
                })
        }
    }

    getFocusGroup = () => {
        if (this.state.groups.length != 0) {
            const currentGroupID = this.state.groups[this.state.currentGroup] ?
                 this.state.groups[this.state.currentGroup]._id : this.state.groups[0]._id;
            Axios.axiosInstance.get('/group/' + currentGroupID)
                .then(res => {
                    const { group } = res.data;
                    const authority = this.getUserAuthority(group);

                    let focusMember = group.groupOwner;
                    focusMember.authority = 3;
                    group.focusMember = focusMember;

                    this.setState({
                        currentGroupInfo: group,
                        authority: authority,
                        isLoading: false
                    });
                });
        }
    }

    // request a description change for group
    changeGroupDescription = () => {
        const requestBody = {
            description: this.state.currentGroupInfo.description
        }
        const groupID = this.state.currentGroupInfo._id;

        Axios.axiosInstance.put('/group/' + groupID, requestBody)
            .then(res => {
                const { group } = res.data;

                this.setState({
                    currentGroupInfo: group,
                    groupInfoChangeActive: false
                });
            });
    }

    // cancel change decription
    cancelChangeDescription = () => {
        this.getFocusGroup();
        this.closeDialog();
    }

    // handle user input for change description dialog
    descriptionOnChange = (e) => {
        const description = e.target.value;

        this.setState({
            currentGroupInfo: {
                ...this.state.currentGroupInfo,
                description: description
            }
        })
    }

    // get current group invitations
    getCurrentGroupInvitations = () => {
        const groupID = this.state.currentGroupInfo._id;

        Axios.axiosInstance.get('/group/' + groupID + '/invitation')
            .then(res => {
                const { invitations } = res.data;

                this.setState({
                    currentGroupInfo: {
                        ...this.state.currentGroupInfo,
                        pendingInvitations: invitations
                    }
                })
            })
    }

    // remove invitation
    removeInvitation = (invitationID) => {
        const groupID = this.state.currentGroupInfo._id;

        Axios.axiosInstance.post('/group/' + groupID + '/invitation/remove?invitation=' + invitationID)
            .then(res => {
                const {group} = res.data;

                this.setState({
                    currentGroupInfo: group,
                })
            })
    }

    // leave group
    onLeave = () => {
        const groupID = this.state.currentGroupInfo._id;
        this.setState({
            isLoading: true,
        })

        Axios.axiosInstance.post('/group/' + groupID + '/leave')
            .then(res => {
                const {user} = res.data;
                this.props.setCurrentUser(user);

                const groups = this.state.groups;
                const index = this.state.groups.indexOf(group => group._id == groupID);
                groups.splice(index, 1);

                groups.length != 0 ?
                this.setState({
                    groups: groups, 
                    currentGroupInfo: null,
                    currentGroup: 0
                }) :
                this.setState({
                    groups: [],
                    currentGroupInfo: null,
                });
                
                this.getFocusGroup();
            });
    }

    // accept appointment
    acceptAppointment = (event, appointmentID) => {
        event.stopPropagation();
        Axios.axiosInstance.post('/appointment/' + appointmentID + '/accept')
            .then(res => {
                let { appointment } = res.data;
                let { currentUser } = this.props;

                currentUser.acceptedAppointments.push(appointment._id);
                this.props.setCurrentUser(currentUser);

                let apptList = this.state.currentGroupInfo.appointments;
                apptList.forEach(appt => {
                    if (appt._id == appointment._id) {
                        appt.accepted = appointment.accepted;
                    }
                });

                this.setState({
                    currentGroupInfo: {
                        ...this.state.currentGroupInfo,
                        appointments: apptList
                    }
                })
            })
    }

    // decline appointment
    withdrawAppointment = (event, appointmentID) => {
        event.stopPropagation();
        Axios.axiosInstance.post('/appointment/' + appointmentID + '/withdraw')
            .then(res => {
                let { appointment } = res.data;
                let { currentUser } = this.props;

                currentUser.acceptedAppointments.push(appointment._id);
                this.props.setCurrentUser(currentUser);

                let apptList = this.state.currentGroupInfo.appointments;
                apptList.forEach(appt => {
                    if (appt._id == appointment._id) {
                        appt.accepted = appointment.accepted;
                    }
                });

                this.setState({
                    currentGroupInfo: {
                        ...this.state.currentGroupInfo,
                        appointments: apptList
                    }
                })
            })
    }

    // delete appointment
    deleteAppointment = (event, appointmentID) => {
        event.stopPropagation();
        Axios.axiosInstance.delete('/appointment/' + appointmentID)
            .then(res => {
                const { appointments } = res.data;
                
                this.setState({
                    currentGroupInfo: {
                        ...this.state.currentGroupInfo,
                        appointments: appointments
                    }
                })
            })
    }

    // upload image
    uploadImage = (acceptedFiles) => {
        const promise = new Promise((resolve, reject) => {
            const files = acceptedFiles;
            const newphotos = this.state.currentGroupInfo.photos ? this.state.currentGroupInfo.photos : [];
            const length = newphotos.length + acceptedFiles.length;

            setTimeout(() => {
                files.forEach(photo => {
                    let date = new Date();
                    let groupID = this.state.currentGroupInfo._id;
                    const metadata = { 
                        contentType: 'image/png',
                    }
        
                    const uploadTask = storageRef.child('images/test/' + photo.name + date.toString()).put(photo, metadata);
                    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
                        function(snapshot) {
                            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                            
                            switch (snapshot.state) {
                                case firebase.storage.TaskState.PAUSED: // or 'paused'
                                    break;
                                case firebase.storage.TaskState.RUNNING: // or 'running'
                                    break;
                            }
                        },
                        function(error) {
                            // A full list of error codes is available at
                            // https://firebase.google.com/docs/storage/web/handle-errors
                            switch (error.code) {
                                case 'storage/unauthorized': {
                                    // User doesn't have permission to access the object
                                    break;
                                }
                                case 'storage/canceled': {
                                    // User canceled the upload
                                    break;
                                }
                                case 'storage/unknown': {
                                    // Unknown error occurred, inspect error.serverResponse
                                    break;
                                }
                            }
                        },
                        function() {
                            // Upload completed successfully, now we can get the download URL
                            uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
                                newphotos.push(downloadURL);
                                let body = {
                                    photos: downloadURL
                                }
                    
                                Axios.axiosInstance.put('/group/' + groupID + '/photo/add', body)
                                    .then(() => {
                                        if (newphotos.length == length) {
                                            resolve(newphotos)
                                        }
                                    })
                            });
                        }
                    );
                });
            });
        });

        promise.then((newphotos) => {
            this.setState({
                ...this.state,
                currentGroupInfo: {
                    ...this.state.currentGroupInfo,
                    photos: newphotos
                }
            })
        })
    }

    uploadAvatar = (file) => {
        let promise = new Promise((resolve, reject) => {
            setTimeout(() => {
                let avatar = this.state.currentGroupInfo.avatar ? this.state.currentGroupInfo.avatar : '';
                let groupID = this.state.currentGroupInfo._id;
                const metadata = { 
                    contentType: 'image/png',
                }

                const uploadTask = storageRef.child('images/Group/GroupAvatar/' + this.state.currentGroupInfo._id).put(file[0], metadata);
                uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
                    function(snapshot) {
                        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                        
                        switch (snapshot.state) {
                            case firebase.storage.TaskState.PAUSED: // or 'paused'
                                break;
                            case firebase.storage.TaskState.RUNNING: // or 'running'
                                break;
                        }
                    },
                    function(error) {
                        // A full list of error codes is available at
                        // https://firebase.google.com/docs/storage/web/handle-errors
                        switch (error.code) {
                            case 'storage/unauthorized': {
                                // User doesn't have permission to access the object
                                break;
                            }
                            case 'storage/canceled': {
                                // User canceled the upload
                                break;
                            }
                            case 'storage/unknown': {
                                // Unknown error occurred, inspect error.serverResponse
                                break;
                            }
                        }
                    },
                    function() {
                        // Upload completed successfully, now we can get the download URL
                        uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
                            avatar = downloadURL;
                            let body = {
                                avatar: downloadURL
                            }
                
                            Axios.axiosInstance.post('/group/' + groupID + '/profile/picture', body)
                                .then(() => {
                                    resolve(avatar);
                                })
                        });
                    }
                );
            })
        });

        promise.then((avatar) => {
            let elementIndex = this.state.groups.findIndex(group => group._id == this.state.currentGroupInfo._id);
            let groups = [...this.state.groups];
            
            groups[elementIndex] = {...groups[elementIndex], avatar: avatar};

            this.setState({
                ...this.state,
                groups: groups,
                currentGroupInfo: {
                    ...this.state.currentGroupInfo,
                    avatar: avatar
                },
                uploadAvatarActive: false
            })
        })
    }

    // remove invitation from user's list of pending invitations
    util_user_remvoveinvitationfromlist = (invitation) => {
        let user_pendingInvitations = this.state.pendingInvitations;
        const index = user_pendingInvitations.indexOf(object => object._id = invitation);

        user_pendingInvitations.splice(index, 1);
        this.setState({
            ...this.state,
            pendingInvitations: user_pendingInvitations,
        });
    }

    // accept invitation
    user_invitation_accept = (invitation) => {
        Axios.axiosInstance.put('/user/invitation/accept?id=' + invitation)
            .then(res => {
                const { group } = res.data;

                // concat new group into user's list of groups
                const groups = this.state.groups;
                groups.push(group);

                // remove invitation
                this.util_user_remvoveinvitationfromlist(invitation);
                
                this.setState({
                    ...this.state,
                    groups: groups
                });
            });
    }

    // reject invitation
    user_invitation_reject = (invitation) => {
        Axios.axiosInstance.put('/user/invitation/deny?id=' + invitation)
            .then(res => {
                this.util_user_remvoveinvitationfromlist(invitation);
            });
    }

    // Remove photo
    group_photos_remove = (photo) => {
        const groupID = this.state.currentGroupInfo._id;
        const body = {
            photo: photo
        };

        this.setState({
            isLoading: true,
        })

        Axios.axiosInstance.put('/group/' + groupID + '/photo/remove', body)
            .then(res => {
                const { group } = res.data;
                const photos = group.photos;

                this.setState({
                    ...this.state,
                    currentGroupInfo: {
                        ...this.state.currentGroupInfo,
                        photos: photos
                    },
                    isLoading: false
                });
            })
    }

    // Remove member
    group_member_remove = (member) => {
        const groupID = this.state.currentGroupInfo._id;
        this.closeDialog();
        this.setState({
            isLoading: true
        });

        Axios.axiosInstance.put('/group/' + groupID + '/member/remove?id=' + member)
            .then(res => {
                const { group } = res.data;

                group.focusMember = group.groupOwner;
                group.focusMember.authority = 3;

                this.setState({
                    ...this.state,
                    currentGroupInfo: group,
                    isLoading: false,
                });
            });
    }

    // downgrade member
    group_member_downGrade = (member) => {
        const groupID = this.state.currentGroupInfo._id;

        Axios.axiosInstance.put('/group/' + groupID + '/moderator/derole?mod=' + member)
            .then(res => {
                let { group } = res.data;
                group.focusMember = this.state.currentGroupInfo.focusMember;
                group.focusMember.authority = 1;

                this.setState({
                    ...this.state,
                    currentGroupInfo: group
                });
            })
    }

    // Upgrade member
    group_member_upGrade = (member) => {
        const groupID = this.state.currentGroupInfo._id;

        Axios.axiosInstance.put('/group/' + groupID + '/moderator/add?id=' + member)
            .then(res => {
                let { group } = res.data;
                group.focusMember = this.state.currentGroupInfo.focusMember;
                group.focusMember.authority = 2;

                this.setState({
                    ...this.state,
                    currentGroupInfo: group
                });
            })
    }

    // Navigate to map and show appointment info
    navigate_to_appointment_destination = (event, location) => {
        event.stopPropagation();
        const selectedAppointment = {
            location: location.location,
            locationAddress: location.locationAddress,
        };
        this.props.setSelectedAppointment(selectedAppointment);

        this.props.history.push('/maps');
    }

	render() {
        const users = this.state.candidates;
        const { currentUser } = this.props;

        let images = [];
        if (this.state.groups.length != 0) {
            if (this.state.currentGroupInfo) {
                images = this.state.currentGroupInfo.photos;
            }
        }

        if (this.state.currentGroupInfo == null) {
            this.getFocusGroup();
        }

        const focusMember = this.state.currentGroupInfo != null ? 
            this.state.currentGroupInfo.focusMember : null;


		return(
			<div className='GroupWrapper'>
                {this.state.groups.length  ?
                    <GroupBar 
                        groups={this.state.groups}
                        currentGroup={this.state.currentGroup}
                        currentTab={this.state.currentTab}
                        groupSelect={this.onGroupSelect}
                        tabSelect={this.onTabSelect}
                        newGroup={this.onNewGroup}
                        invite={this.onInvite}
                        focusMember={this.onMemberFocus}
                        onInvitations={this.onInvitations}
                        currentGroupInfo={this.state.currentGroupInfo}
                        myAuthority={this.state.authority}
                        pendingInvitations={this.state.pendingInvitations} />
                    :
                    <GroupBarEmpty
                        newGroup = {this.onNewGroup} />
                }

                {this.state.currentTab == 0 ?
                    <GroupImageContent 
                        group = {this.state.currentGroupInfo}
                        images = {this.state.currentGroupInfo ? this.state.currentGroupInfo.photos : []}
                        upLoad = {this.uploadImage}
                        removeImage = {this.group_photos_remove} /> :
                    <div/>}

                {this.state.currentTab == 1 ?
                    <GroupAppointmentsContent
                        group={this.state.currentGroupInfo}
                        onAccept={this.acceptAppointment}
                        onWithdraw={this.withdrawAppointment}
                        onDelete={this.deleteAppointment}
                        userID={currentUser._id}
                        myAuthority={this.state.authority}
                        selectAppointment={this.navigate_to_appointment_destination} /> :
                    <div/>}

                {this.state.currentTab == 2 ?
                    <GroupManagementContent
                        group={this.state.currentGroupInfo}
                        initChange = {this.onChangeGroupInfo}
                        invitations = {this.state.currentGroupInfo ? this.state.currentGroupInfo.pendingInvitations : []}
                        removeInvitation = {this.removeInvitation}
                        onLeave = {this.onLeave}
                        uploadAvatarActive = {this.state.uploadAvatarActive}
                        onStartUpload = {this.onStartUpload}
                        uploadAvatar = {this.uploadAvatar}
                        closeDialog = {this.closeDialog} /> :
                    <div/>}

                <NewGroupDialog 
                    newGroupDialogActive = {this.state.newGroupDialogActive}
                    newGroup = {this.state.newGroup}
                    users = {users}
                    closeDialog = {this.closeDialog}
                    groupDescription = {this.newGroupDescription}
                    handleAddMember = {this.handleAddMember}
                    createGroup = {this.createGroup}
                    groupName = {this.newGroupName}
                    handleUserInput = {this.getCandidateUsers} />

                <InviteDialog
                    invite = {this.state.newMemberDialogActive}
                    closeDialog = {this.closeDialog}
                    users = {users}
                    handleInvite = {this.handleInvite}
                    sendInvites = {this.sendInvites}
                    handleUserInput = {this.getCandidateUsers}
                    group = {this.state.currentGroupInfo} />


                <UserCard
                    focusMember={focusMember}
                    myAuthority={this.state.authority}
                    open={this.state.memberInspectActive}
                    closeDialog={this.closeDialog}
                    member_demote={this.group_member_downGrade}
                    member_promote={this.group_member_upGrade}
                    member_remove={this.group_member_remove} />

                <GroupInfoDialog
                    groupInfo = {this.state.currentGroupInfo}
                    open = {this.state.groupInfoChangeActive}
                    closeDialog = {this.closeDialog}
                    changeDescription = {this.descriptionOnChange}
                    cancelChange = {this.cancelChangeDescription}
                    commitChange = {this.changeGroupDescription} />

                <PendingInvitationDialog 
                    invitationDialogActive = {this.state.invitationDialogActive}
                    closeDialog = {this.closeDialog}
                    pendingInvitations = {this.state.pendingInvitations}
                    acceptInvitation = {this.user_invitation_accept}
                    rejectInvitation = {this.user_invitation_reject} />

                {
                    this.renderAlert()
                }

                <Backdrop open={this.state.isLoading}> 
                    <CircularProgress />
                </Backdrop>
			</div>
		)
	}
}

const mapStateToProps = state => ({
    currentUser: selectors.getCurrentUser(state),
});

const mapDispatchToProps = dispatch => ({
    setCurrentUser: currentUser => dispatch(actions.setCurrentUser(currentUser)),
    setCurrentGroupID: currentGroupID => dispatch(actions.setCurrentGroupID(currentGroupID)),
    setSelectedAppointment: selectedAppointment => dispatch(actions.setSelectedAppointment(selectedAppointment)),
});

export default connect(mapStateToProps, mapDispatchToProps)(GroupScreen);