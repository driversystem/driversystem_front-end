import React, { PureComponent } from 'react';
import "./SignInScreen.scss";

// import redux
import { connect } from 'react-redux';
import actions from 'actions';
// import material-UI & FontAwesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope, faLock, faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
// import axios
import Axios from 'axiosConfig';
// import react-router-dom
import { Link } from 'react-router-dom';
// import components
import FacebookAuthButton from 'components/FacebookAuthButton';
import GoogleAuthButton from 'components/GoogleAuthButton';
import NotificationModal from 'components/NotificationModal';

class SignInScreen extends PureComponent {
  constructor(props){
    super(props);
    this.state = {
      email:'',
      password:'',
      show: false,
      error: '',
      success: false,
      openNotificationModal: false,
    };
  }

  componentDidMount() {
    const { history } = this.props;
    // check if user was log in
    if (localStorage.getItem('token')) {
      history.push('/');
    }
    window.document.title = this.props.pageTitle + ' - Đăng Nhập'  ;
  }

  // Set Open or Close Notification Modal
  setOpenNotificationModal = () => {
    this.setState({ openNotificationModal: true });
  }

  setDisableNotificationModal = () => {
    this.setState({ openNotificationModal: false });
  }
  // ----------------------------------------------------

  handleTextChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  hiddenPassword = (e) => {
    const { show } = this.state;
    this.setState({
      show: !show,
    })
  }

  handleKeyUp = (e) => {
    const { name, value } = e.target;
    // if(name === 'email') {
    //   const error = {
    //     email: true
    //   }
    // }
    // if(name === 'password') {

    // }
  }

  handleBlurError = (e) => {
  }

  handleSubmitSignIn = (e) => {
    e.preventDefault();

    const { email, password } = this.state;
    const user = { 
      email: email,
      password: password,
    }

    Axios.axiosInstance.post('/auth/email', user)
    .then(res => {
      const { token, user} = res.data;
      const { setCurrentUser } = this.props;
      localStorage.setItem('token', token);
      setCurrentUser(user);
      this.setState({ success: true });
      this.setOpenNotificationModal();
    })
    .catch(err => {
      this.setState({
        error: {
          response: err.response,
        },
      });
      this.setOpenNotificationModal();
    });
  }

  render() {
    const { password, email, show, error, success, openNotificationModal } = this.state;
    const { history } = this.props;
    return (
      <React.Fragment>
        <div className='SignInForm'>
          <div className="SignInForm__title">Đăng Nhập</div>

          {/* Hộp nội dung */}
          <div className="SignInForm__content">
            {/* Logo */}
            <div className="SignInForm__logo--icon">
              <img className="SignInForm__logo" src="/assets/ico/icon_GOVN.ico" alt="Logo"/>
            </div>
            
            <div className="SignInForm__Form">
              {/* Form nhập */}
              <form onSubmit={this.handleSubmitSignIn}>
                <div className="SignInForm__input--form">
                  <FontAwesomeIcon className="SignInForm__icon" icon={faEnvelope}/>
                  <input 
                    className="SignInForm__input" 
                    type="email" 
                    placeholder="Địa chỉ Email ..." 
                    name="email" 
                    onChange={this.handleTextChange}
                    onKeyUp={this.handleKeyUp}
                    onBlur={this.handleBlurError}
                    value={email} 
                    required
                  />
                </div>
                <div className="SignInForm__input--form">
                  <FontAwesomeIcon className="SignInForm__icon" icon={faLock}/>
                  <input 
                    className="SignInForm__input" 
                    type={`${show ? "text":"password"}`}
                    placeholder="Mật khẩu ..." 
                    name="password" 
                    onChange={this.handleTextChange} 
                    onKeyUp={this.handleKeyUp}
                    onBlur={this.handleBlurError}
                    value={password} 
                    required
                  />
                  <FontAwesomeIcon className="SignInForm__icon" icon={show ? faEye:faEyeSlash} onClick={this.hiddenPassword}/>
                </div>
                {/* Button Đăng Nhập */}
                <button className="SignInForm__btn" type='submit'>Đăng nhập</button>
              </form>
              {/* Quên mật khẩu & Đăng Kí */}
              <div className="SignInForm__forgotCreate">
                <Link 
                  to='/authentication/forgotpassword' 
                  className="SignInForm__forgotCreate--btn">
                    Quên mật khẩu
                </Link>
                <Link to='/authentication/signup' className="SignInForm__forgotCreate--btn">Đăng ký</Link>
              </div>

              {/* Đăng nhập bằng facebook, google , twitter */}
              <div className="SignInForm__social">
                <div className="SignInForm__social--title">Đăng nhập bằng : </div>
                <div className="SignInForm__social--btn">
                  <FacebookAuthButton history={history} />
                  <GoogleAuthButton history={history}/>
                </div>
              </div>
              
              { openNotificationModal && 
                <NotificationModal 
                  success={success} 
                  error={error} 
                  history={history}
                  typeCheck='Login'
                  openNotificationModal={openNotificationModal}
                  setDisableNotificationModal={this.setDisableNotificationModal}
                />
              }
            </div>   
          </div>
        </div>
      </React.Fragment>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  setCurrentUser: currentUser => dispatch(actions.setCurrentUser(currentUser)),
});

export default connect(null, mapDispatchToProps)(SignInScreen);
