import React from 'react';
import './GroupMemberIcon.scss';

const GroupMemberIcon = (props) => {
    return (
        <div className='memberIcon' onClick={props.handleClick}>
            <img src={props.avatar}/>
            <div className='memberName'>{props.name}</div>
        </div>
    )
}

export default GroupMemberIcon;