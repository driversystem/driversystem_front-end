import React, { Component } from 'react'

import './GoogleMapComponent.scss';

// import google map key
import { GOOGLE_MAP_KEY } from 'constants/jsFile/urls';
// Import axios
import Axios from 'axiosConfig';

// Import GoogleMap
import { withScriptjs, withGoogleMap, GoogleMap, DirectionsRenderer, TrafficLayer } from "react-google-maps"

// Import Redux
import { connect } from 'react-redux';
import actions from 'actions';
import selectors from 'selectors';

// Import components
import MapSidebar from 'components/MapSidebar';
import ClusterMarker from 'components/ClusterMarker';
import MapAuthenButton from 'components/MapAuthenButton';
import CreateDateModal from 'components/CreateDateModal';
import RedirectSignInModal from 'components/RedirectSignInModal';
import OptionSidebar from 'components/OptionSidebar';

class GoogleMapComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mapRefs: {},

      directions: '',

      zoom: 8,
    };
  }

  // ----------------------------------------------------------------------------------------------------------------------
  // React lifecycle
  componentDidMount(){
    this.getReportsMarker();
    this.getGroupMarker();
    this.interval = setInterval(() => this.getReportsMarker(), 60000);
  }

  componentWillUnmount() {
    const { clearStartingPoint, 
            clearArrivePoint, 
            clearClickMarker, 
            clearChooseMarker,
            clearGasMarkers,
            clearParkingMarkers,
            clearCurrentCenter,
            clearPlaces,
            clearSearchMarkers,
            clearGroupMarker,
            clearOpenTrafficLayer,
            clearOpenSidebar } = this.props;

    clearStartingPoint();
    clearArrivePoint();
    clearClickMarker();
    clearChooseMarker();
    clearGasMarkers();
    clearParkingMarkers();
    clearCurrentCenter();
    clearPlaces();
    clearSearchMarkers();
    clearGroupMarker();
    clearOpenTrafficLayer();
    clearOpenSidebar();
   
    clearInterval(this.interval);
  }

  componentDidUpdate(prevProps, prevState) {
    const { bounds } = this.state;
    const { startingPoint, arrivePoint } = this.props;
    if(prevState.bounds != bounds) {
      this.state.mapRefs.fitBounds(bounds);
    }

    if(startingPoint !== prevProps.startingPoint || arrivePoint !== prevProps.arrivePoint)
    {
      this.directionRender();
    }
  }

  // ----------------------------------------------------------------------------------------------------------------------
  // Map Action

  // mount của map
  onMapMounted = (ref) => {
    this.state.mapRefs = ref;
  }

  // thay đổi khi zoom thay đổi
  onZoomChanged = () => {
    // console.log("GoogleMapComponent -> onZoomChanged -> this.state.mapRef.map.getZoom()", this.state.refs.getZoom())
    // onZoomChange(this.state.mapRef.map.getZoom())
  }
  
  // Thay đổi center
  onCenterChanged = () => {
    const { setCurrentCenter, currentCenter } = this.props;
    const newCenter = {
      lat: this.state.mapRefs.getCenter().lat(),
      lng: this.state.mapRefs.getCenter().lng()
    }
    if(currentCenter.lat !== newCenter.lat || currentCenter.lng !== newCenter.lng) { 
      setCurrentCenter(newCenter);
    }
  }

  // Thay đổi bound khi dùng search box để search location
  changeBoundsWhenSearch = (bounds) => {
    this.setState({bounds});
  }

  // ----------------------------------------------------------------------------------------------------------------------
  // Gọi api lấy địa điểm nhóm bằng location geometry
  getGroupLocationByGeocoder = async (location) => {
    return new Promise((resolve, reject) => { 
      const geocoder = new google.maps.Geocoder();
      geocoder.geocode({location: location}, (results, status) => {
        if (status === "OK") {
          if (results[0]) {
            resolve(results[0])
          } else {
            window.alert("No results found");
          }
        } else {
          window.alert("Geocoder failed due to: " + status);
        }
      })
    })
  }

  // Gọi api lấy địa điểm nhóm bằng location address
  getGroupLocationByPlaceService = async (locationAddress) => {
    return new Promise((resolve, reject) => { 
      const map = this.state.mapRefs.context.__SECRET_MAP_DO_NOT_USE_OR_YOU_WILL_BE_FIRED;
      const placeService = new google.maps.places.PlacesService(map);

      const request = {
        query: locationAddress,
      };

      placeService.textSearch(request, (results, status) => {
        if (status == google.maps.places.PlacesServiceStatus.OK) {
          if (status == google.maps.places.PlacesServiceStatus.OK) {
              resolve(results[0]);
          } else {
            window.alert("Failed to Find Places: " + status);
          }
        } else {
          window.alert("Failed to Find Places: " + status);
        }
      });
    })
  }

  // Gọi hàm set marker địa điểm nhóm
  getGroupMarker = async () => {
    const { selectedAppointment, setGroupMarker } = this.props;
    const { location, locationAddress } = selectedAppointment;
    if(locationAddress) {
      const marker = await this.getGroupLocationByPlaceService(locationAddress);
      const newMarker = {
        id: marker.place_id,
        name: marker.name,
        address: marker.formatted_address,
        geometry: {
          position: {
            lat: marker.geometry.location.lat(),
            lng: marker.geometry.location.lng()
          }
        },
        image: marker.photos,
        checkMarker: 'groupMarker',
        type: '',
        rating: marker.rating,
      };

      const arrayMarkers = new Array;
      arrayMarkers.push(newMarker);
      setGroupMarker(arrayMarkers);
    };

    // if(location) {
    //   const inputLocation = {
    //     lat: location.coordinates[1],
    //     lng: location.coordinates[0],
    //   }
    //   if(inputLocation.lat && inputLocation.lng) {
    //     const marker = await this.getGroupLocationByGeocoder(inputLocation);
    //     console.log("getGroupMarker -> marker", marker)

    //     // const newMarker = {
    //     //   id: marker._id,
    //     //   image: marker.byteImageFile,
    //     //   geometry: {
    //     //     id: marker.geometry._id,
    //     //     position: {lat: marker.geometry.coordinates[1], lng: marker.geometry.coordinates[0]}
    //     //   },
    //     //   description: report.description,
    //     //   checkMarker: 'report',
    //     // }
    //     // this.setState({groupMarker: newMarker});
    //   }
    // }
  }
  // ----------------------------------------------------------------------------------------------------------------------
  // Render Đường đi
  directionRender = () => {
    const { startingPoint, arrivePoint } = this.props;
    if(startingPoint && arrivePoint) {
      const DirectionsService = new google.maps.DirectionsService();
      DirectionsService.route({
        origin: new google.maps.LatLng(startingPoint.geometry.position.lat(), startingPoint.geometry.position.lng()),
        destination: new google.maps.LatLng(arrivePoint.geometry.position.lat(), arrivePoint.geometry.position.lng()),
        travelMode: google.maps.TravelMode.DRIVING,
      }, (result, status) => {
        if (status === google.maps.DirectionsStatus.OK) {
          this.setState({
            directions: result,
          });
        } else {
          console.error(`error fetching directions ${result}`);
        }
      });
    }
  }

  // ----------------------------------------------------------------------------------------------------------------------
  // Gọi api lấy reports marker
  getReportsMarker = () => {
    const { currentCenter } = this.props;
    Axios.axiosInstance.get(`/report/nearby?lat=${currentCenter.lat}&lng=${currentCenter.lng}&radius=25000`)
    .then(res => {
      const { setReportMarkers } = this.props;
      const { reports } = res.data;
      const arrayMarkers = reports.map(report => ({
        id: report._id,
        image: report.byteImageFile,
        geometry: {
          id: report.geometry._id,
          position: {lat: report.geometry.coordinates[1], lng: report.geometry.coordinates[0]}
        },
        description: report.description,
        phoneNumber: report.phoneNumber,
        type: report.type,
        checkMarker: 'report',
      }));
      setReportMarkers(arrayMarkers)
    })
    .catch(err => {
      this.setState({
        error: err,
      });
    });
  }
  
  // ----------------------------------------------------------------------------------------------------------------------
  // Lấy marker khi click lên maps
  getClickMarker = async(location) => {
    return new Promise((resolve, reject) => { 
      const geocoder = new google.maps.Geocoder();
      geocoder.geocode({location: location}, (results, status) => {
        if (status === "OK") {
          if (results[0]) {
            resolve(results[0])
          } else {
            window.alert("No results found");
          }
        } else {
          window.alert("Geocoder failed due to: " + status);
        }
      })
    })
  }

  // Thay đổi khi click vào bản đồ
  onMapClick = async(e) => {
    const location = {
      lat: e.latLng.lat(),
      lng: e.latLng.lng(),
    }
   
    const newMarker = await this.getClickMarker(location);
    let newName = '';
    if(!newMarker.name) {
      newName = newMarker.address_components[0].long_name + ' '+newMarker.address_components[1].long_name;
    } else {
      newName = newMarker.name;
    }
    // setClickMarker khi click trên maps
    const { setClickMarker } = this.props;
    const marker = {
      id: newMarker.place_id,
      name: newName,
      address: newMarker.formatted_address,
      geometry: {
        position: {
          lat: newMarker.geometry.location.lat(),
          lng: newMarker.geometry.location.lng()
        }
      },
      checkMarker: 'clickMarker',
      type: 'street_address',
      rating: newMarker.rating,
    }

    const arrayMarkers = new Array;
    arrayMarkers.push(marker);
    setClickMarker(arrayMarkers);
  }
  // -----------------------------------------------------------------------------------------------
  // lấy địa điểm Option bằng placeService
  getOptionLocation = async (optionLocation) => {
    return new Promise((resolve, reject) => {
      const bounds = this.state.mapRefs.getBounds();
      const map = this.state.mapRefs.context.__SECRET_MAP_DO_NOT_USE_OR_YOU_WILL_BE_FIRED;

      const placeService = new google.maps.places.PlacesService(map);
  
      const request = {
        bounds: bounds,
        query: optionLocation
      }
  
      placeService.textSearch(request, (results, status) => {
        if (status == google.maps.places.PlacesServiceStatus.OK) {
          resolve(results);
        } else {
          window.alert("Failed to Find Places: " + status);
        }
      });
    })
  }

  // Option Location Like Gas Station OR Parking
  onClickOptionLocation = async(optionLocation) => {
    const { setParkingMarkers, setGasMarkers } = this.props;

    const getResult = await this.getOptionLocation(optionLocation);

    switch (optionLocation) {
      case 'gas_station': {
        const arrayMarkers = getResult.map(place => ({
          id: place.place_id,
          name: place.name,
          address: place.formatted_address,
          geometry: {
            position: {
              lat: place.geometry.location.lat(),
              lng: place.geometry.location.lng()
            }
          },
          image: place.photos,
          checkMarker: 'optionMarker',
          type: 'gas_station',
          rating: place.rating,
        }));
        setGasMarkers(arrayMarkers);
        break;
      }
      case 'parking': {
        const arrayMarkers = getResult.map(place => ({
          id: place.place_id,
          name: place.name,
          address: place.formatted_address,
          geometry: {
            position: {
              lat: place.geometry.location.lat(),
              lng: place.geometry.location.lng()
            }
          },
          image: place.photos,
          checkMarker: 'optionMarker',
          type: 'parking',
          rating: place.rating,
        }));
        setParkingMarkers(arrayMarkers);
        break;
      }
    }
  }
  // -----------------------------------------------------------------------------------------------

  // React Component Render
  render() {
    const { parkingMarkers, 
            gasMarkers, 
            reportMarkers, 
            searchMarkers, 
            currentCenter, 
            clickMarker, 
            openAddDateModal, 
            currentUser,  
            openTrafficLayer,
            groupMarker } = this.props;
            
    const { directions } = this.state;
  
    return (
      <React.Fragment>
        <div className='GoogleMapComponent'>
          <GoogleMap
            // mapTypeId='b3eb4ceb449aa55a'
            defaultZoom={15}
            defaultCenter={currentCenter}
            ref={this.onMapMounted}
            onClick={this.onMapClick}
            onZoomChanged={this.onZoomChanged} 
            onCenterChanged={this.onCenterChanged}
          >
            {/* Map side bar */}
            <MapSidebar 
              changeBoundsWhenSearch={this.changeBoundsWhenSearch}
              onClickOptionLocation={this.onClickOptionLocation}
            />

            {/* Option Sidebar open */}
            <OptionSidebar />

            {/* Map authenButton */}
            <MapAuthenButton />

            {/* Map Create Date Modal */}
            { currentUser && <CreateDateModal /> }
            { !currentUser && openAddDateModal && <RedirectSignInModal />}
            
            {/* Click Marker */}
            <ClusterMarker Markers={clickMarker} />

            {/* Search Markers */}
            <ClusterMarker Markers={searchMarkers}/>

            {/* Report Markers */}
            <ClusterMarker Markers={reportMarkers}/>
            
            {/* Parking Markers */}
            <ClusterMarker Markers={parkingMarkers}/>

            {/* Gas Markers */}
            <ClusterMarker Markers={gasMarkers}/>

            {/* Group Marker */}
            <ClusterMarker Markers={groupMarker}/>

            {/* Directions render */}
            {directions && <DirectionsRenderer directions={directions} />}

            {/* traffic layer */}
            {/* Switch tình trạng giao thông */}
            {openTrafficLayer && <TrafficLayer autoUpdate />}
          </GoogleMap>
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  currentUser: selectors.getCurrentUser(state),
  currentCenter: selectors.getCurrentCenter(state),
  startingPoint: selectors.getStartingPoint(state),
  arrivePoint: selectors.getArrivePoint(state),
  searchMarkers: selectors.getSearchMarkers(state),
  clickMarker: selectors.getClickMarker(state),
  reportMarkers: selectors.getReportMarkers(state),
  parkingMarkers: selectors.getParkingMarkers(state),
  gasMarkers: selectors.getGasMarkers(state),
  openAddDateModal: selectors.getOpenAddDateModal(state),
  openTrafficLayer: selectors.getOpenTrafficLayer(state),
  selectedAppointment: selectors.getSelectedAppointment(state),
  groupMarker: selectors.getGroupMarker(state),
});

const mapDispatchToProps = dispatch => ({
  setClickMarker: clickMarker => dispatch(actions.setClickMarker(clickMarker)),
  setSearchMarkers: searchMarkers => dispatch(actions.setSearchMarkers(searchMarkers)),
  setCurrentCenter: currentCenter => dispatch(actions.setCurrentCenter(currentCenter)),
  setReportMarkers: reportMarkers => dispatch(actions.setReportMarkers(reportMarkers)),
  setGasMarkers: gasMarkers => dispatch(actions.setGasMarkers(gasMarkers)),
  setParkingMarkers: parkingMarkers => dispatch(actions.setParkingMarkers(parkingMarkers)),
  setGroupMarker: groupMarker => dispatch(actions.setGroupMarker(groupMarker)),

  clearStartingPoint: () => dispatch(actions.clearStartingPoint()),
  clearArrivePoint: () => dispatch(actions.clearArrivePoint()),
  clearClickMarker: () => dispatch(actions.clearClickMarker()),
  clearChooseMarker: () => dispatch(actions.clearChooseMarker()),
  clearGasMarkers: () => dispatch(actions.clearGasMarkers()),
  clearParkingMarkers: () => dispatch(actions.clearParkingMarkers()),
  clearCurrentCenter: () => dispatch(actions.clearCurrentCenter()),
  clearPlaces: () => dispatch(actions.clearPlaces()),
  clearSearchMarkers: () => dispatch(actions.clearSearchMarkers()),
  clearGroupMarker: () => dispatch(actions.clearGroupMarker()),
  clearOpenTrafficLayer: () => dispatch(actions.clearOpenTrafficLayer()) ,
  clearOpenSidebar: () => dispatch(actions.clearOpenSidebar()),
});

export default connect(mapStateToProps, mapDispatchToProps)(withScriptjs(withGoogleMap(GoogleMapComponent)));