import React from 'react';

import './ProfileButton.scss';

// import material-ui & font awesome
import { Button } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretDown } from '@fortawesome/free-solid-svg-icons'

const ProfileButton = (props) => {
  const { currentUser, toggleOpenDropDown, sticky } = props;
  return (
    <React.Fragment>
      <Button className="Profile__User" onClick={toggleOpenDropDown}>
        <img className="User__avatar" src={currentUser.avatar || `/assets/png/avatar.png`} alt="User avatar"/>
        <div id={`${sticky ? 'Name--sticky' : ''}`} className="User__name">{currentUser.name}</div>
        <FontAwesomeIcon className={`Icon__dropdown ${sticky ? 'icon--sticky' : ''}`} icon={faCaretDown} />
      </Button>
    </React.Fragment>
  );
}

export default ProfileButton;
