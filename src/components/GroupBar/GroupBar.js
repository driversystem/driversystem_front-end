import React from 'react'
import { Link } from 'react-router-dom'
import './GroupBar.scss';

// Minor components
import GroupIcon from 'components/GroupIcon';
import MemberIcon from 'components/GroupMemberIcon';
import GroupFunctionBtn from 'components/GroupFunctionBtn';

// import images and icons
import plus from 'assets/png/plus.png'
import avatar from 'assets/png/avatar.png';
import invite from 'assets/png/invite.png';

class GroupBar extends React.Component {
    renderGroupInfo = (groupInfo) => {
        const activeBtn = this.props.currentTab;
        const members = groupInfo.members.concat(groupInfo.groupModerators, groupInfo.groupOwner);

        return (
            <div className='groupWrapper'>
                <div className='wrapAgain'>
                    <div className='overView'>
                        <b>
                            {groupInfo.groupName}
                        </b>
                        <span>
                            {groupInfo.description}
                        </span>
                    </div>
                    <hr/>
                    <div className='members'>
                        <b>Thành viên</b>
                        {
                            members.map(member => {
                                return (
                                    <MemberIcon
                                        key = {member._id}
                                        avatar = {member.avatar ? member.avatar : avatar}
                                        name = {member.name}
                                        handleClick = {(e) => this.props.focusMember(member)}
                                    />
                                )
                            })
                        }

                        {
                            this.props.myAuthority >= 2 ?
                            <div className="btnInvite" onClick={(e) => this.props.invite()}>
                                Thêm Thành viên
                            </div> : <div />
                        }
                    </div>
                    <hr/>
                    <div className='functionButtons'>
                        <GroupFunctionBtn 
                            btnName = 'Ảnh'
                            active = {activeBtn == 0}
                            onClick={(e) => this.props.tabSelect(0)}/>
                        <GroupFunctionBtn 
                            btnName = 'Buổi hẹn'
                            active = {activeBtn == 1}
                            onClick={(e) => this.props.tabSelect(1)}/>
                        <GroupFunctionBtn 
                            btnName = 'Quản lý nhóm'
                            active = {activeBtn == 2}
                            onClick={(e) => this.props.tabSelect(2)}/>
                    </div>
                    <hr/>
                </div>
                <Link
                    className='btnBack'
                    to="/" >
                    Trở về
                </Link>
            </div>
        )
    }

    renderGroupInfo_empty = () => {
        const activeBtn = this.props.currentTab;

        <div className='groupWrapper'>
            <div className='wrapAgain'>
                <div className='overView'>
                    
                </div>
                <hr/>
                <div className='members'>
                    <b>Thành viên</b>
                    {
                        this.props.myAuthority >= 2 ?
                        <div className="btnInvite" onClick={(e) => this.props.invite()}>
                            Thêm Thành viên
                        </div> : <div />
                    }
                </div>
                <hr/>
                <div className='functionButtons'>
                    <GroupFunctionBtn 
                        btnName = 'Ảnh'
                        active = {activeBtn == 0}
                        onClick={(e) => this.props.tabSelect(0)}/>
                    <GroupFunctionBtn 
                        btnName = 'Buổi hẹn'
                        active = {activeBtn == 1}
                        onClick={(e) => this.props.tabSelect(1)}/>
                    <GroupFunctionBtn 
                        btnName = 'Quản lý nhóm'
                        active = {activeBtn == 2}
                        onClick={(e) => this.props.tabSelect(2)}/>
                </div>
                <hr/>
            </div>
            <Link
                className='btnBack'
                to="/" >
                Trở về
            </Link>
        </div>
    }

    renderInvitation = () => {
        if (this.props.pendingInvitations.length) {
            return (
                <div>
                    <hr />
                    <GroupIcon
                        isSelected = {false}
                        image = {invite}
                        handleClick = {(e) => this.props.onInvitations()}/>
                </div>
            )
        } else {
            return (
                <div/>
            )
        }
    }

    render() {
        const groups = this.props.groups;
        const currentGroup = this.props.currentGroup;
        const groupInfo = this.props.currentGroupInfo;

        return (
            <div className='barWrapper'>
                <div className='groupSelection'>
                    <GroupIcon
                        isSelected = {false}
                        image = {plus}
                        handleClick = {(e) => this.props.newGroup()}/>
                    {
                        groups.map((group, index) => {
                            return (
                                <GroupIcon
                                    key = {group._id}
                                    isSelected = {index == currentGroup ? true : false}
                                    image = {group.avatar ? group.avatar : avatar}
                                    handleClick={(e) => this.props.groupSelect(index)}/>
                            )
                        })
                    }
                    {
                        this.renderInvitation()
                    }
                </div>
                {
                    groupInfo ? 
                    this.renderGroupInfo(groupInfo) : this.renderGroupInfo_empty()
                }
            </div>
        )
    }
}

export default GroupBar;