import React, { Component } from 'react'

import './NavbarHeader.scss';

// import react-router-dom
import { Link } from 'react-router-dom'
// import redux
import { connect } from 'react-redux';
import selectors from 'selectors';
// import components
import Menu from 'components/Menu';

class NavbarHeader extends Component {
  constructor(props){
    super(props);
    this.state = {
      logo: '/assets/ico/icon_logo_header_green.ico',
      sticky: false,
      display: false,
    }
  }

  componentDidMount = () => {
    window.onscroll = () => {
      if(window.pageYOffset > 50) { 
        this.setState({ 
          sticky: true,
          logo: '/assets/ico/icon_logo_header_white.ico',
        })
      } else {
        this.setState({ 
          sticky: false,
          logo: '/assets/ico/icon_logo_header_green.ico',
        })
      }
    } 
  }

  handleLogoClick = (e) => {
    const { history } = this.props;
    history.push('/');
  }

  render() {
    const { currentUser, history } = this.props;
    const { logo, sticky } = this.state;

    return (
      <React.Fragment>
        <div className="NavbarHeader">
          {/* Navbar */}
          <div id={`${sticky ? 'navbar--sticky' : ''}`} className='Header__navbar' >
              {/* Logo */}
              <img className="Navbar__logo" src={logo} alt="App Logo" onClick={this.handleLogoClick}/>

              {/* navigation */}
              <div className="Navbar__navigation">
                {/* Link */}
                <Link id={`${sticky ? 'link--sticky' : ''}`} className='Navigation__link' to='/maps'>Bản đồ trực tuyến</Link> 
                
                { currentUser ? (  
                  <React.Fragment>
                    <Link id={`${sticky ? 'link--sticky' : ''}`} className="Navigation__link" to='/group'>Quản lý nhóm</Link>
                  </React.Fragment>
                  ): (<div/>)
                }

                <Link id={`${sticky ? 'link--sticky' : ''}`} className="Navigation__link" to='/'>Trang chủ</Link>
                <Link id={`${sticky ? 'link--sticky' : ''}`} className="Navigation__link" to='/about'>Về chúng tôi</Link>
                <Link id={`${sticky ? 'link--sticky' : ''}`} className="Navigation__link" to='/contact'>Liên hệ</Link>
              </div> 

              <Menu sticky={sticky} history={history}/>
            </div>
          </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  currentUser: selectors.getCurrentUser(state),
});

export default connect(mapStateToProps, null)(NavbarHeader);