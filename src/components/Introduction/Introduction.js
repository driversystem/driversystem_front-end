import React from 'react'

import './Introduction.scss';

import { Link } from 'react-router-dom';

const Introduction = (props) => {

  return (
    <React.Fragment>
      <div className='Introduction'>
        <div className='Introduction__background'>
          <div className='Introduction__blur'>
            <div className='Introduction__content'>
              {/* Title */}
              <span className='content__title'>GO VN</span>
              <div className='content__intro'>
                <span>NGƯỜI BẠN ĐỒNG HÀNH</span>
                <span>TUYỆT HẢO</span>
              </div>

              {/* navigate */}
              <div className='Introduction__navigate'>
                <Link className="navigate__link link--maps" to='/maps'>Truy cập bản đồ</Link>
                <Link className="navigate__link link--learnMore" to='/learnmore'>Tìm hiểu thêm</Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

export default Introduction;