import React from 'react';
import './GroupAppointmentsContent.scss';

import AppointmentCard from 'components/AppointmentCard';

class GroupAppointmentsContent extends React.Component {
    

    renderAppointments = (appointments) => {
        return (
            <div className='appointments'>
                {
                    appointments.map(appointment => {
                        const powa = this.props.authority >= 2;
                        const location = {
                            location: appointment.location,
                            locationName: appointment.locationName,
                            locationAddress: appointment.locationAddress,
                        }
                        return (
                            <AppointmentCard
                                key={appointment._id} 
                                time={appointment.time}
                                apptID={appointment._id}
                                location={location}
                                description={appointment.reminderMsg}
                                participants={appointment.accepted}
                                onAccept={this.props.onAccept}
                                onWithdraw={this.props.onWithdraw}
                                onDelete={this.props.onDelete}
                                selectAppointment={this.props.selectAppointment}
                                accepted={this.isOnboard(appointment, this.props.userID)}
                                powa={powa}
                            />
                        )
                    })
                }
            </div>
        )
    }

    isOnboard = (appointment, userID) => {
        let onboard = false;
        appointment.accepted.forEach(member => {
            if (member._id == userID) {
                onboard = true;
            }
        });

        return onboard;
    }

    renderAppointments_empty = () => {
        return (
            <div />
        )
    }

    render() {
        const group = this.props.group;

        let appointments = [];
        if (group)
            appointments = group.appointments;

        return (
            <div className='appointmentsWrapper'>
                {
                    appointments.length != 0 ?
                    this.renderAppointments(group.appointments) : this.renderAppointments_empty()
                }
            </div>
        )
    }
}

export default GroupAppointmentsContent;