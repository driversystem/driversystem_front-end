import React, { Component } from 'react'

import './GoogleAuthButton.scss';

import GoogleLogin from 'react-google-login';

// import material-UI & FontAwesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGoogle } from '@fortawesome/free-brands-svg-icons'
// import axios
import Axios from 'axiosConfig';
// import Redux
import { connect } from 'react-redux';
import actions from 'actions';
// import components
import NotificationModal from 'components/NotificationModal';
// import clientId
import { GOOGLE_CLIENT_ID_OAUTH } from 'constants/jsFile/urls';

class GoogleAuthButton extends Component {
  constructor(props){
    super(props);
    this.state = {
      error: {
        response: {
          status: '',
          data: {
            message: '',
          }
        }
      },
      success: false,      
      openNotificationModal: false,
    }
  }
 
  // Set Open or Close Notification Modal
  setOpenNotificationModal = () => {
    this.setState({ openNotificationModal: true });
  }

  setDisableNotificationModal = () => {
    this.setState({ openNotificationModal: false });
  }
  // ----------------------------------------------------

  responseGoogle = (response) => {
    const user = {
      name: response.profileObj.familyName + ' ' + response.profileObj.givenName,
      providerUID: response.profileObj.googleId,
      authProvider: 'google',
    }

    Axios.axiosInstance.post('/auth/provider_auth', user)
    .then((res) => {
      // set token và user
      const { token, user, message } = res.data;
      const { setCurrentUser, history } = this.props;
      localStorage.setItem('token', token);
      setCurrentUser(user);

      if(message === 'Người dùng mới, tạo profile') {
        history.push('/authentication/profile');
      }
      else {
        history.push('/');
      }
    })
    .catch((err) => {
      this.setState({
        error: err,
      });
    });
  }

  responseFailGoogle = (response) => {
    this.setState({ 
      error: {
        response: {
          status: '',
          data: {
            message: 'Xác thực không thành công',
          }
        }
      }
    });
    this.setOpenNotificationModal();
  }
  
  render() {
    const { history } = this.props;
    const { openNotificationModal, success, error } = this.state;

    return (
      <React.Fragment>
        <GoogleLogin
          clientId={GOOGLE_CLIENT_ID_OAUTH} //CLIENTID NOT CREATED YET
          render={renderProps => 
              (
                <button onClick={renderProps.onClick} disabled={renderProps.disabled} className="social__google">
                  <FontAwesomeIcon className="social__icon--style" icon={faGoogle} />
                </button>
              )
          }
          onSuccess={this.responseGoogle}
          onFailure={this.responseFailGoogle}
          cookiePolicy={'single_host_origin'}
        />

        { openNotificationModal && 
          <NotificationModal 
            success={success} 
            error={error} 
            history={history}
            typeCheck='Login'
            openNotificationModal={openNotificationModal}
            setDisableNotificationModal={this.setDisableNotificationModal}
          />
        }
      </React.Fragment>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  setCurrentUser: currentUser => dispatch(actions.setCurrentUser(currentUser)),
});

export default connect(null, mapDispatchToProps) (GoogleAuthButton);
