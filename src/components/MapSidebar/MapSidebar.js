import React, { useState } from 'react'

import './MapSidebar.scss';

// import components
import GoogleSearchBox from 'components/GoogleSearchBox';
import DirectionBox from 'components/DirectionBox';
import ListPlaceSearch from 'components/ListPlaceSearch';
import OptionLocation from 'components/OptionLocation';

const MapSidebar = (props) => {
  const [isOpenDirectionBox, setIsOpen] = useState(false);
  
  const openDirectionBox = (e) => { setIsOpen(!isOpenDirectionBox) }
  
  return (
    <React.Fragment>
      <div className='MapSidebar'>
        <GoogleSearchBox openDirectionBox={openDirectionBox} changeBoundsWhenSearch={props.changeBoundsWhenSearch} />
        <div className='SideBar'>
          {isOpenDirectionBox ? (<DirectionBox />):(<React.Fragment/>)}
          {/* <ListPlaceSearch /> */}
          <OptionLocation onClickOptionLocation={props.onClickOptionLocation} />
          {/* <Location />
          <Collection /> */}
        </div>
      </div>
    </React.Fragment>
  )
}

export default MapSidebar;