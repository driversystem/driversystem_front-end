import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';

import './ImageView.scss';

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
    paper: {
        position: 'absolute',
        display: 'flex',
        maxHeight: '80%',
        maxWidth: '80%',
        outline: 'none',
        overflow: 'scroll',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    button: {
        margin: '5px'
    }
}));

const ImageView = (props) => {
    const classes = useStyles();
    const [modalStyle] = React.useState(getModalStyle);

    return (
        <div>
            <Modal
                open={props.open}
                onBackdropClick={() => props.handleClose()}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
            >
                <div className={classes.paper} style={modalStyle}>
                    <img className='Group__ImageView' src={props.image} />

                    <Button 
                        color="secondary" 
                        variant="contained" 
                        onClick={(e) => props.removeImage(props.image)}
                        className={classes.button} >
                        Xóa ảnh
                    </Button>
                </div>
            </Modal>
        </div>
      );
}

export default ImageView;