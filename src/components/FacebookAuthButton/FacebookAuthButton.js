import React, { Component } from 'react'
import './FacebookAuthButton.scss';
// import facebook Login 
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
// import material-UI & FontAwesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebookF } from '@fortawesome/free-brands-svg-icons'
// import Facebook AppID
import { FACEBOOK_APP_ID } from 'constants/jsFile/urls';
// import axios
import Axios from 'axiosConfig';
// import redux
import { connect } from 'react-redux';
import actions from 'actions';
// import components
import NotificationModal from 'components/NotificationModal';

class FacebookAuthButton extends Component {
  constructor(props){
    super(props);
    this.state = {
      error: {
        response: {
          status: '',
          data: {
            message: '',
          }
        }
      },
      success: false,
      openNotificationModal: false,
    }
  }

  // Set Open or Close Notification Modal
  setOpenNotificationModal = () => {
    this.setState({ openNotificationModal: true });
  }

  setDisableNotificationModal = () => {
    this.setState({ openNotificationModal: false });
  }
  // ----------------------------------------------------
  responseFacebook = (response) => {
    // Lỗi không xác định
    if(response.status === 'unknown'){
      this.setState({ 
        error: {
          response: {
            status: '',
            data: {
              message: 'Lỗi Không Xác Định',
            }
          }
        }
      });
      this.setOpenNotificationModal();
      return;
    }

    // Chưa đăng nhập facebook xác thực
    if(response.status === 'not_authorized') {
      this.setState({ 
        error: {
          response: {
            status: '',
            data: {
              message: 'Tài Khoản Chưa Xác Thực',
            }
          }
        }
      });
      this.setOpenNotificationModal();
      return;
    }
    
    if(response.accessToken) {
      const user = {
        name: response.name,
        providerUID: response.id,
        authProvider: 'facebook',
      }
  
      Axios.axiosInstance.post('/auth/provider_auth', user)
      .then((res) => {
        // set token và user
        const { token, user, message} = res.data;
        const { setCurrentUser, history } = this.props;
        localStorage.setItem('token', token);
        setCurrentUser(user);
        if(message === 'Người dùng mới, tạo profile') {
          history.push('/authentication/profile');
        }
        else {
          history.push('/');
        }
      })
      .catch((err) => {
        this.setState({
          error: err.response,
        });
      });
    }
  }

  render() {
    const { history } = this.props;
    const { openNotificationModal, success, error } = this.state;
    return (
      <React.Fragment>
        <FacebookLogin
          appId={ FACEBOOK_APP_ID }
          fields="name,email,picture"
          callback={this.responseFacebook}
          render={renderProps => (
            <button onClick={renderProps.onClick} className="social__facebook">
              <FontAwesomeIcon className="social__icon--style" icon={faFacebookF} />
            </button>
          )}
        />
        { openNotificationModal && 
          <NotificationModal 
            success={success} 
            error={error} 
            history={history}
            typeCheck='Login'
            openNotificationModal={openNotificationModal}
            setDisableNotificationModal={this.setDisableNotificationModal}
          />
        }
      </React.Fragment>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  setCurrentUser: currentUser => dispatch(actions.setCurrentUser(currentUser)),
});

export default connect(null, mapDispatchToProps)(FacebookAuthButton);