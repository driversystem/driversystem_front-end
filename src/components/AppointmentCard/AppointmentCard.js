import React from 'react';
import './AppointmentCard.scss';
import moment from 'moment';

import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionActions from '@material-ui/core/AccordionActions';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Link from '@material-ui/core/Link';

const AppointmentCard = (props) => {
    const time = moment(props.time, 'YYYY-MM-DD HH:mm:ss Z');
    const timeString = time.format("MMM Do YY [  ] HH:mm");
    const apptID = props.apptID;

    return (
        <div className='apptCard'>
            <Accordion>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-label="Expand"
                    aria-controls="additional-actions1-content"
                    id="additional-actions1-header"
                >
                    <div className='basicInfo'>
                        <Typography
                            className='locationName'
                            paragraph={true}
                        >
                            <Link
                                component="button"
                                color='secondary'
                                variant='h5'
                                onClick={(event) => props.selectAppointment(event, props.location)}
                            >
                                {props.location.locationName}
                            </Link>
                        </Typography>

                        <Typography
                            className='apptTime'
                            paragraph={true}
                            color='initial'
                        >
                            {timeString.toString()}
                        </Typography>
                    </div>
                    <AccordionActions className='actionGroup'>
                        {
                            props.accepted ?
                            <Button 
                                onClick={(event) => props.onWithdraw(event, apptID)}
                                onFocus={(event) => event.stopPropagation()}
                                size='small' 
                                variant="contained" 
                                color='secondary'>Không tham gia</Button> :
                            <Button 
                                onClick={(event) => props.onAccept(event, apptID)}
                                onFocus={(event) => event.stopPropagation()}
                                size='small' 
                                variant="contained" 
                                color='secondary'>Tham Gia</Button>
                        }
                        {
                            props.myAuthority >= 2 ?
                            <Button 
                                onClick={(event) => props.onDelete(event, apptID)}
                                onFocus={(event) => event.stopPropagation()}
                                size='small' 
                                variant="contained" 
                                color='default'>Xóa</Button> : <div/>
                        }
                    </AccordionActions>
                </AccordionSummary>
                <AccordionDetails>
                    <div className='specifications'>
                        <Typography
                            className='descriptionGroup'
                            paragraph={true}
                            color='textSecondary'
                        >
                            {props.description}
                        </Typography>
                        <Divider />
                        <div className='participantList'>
                            <Typography
                                className='apptTime'
                                paragraph={true}
                                color='initial'
                            >
                                Danh sách người tham gia
                            </Typography>
                            {
                                props.participants.length != 0 ?
                                <List className='acceptedUsers'>
                                    {
                                        props.participants.map(member => {
                                            return (
                                                <ListItem>
                                                    <ListItemText primary={member.name} secondary={member.email} />
                                                </ListItem>
                                            )
                                        })
                                    }
                                </List> : <div/>
                            }
                        </div>
                    </div>
                </AccordionDetails>
            </Accordion>
        </div>
    )
}

export default AppointmentCard;