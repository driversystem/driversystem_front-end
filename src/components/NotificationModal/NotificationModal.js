import React, { Component } from 'react'

import './NotificationModal.scss';

// import material-ui
import { Modal, Backdrop, Fade, Button } from '@material-ui/core';

class NotificationModal extends Component {
  constructor(props) { 
    super(props);
    this.state = {
      title: '',
      message: '',
      buttonDescription: '',
    };
  }

  componentDidMount() {
    const { typeCheck, success, error } = this.props;
    switch(typeCheck) {
      case 'Login': {
        if(success) {
          this.setState({ 
            title: 'Thành Công',
            message: 'Đăng Nhập Thành Công',
            buttonDescription: 'Chuyển Hướng',
          });
        }
        else {
          if(error) {
            this.setState({ 
              title: 'Lỗi ' + error.response.status,
              message: error.response.data.message,
              buttonDescription: 'Đóng',
            });
          }
        }
        break;
      }
      case 'SignUp': {
        if(success) {
          this.setState({ 
            title: 'Thành Công',
            message: 'Đăng Ký Thành Công',
            buttonDescription: 'Chuyển Hướng',
          });
        }
        else {
          if(error) {
            this.setState({ 
              title: 'Lỗi ' + error.response.status,
              message: error.response.data.message,
              buttonDescription: 'Đóng',
            });
          }
        }
        break;
      }
      case 'CreateDate': {
        if(success) {
          this.setState({ 
            title: 'Thành Công',
            message: 'Tạo Lịch Hẹn Thành Công',
            buttonDescription: 'Đóng',
          });
        }
        else {
          if(error) {
            this.setState({ 
              title: 'Lỗi ' + error.response.status,
              message: error.response.data.message,
              buttonDescription: 'Đóng',
            });
          }
        }
        break;
      }
      case 'UserInfo': {
        if(success) {
          this.setState({ 
            title: 'Thành Công',
            message: 'Cập Nhật Thông Tin Thành Công',
            buttonDescription: 'Chuyển Hướng',
          });
        }
        else {
          if(error) {
            this.setState({ 
              title: 'Lỗi ' + error.response.status,
              message: error.response.data.message,
              buttonDescription: 'Đóng',
            });
          }
        }
        break;
      }
      case 'Profile': {
        if(success) {
          this.setState({ 
            title: 'Thành Công',
            message: 'Cập Nhật Thành Công',
            buttonDescription: 'Đóng',
          });
        }
        else {
          if(error) {
            this.setState({ 
              title: 'Lỗi ' + error.response.status,
              message: error.response.data.message,
              buttonDescription: 'Đóng',
            });
          }
        }
        break;
      }
      default:
        return;
    }
  }

  // xử lí trong modal
  handleCloseModal = (e) => {
    const { setDisableNotificationModal, success, typeCheck, history } = this.props;
    setDisableNotificationModal();
    switch(typeCheck) {
      case 'Login': {
        if(success) {
          history.push('/');
        }
        break;
      }
      case 'SignUp': {
        if(success) {
          history.push('/authentication/signin');
        }
        break;
      }
      case 'UserInfo': {
        if(success) {
          history.push('/');
        }
        break;
      }
      default:
        return;
    }
  } 

  handleCancel = (e) => {
    const { setDisableNotificationModal } = this.props;
    setDisableNotificationModal();
  }

  render() {
    const { openNotificationModal, typeCheck } = this.props;
    const { message, title, buttonDescription } = this.state;
    return (
      <React.Fragment>
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className="Notification__Modal"
          open={openNotificationModal}
          onClose={this.handleCloseModal}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={openNotificationModal}>
            <React.Fragment>
              <div id="Modal__box" className="Modal__Box">
                <div className="Box__Content">
                  <React.Fragment>
                    <h2 id="transition-modal-title" className="Box__Title">{ title }</h2>
                    <div className="Box__Description">{ message }</div>
                    <div className="Box__Action">
                      <Button className='Btn__close' onClick={this.handleCloseModal}>
                        { buttonDescription }
                      </Button>
                    </div>

                  </React.Fragment>
                </div>
              </div>
            </React.Fragment>
          </Fade>
        </Modal>
      </React.Fragment>
    )
  }
}

export default NotificationModal;