import React from 'react'
import './GroupInfoDialog.scss';

// material UI
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

class GroupInfoDialog extends React.Component {
	renderDialog = (groupInfo) => {
		return (
			<Dialog
				maxWidth='sm'
				fullWidth={true}
				open={this.props.open} 
				onClose={(e) => this.props.closeDialog()} 
				aria-labelledby="form-dialog-title" >
				<DialogTitle id="new-group-dialog-title">Thay đổi thông tin nhóm</DialogTitle>
				<DialogContent>
					<form>
						<TextField
							required
							margin="dense"
							id="groupName"
							label="Tên Nhóm"
							type="text"
                            value={groupInfo.groupName}
                            default={groupInfo.groupName}
							fullWidth />
	
						<TextField 
							multiline
							rows="3"
							margin="dense"
							id="groupDescription"
							label="Lời giời thiệu"
							type="text"
							value={groupInfo.description}
                            onChange={(e) => this.props.changeDescription(e)}
                            default={groupInfo.description}
							fullWidth />
					</form>
				</DialogContent>
				<DialogActions>
					<Button onClick={(e) => this.props.cancelChange()} color="primary" >
						Hủy
					</Button>
					<Button onClick={(e) => this.props.commitChange()} color="primary">
						Xác nhận
					</Button>
				</DialogActions>
			</Dialog>
		)
	}

	renderDialog_empty = () => {
		return (
			<Dialog
				maxWidth='sm'
				fullWidth={true}
				open={this.props.open} 
				onClose={(e) => this.props.closeDialog()} 
				aria-labelledby="form-dialog-title" >
				<DialogTitle id="new-group-dialog-title">Thay đổi thông tin nhóm</DialogTitle>
				<DialogContent>
					<form>
						<TextField
							required
							margin="dense"
							id="groupName"
							label="Tên Nhóm"
							type="text"
							fullWidth />
	
						<TextField 
							multiline
							rows="3"
							margin="dense"
							id="groupDescription"
							label="Lời giời thiệu"
							type="text"
                            onChange={(e) => this.props.changeDescription(e)}
							fullWidth />
					</form>
				</DialogContent>
				<DialogActions>
					<Button onClick={(e) => this.props.cancelChange()} color="primary" >
						Hủy
					</Button>
					<Button onClick={(e) => this.props.commitChange()} color="primary">
						Xác nhận
					</Button>
				</DialogActions>
			</Dialog>
		)
	}

    render() {
		const groupInfo = this.props.groupInfo;

		if (groupInfo) {
			return this.renderDialog(groupInfo);
		} else {
			return this.renderDialog_empty();
		}
	}
}

export default GroupInfoDialog;