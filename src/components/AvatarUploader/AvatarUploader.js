import React, { PureComponent } from 'react'

import './AvatarUploader.scss';

// import fontAwesome & Material
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faImage } from '@fortawesome/free-solid-svg-icons';
import { Fab, Modal, Backdrop, Fade } from '@material-ui/core';
// import constants
import { supportedAvatarExtensions } from 'constants/jsFile/supportFileType';
import { maximumAvatarSize } from 'constants/jsFile/customConstants';
// import react-image-crop
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';

class AvatarUploader extends PureComponent {
  constructor(props) {
    super(props);
    this.state ={
      avatar: '',
      imageCropURL: '',

      error: null,

      openModal: false,
      file: null,
      fileSrc: null,

      crop: {
        unit: '%',
        width: 30,
        aspect: 1/1,
      },
    };
  }

  componentDidMount() {
    this.setState({ avatar: '/assets/png/avatar.png' });
  }

  componentDidUpdate(prevProps) {
    const { userAvatar } = this.props;
    if(userAvatar !== prevProps.userAvatar && !(typeof(userAvatar) == 'object')) {
      if(userAvatar) {
        this.setState({ avatar: userAvatar })
      }
      else {
        this.setState({ avatar: '/assets/png/avatar.png' });
      }
    }
  }

  // Lấy file từ local
  onPickFile = (e) => {
    const { files } = e.target;
    if(files.length === 0) return;

    const file = files[0];

    e.target.value = null;
    // Check xem file phải file ảnh ko
    if (!supportedAvatarExtensions.includes(file.type)) {
      // A modal here to alert that wrong type
      this.setState({
        error: 'Tiệp ảnh tải lên không hợp lệ. Xin hãy thử ảnh khác',
        openModal: true
      })
      return;
    }
    // Check độ lớn của file có vượt quá độ lớn quy định
    if (file.size > maximumAvatarSize.INDIVIDUAL) {
      // A modal here to alert that file size is too large
      this.setState({
        error: `Dung lượng ảnh vượt quá ${(maximumAvatarSize.INDIVIDUAL / 1024 / 1024)}MB. Xin hãy thử ảnh khác`,
        openModal: true
      })
      return;
    }

    // Đọc file
    const reader = new FileReader();
    const url = reader.readAsDataURL(file);
    
    reader.onloadend = (e) => {
      this.setState({ 
        file,
        fileSrc: reader.result,
        openModal: true,
      });
    };
  }

  // tạo ref cho ảnh
  onImageLoaded = image => {
    this.imageRef = image;
  };

  // thay đổi độ lớn crop
  onCropChange = (crop) => {
    this.setState({ crop });
  };
  
  // khi thay đổi crop xong
  onCropComplete = crop => {
    this.makeClientCrop(crop);
  };

  async makeClientCrop(crop) {
    if (this.imageRef && crop.width && crop.height) {
      // Lấy file đã được cắt
      const croppedImageURL = await this.getCroppedImg(
        this.imageRef,
        crop,
      );
      this.setState({ imageCropURL: croppedImageURL });
    }
  }

  // Lấy ảnh đã được crop
  getCroppedImg(image, crop) {
    const canvas = document.createElement("canvas");
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext("2d");

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );

    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          //reject(new Error('Canvas is empty'));
          console.error("Canvas is empty");
          return;
        }
        // Đổi ảnh canvas sang base64
        const dataURL = canvas.toDataURL();
        resolve(dataURL);
      }, "image/jpeg");
    });
  }

  // convert base64 to File
  dataURLtoFile(dataURL) {
    const { currentUser } = this.props;
    let arr = dataURL.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), 
        n = bstr.length, 
        u8arr = new Uint8Array(n);
            
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    let croppedImage = new File([u8arr], currentUser._id, {type:mime});

    return croppedImage;
  }

  // Handle đóng modal
  handleCloseModal = (e) => {
    const { imageCropURL, file } = this.state;
    if(file) {
      // Đổi Base64URL thành file ảnh
      const cropFile = this.dataURLtoFile(imageCropURL);
  
      const { handleSetAvatar } = this.props;
      handleSetAvatar(cropFile);
      this.setState({ avatar: imageCropURL})
    }

    this.setState({
      error: null,
      openModal: false,
      fileSrc: null,
      file: null
    })
  }

  render() {
    const { openModal, error, crop, fileSrc, avatar } = this.state;
    return (
      <div className='AvatarUploader'>
        <img className="AvatarUploader__avatar" src={avatar} alt='User Avatar'/>
        <span className="AvatarUploader__description">Dung lượng tối đa: 3MB</span>
        <input 
          id='pick-avatar' 
          className='AvatarUploader__pickFile' 
          accept="image/jpg, image/jpeg, image/png" 
          name='file' 
          type='file' 
          onChange={this.onPickFile}/>

        <label htmlFor='pick-avatar'>
          <Fab component='span' className="AvatarUploader__CustomPickFile" >
            <FontAwesomeIcon className="CustomPickFile__icon" icon={faImage} />
            <div className='CustomPickFile__label'>Chọn ảnh</div>
          </Fab>
        </label>
        
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className="AvatarUploader__Modal"
          open={openModal}
          onClose={this.handleCloseModal}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={openModal}>
            { fileSrc ? 
              ( 
                <React.Fragment>
                  <div className="Modal__Box">
                    <h2 id="transition-modal-title" className="Box__Title">Chỉnh Sửa Ảnh</h2>
                    <ReactCrop
                      src={fileSrc}
                      crop={crop}
                      ruleOfThirds
                      onImageLoaded={this.onImageLoaded}
                      onComplete={this.onCropComplete}
                      onChange={this.onCropChange}
                    />
                    <button className="Confirm__btn" onClick={this.handleCloseModal}>Xác Nhận</button>
                  </div>
                </React.Fragment>
              ):
              ( 
                <div className="Modal__Box">
                  <h2 id="transition-modal-title" className="Box__Title">Lỗi</h2>
                  <p id="transition-modal-description" className="Box__Content">{error}</p>
                  <button className="Confirm__btn" onClick={this.handleCloseModal}>Xác Nhận</button>
                </div>
              )
            }
          </Fade>
        </Modal>
      </div>
    )
  }
}

export default AvatarUploader;