import React, { Component } from 'react'

import './MarkerInfoWindow.scss';

// import google map 
import { InfoWindow } from 'react-google-maps';

// import Redux
import { connect } from 'react-redux';
import actions from 'actions';
// import selectors from 'selectors';

// import fontAwesome & material-ui
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendarPlus } from '@fortawesome/free-solid-svg-icons';
import { Button, Tooltip } from '@material-ui/core';
import { Rating } from '@material-ui/lab';

const MarkerInfoWindow = (props) => {
  const { handleOpenInfo, setOpenAddDateModal, setChooseMarker, marker } = props;

  const clickOpenAddDate = (e) => {
    setChooseMarker(marker);
    setOpenAddDateModal();
    handleOpenInfo();
  }

  return (
    <React.Fragment>
      <InfoWindow className='InfoWindow' onCloseClick={handleOpenInfo}>
        <div className='InfoWindow__Content' >
          {/* check xem là ảnh của địa điểm */}
          { (marker.image) && ((marker.checkMarker === 'searchMarker') || (marker.checkMarker === 'optionMarker')) && 
            (<img className='Info__image' src={marker.image[0].getUrl()} alt='location image'/>)
          }   
          {/* check xem là ảnh của report */}
          { (marker.image) && (marker.checkMarker === 'report') && 
            (<img className='Info__image' src={`data:image/jpeg;base64, ${marker.image}`} alt='location image'/>)
          }
          {/* thông tin của địa điểm */}
          { marker.name && (<div className='Info__name'>{marker.name}</div>)}
          { marker.address && (<div className='Info__address'>Địa chỉ: {marker.address}</div>)}
          { marker.phoneNumber && (<div className='Info__phoneNumber'>Số điện thoại: {marker.phoneNumber}</div>)}
          { marker.description && (<div className='Info__description'>Mô tả: {marker.description}</div>)}
          { !marker.rating || !(marker.rating >= 0) ||
            (<div className="Info__rating">
              <div>Đánh giá: </div>
              <Rating className="rating" name="Đánh giá" value={marker.rating} readOnly />
            </div>)
          }

          {/* Check nếu địa điểm ko có thông tin */}
          { !marker.image && !marker.address && !marker.phoneNumber && !marker.description && (
            <div className='Info__NoInfo'>Không có thông tin</div>
          )}
       
          {/* Thêm các button cho các hành động như thêm lịch hẹn,... */}
          <div className='Info__actions'>
            { ((marker.checkMarker === 'searchMarker') || (marker.checkMarker === 'clickMarker')) && 
              ( <Tooltip title="Thêm lịch hẹn" aria-label="add">
                  <Button className='AddDate__btn' onClick={clickOpenAddDate}>
                    <FontAwesomeIcon icon={faCalendarPlus} />
                  </Button>
                </Tooltip>)
            }
          </div>
            
        </div>
      </InfoWindow>
    </React.Fragment>
  )
}

const mapDispatchToProps = dispatch => ({
  setOpenAddDateModal: () => dispatch(actions.setOpenAddDateModal()),
  setChooseMarker: chooseMarker => dispatch(actions.setChooseMarker(chooseMarker)),
});

export default connect(null, mapDispatchToProps)(MarkerInfoWindow);