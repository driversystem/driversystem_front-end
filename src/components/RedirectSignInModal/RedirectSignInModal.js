import React from 'react'

import './RedirectSignInModal.scss';

// Import Redux
import { connect } from 'react-redux';
import actions from 'actions';
import selectors from 'selectors';
// import fontAwesome & Material
import { Modal, Backdrop, Fade } from '@material-ui/core';
// import react-router-dom
import { Link } from 'react-router-dom';

const RedirectSignInModal = (props) => {

  const { openAddDateModal, setDisableAddDateModal } = props;

  const handleCloseModal = (e) => {
    setDisableAddDateModal();
  } 

  return (
    <React.Fragment>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className="RedirectSignIn__Modal"
        open={openAddDateModal}
        onClose={handleCloseModal}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={openAddDateModal}>
          <div id="Modal__box" className="Modal__Box">
            <div className="Box__Content">
              <h2 id="transition-modal-title" className="Box__Title">Tạo lịch hẹn</h2>
              <div>Bạn chưa đăng nhập?</div>
              <Link className="Link__signIn" to='/authentication/signin' onClick={handleCloseModal}>Đăng Nhập Ngay</Link>
            </div>
          </div>
        </Fade>
      </Modal>
    </React.Fragment>
  )
}

const mapStateToProps = state => ({
  openAddDateModal: selectors.getOpenAddDateModal(state),
});

const mapDispatchToProps = dispatch => ({
  setDisableAddDateModal: () => dispatch(actions.setDisableAddDateModal()),
});

export default connect(mapStateToProps, mapDispatchToProps)(RedirectSignInModal);