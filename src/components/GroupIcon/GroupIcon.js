import React from 'react';
import './GroupIcon.scss';

const GroupIcon = (props) => {
    if (props.isSelected) {
        return (
            <div className='selectedGroupIcon'>
                <img 
                    src={props.image}
                    onClick = {props.handleClick}
                    />
            </div>
        )
    } else {
        return (
            <div className='groupIcon'>
                <img 
                    src={props.image}
                    onClick = {props.handleClick}
                    />
            </div>
        )
    }
}

export default GroupIcon;