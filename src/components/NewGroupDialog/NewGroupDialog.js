import React from 'react'
import './NewGroupDialog.scss';

// material UI
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

class NewGroupDialog extends React.Component {
	render() {
		return (
			<Dialog
				maxWidth='sm'
				fullWidth={true}
				open={this.props.newGroupDialogActive} 
				onClose={(e) => this.props.closeDialog()} 
				aria-labelledby="form-dialog-title" >
				<DialogTitle id="new-group-dialog-title">Tạo Nhóm</DialogTitle>
				<DialogContent>
					<form>
						<TextField 
							autoFocus
							required
							margin="dense"
							id="groupName"
							label="Tên Nhóm"
							type="text"
							value={this.props.newGroup.name}
							onChange={(e) => this.props.groupName(e)}
							fullWidth />
	
						<Autocomplete
							id="combo-box-demo"
							options={this.props.users}
							default={[]}
							getOptionLabel={(option) => option.name + ' - ' + option.email}
							multiple
							onChange={(event, value) => this.props.handleAddMember(value)}
							renderInput={(params) =>
								<TextField 
									{...params} 
									label="Thêm thành viên" 
									margin="dense"
									type="text"
									onChange={(e) => this.props.handleUserInput(e, '')}
									fullWidth />} />
	
						<TextField 
							multiline
							rows="3"
							margin="dense"
							id="groupDescription"
							label="Lời giời thiệu"
							type="text"
							value={this.props.newGroup.description}
							onChange={(e) => this.props.groupDescription(e)}
							fullWidth />
					</form>
				</DialogContent>
				<DialogActions>
					<Button onClick={(e) => this.props.closeDialog()} color="primary" >
						Cancel
					</Button>
					<Button onClick={(e) => this.props.createGroup()} color="primary">
						Xác nhận
					</Button>
				</DialogActions>
			</Dialog>
		)
	}
}

export default NewGroupDialog;