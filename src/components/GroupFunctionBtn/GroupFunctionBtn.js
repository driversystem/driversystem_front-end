import React from 'react'
import './GroupFunctionBtn.scss';

const GroupFunctionBtn = (props) => {
    if (props.active) {
        return (
            <div className='activeBtn' onClick={props.onClick}>
                {props.btnName}
            </div>
        )
    } else {
        return (
            <div className='normalBtn' onClick={props.onClick}>
                {props.btnName}
            </div>
        )
    }
}

export default GroupFunctionBtn;