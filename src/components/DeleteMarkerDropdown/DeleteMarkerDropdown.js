import React from 'react'

import './DeleteMarkerDropdown.scss';

import onClickOutside from "react-onclickoutside";

// import material-ui
import { LocalGasStation, LocalParking, Room } from '@material-ui/icons';
import { Button } from '@material-ui/core';

// Import Redux
import { connect } from 'react-redux';
import actions from 'actions';

const DeleteMarkerDropdown = (props) => {
  const { setDeleteDropdown } = props;
  
  DeleteMarkerDropdown.handleClickOutside = () => {setDeleteDropdown(false)}

  const handleDeleteAllLocation = () => {
    const {
      clearStartingPoint,
      clearArrivePoint,
      clearClickMarker,
      clearChooseMarker,
      clearGasMarkers,
      clearParkingMarkers,
      clearPlaces,
      clearSearchMarkers,
      clearGroupMarker, } = props;

    clearStartingPoint();
    clearArrivePoint();
    clearClickMarker();
    clearChooseMarker();
    clearGasMarkers();
    clearParkingMarkers();
    clearPlaces();
    clearSearchMarkers();
    clearGroupMarker();

    setDeleteDropdown(false)
  }

  const handleDeleteGasLocation = () => {
    const {clearGasMarkers} = props;
    clearGasMarkers();

    setDeleteDropdown(false)
  }

  const handleDeleteParkingLocation = () => {
    const {clearParkingMarkers} = props;
    clearParkingMarkers();

    setDeleteDropdown(false)
  }

  return (
    <React.Fragment>
      <div className='DeleteMarkerDropdown'>
        <Button className="Delete__Location" name="all" onClick={handleDeleteAllLocation}>
          <Room className='Icon'/>
          <div className="Icon__description">Toàn bộ</div>
        </Button>
        <hr/>
        <Button className="Delete__Location" name="all" onClick={handleDeleteGasLocation}>
          <LocalGasStation className='Icon'/>
          <div className="Icon__description">Trạm xăng</div>
        </Button>
        <hr/>
        <Button className="Delete__Location" name="all" onClick={handleDeleteParkingLocation}>
          <LocalParking className='Icon'/>
          <div className="Icon__description">Bãi giữ xe</div>
        </Button>
      </div> 
    </React.Fragment>
  )
}

const clickOutsideConfig = {
  handleClickOutside: () => DeleteMarkerDropdown.handleClickOutside
};

const mapDispatchToProps = dispatch => ({
  clearStartingPoint: () => dispatch(actions.clearStartingPoint()),
  clearArrivePoint: () => dispatch(actions.clearArrivePoint()),
  clearClickMarker: () => dispatch(actions.clearClickMarker()),
  clearChooseMarker: () => dispatch(actions.clearChooseMarker()),
  clearGasMarkers: () => dispatch(actions.clearGasMarkers()),
  clearParkingMarkers: () => dispatch(actions.clearParkingMarkers()),
  clearPlaces: () => dispatch(actions.clearPlaces()),
  clearSearchMarkers: () => dispatch(actions.clearSearchMarkers()),
  clearGroupMarker: () => dispatch(actions.clearGroupMarker()),
});

export default connect(null, mapDispatchToProps)(onClickOutside(DeleteMarkerDropdown, clickOutsideConfig));