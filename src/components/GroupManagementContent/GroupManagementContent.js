import React from 'react'
import './GroupManagementContent.scss';
import GroupInvitation from 'components/GroupInvitation';

import List from '@material-ui/core/List';
import { ListItemAvatar } from '@material-ui/core';
import Dropzone from 'react-dropzone';
import Backdrop from '@material-ui/core/Backdrop';

// import axios
import Axios from 'axiosConfig';

// image
import avatar from 'assets/png/avatar.png';

class GroupManagementContent extends React.Component {
    renderInvitations = () => {
        const invitations = this.props.invitations;

        return (
            <List>
                {
                    invitations.map(invitation => {
                        return (
                            <GroupInvitation
                                key = {invitation._id}
                                user = {invitation}
                                onClick = {(e) => this.props.removeInvitation(invitation._id)} />
                        )
                    })
                }
            </List>
        )
    }

    renderInvitations_empty = () => {
        return (
            <div className='emptyPrompt'>
                Hiện tại không có lời mời nào
            </div>
        )
    }

    renderManagement = (group) => {
        return (
            <div className='managementWrapper'>
                <Backdrop open={this.props.uploadAvatarActive} onClick={this.props.closeDialog}>
                    <Dropzone onDrop={acceptedfiles => this.props.uploadAvatar(acceptedfiles)}>
                        {({getRootProps, getInputProps}) => (
                            <section>
                                <div {...getRootProps()}>
                                    <input {...getInputProps()} />
                                    <p style={{color: 'white', fontWeight: 'bold', fontSize: '20px', cursor: 'pointer'}}>
                                        Click to choose from explorer.
                                    </p>
                                </div>
                            </section>
                        )}
                    </Dropzone>
                </Backdrop>
                <div className='generalInfo'>
                    <div className='imageSection'>
                        <div className='groupAvatarWrapper' onClick={this.props.onStartUpload}>
                            <img src={group.avatar ? group.avatar : avatar} className='groupAva' />
                        </div>
                    </div>
                    <div className='description'>
                        <h2 className='groupName'>
                            {group.groupName}
                        </h2>
                        <div className='groupDescription' onClick={(e) => this.props.initChange()}>
                            {group.description}
                        </div>
                    </div>
                </div>
                <hr />
                <div className='invitations'>
                    <div className='title'>
                        <span>
                            Lời mời đang chờ phản hồi
                        </span>
                    </div>
                    {
                        this.props.invitations.length != 0 ?
                        this.renderInvitations() : this.renderInvitations_empty()
                    }
                </div>
                <hr />
            </div>
        )
    }

    renderManagement_emtpy = () => {
        return (
            <div className='managementWrapper'>
                <div className='generalInfo'>
                    <div className='imageSection'>
                        <img src={avatar} className='groupAva' />
                    </div>
                    <div className='description'>
                        <h2 className='groupName'>

                        </h2>
                        <div className='groupDescription' onClick={(e) => this.props.initChange()}>
                            
                        </div>
                    </div>
                </div>
                <hr />
                <div className='invitations'>
                    <div className='title'>
                        <span>
                            Lời mời đang chờ phản hồi
                        </span>
                    </div>
                </div>
                <hr />
            </div>
        )
    }

    render() {
        const group = this.props.group;

        return (
            <div className='contentWrapper'>
                {
                    group != null ? this.renderManagement(group) : this.renderManagement_emtpy()
                }
                <div className='leaveBtn' onClick={(e) => this.props.onLeave()}>
                    Rời nhóm
                </div>
            </div>
        )
    }
}

export default GroupManagementContent;