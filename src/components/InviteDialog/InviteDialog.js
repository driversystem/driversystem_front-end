import React from 'react';
import './InviteDialog';

// material ui
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

class InviteDialog extends React.Component {
	renderDialog = (group) => {
		return (
			<Dialog
				maxWidth='sm'
				fullWidth={true}
				open={this.props.invite} 
				onClose={(e) => this.props.closeDialog()} 
				aria-labelledby="form-dialog-title">
				<DialogTitle id="new-group-dialog-title">Gửi lời mời</DialogTitle>
				<DialogContent>
					<form>
						<Autocomplete
							id="combo-box-demo"
							options={this.props.users}
							default={[]}
							getOptionLabel={(option) => option.name + ' - ' + option.email}
							multiple
							onChange={(event, value) => this.props.handleInvite(value)}
							renderInput={(params) =>
								<TextField 
									{...params} 
									label="Tìm người dùng" 
									margin="dense"
									type="text"
									onChange={(e) => this.props.handleUserInput(e, group)}
									fullWidth />} />
					</form>
				</DialogContent>
				<DialogActions>
					<Button onClick={(e) => this.props.closeDialog()} color="primary" >
						Cancel
					</Button>
					<Button onClick={(e) => this.props.sendInvites()} color="primary">
						Xác nhận
					</Button>
				</DialogActions>
			</Dialog>
		)
	}

	renderDialog_empty = () => {
		return (
			<Dialog
				maxWidth='sm'
				fullWidth={true}
				open={this.props.invite} 
				onClose={(e) => this.props.closeDialog()} 
				aria-labelledby="form-dialog-title">
				<DialogTitle id="new-group-dialog-title">Gửi lời mời</DialogTitle>
				<DialogContent>
					<form>
						<Autocomplete
							id="combo-box-demo"
							options={this.props.users}
							default={[]}
							getOptionLabel={(option) => option.name + ' - ' + option.email}
							multiple
							onChange={(event, value) => this.props.handleInvite(value)}
							renderInput={(params) =>
								<TextField 
									{...params} 
									label="Tìm người dùng" 
									margin="dense"
									type="text"
									fullWidth />} />
					</form>
				</DialogContent>
				<DialogActions>
					<Button onClick={(e) => this.props.closeDialog()} color="primary" >
						Cancel
					</Button>
					<Button onClick={(e) => this.props.sendInvites()} color="primary">
						Xác nhận
					</Button>
				</DialogActions>
			</Dialog>
		)
	}

    render() {
		if (this.props.group) {
			const group = this.props.group._id;
			return this.renderDialog(group);
		} else {
			return this.renderDialog_empty();
		}
	}
}

export default InviteDialog;