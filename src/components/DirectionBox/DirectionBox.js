import React, { Component } from 'react';

import './DirectionBox.scss';

// import google map 
import { StandaloneSearchBox } from 'react-google-maps/lib/components/places/StandaloneSearchBox';

// import Redux
import { connect } from 'react-redux';
import actions from 'actions';
// import selectors from 'selectors';

// import material-ui
import { SwapVert } from '@material-ui/icons';
import { IconButton } from '@material-ui/core';

class DirectionBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startingBoxRefs: {},
      arriveBoxRefs: {},
    }
  }

  onStartingBoxMounted = (ref) => {
    this.state.startingBoxRefs = ref;
  }

  onStaringPointChanged = () => {
    const { startingBoxRefs } = this.state;
    
    const places = startingBoxRefs.getPlaces();

    const startingPlaces = places.map(place => ({
      id: place.id,
      name: place.name,
      address: place.formatted_address,
      geometry: {
        position: place.geometry.location
      },
      image: {
        type: 'locationImage',
        imageFile: place.photos,
      },
    }));

    const startingPlace = startingPlaces[0];

    const { setStartingPoint } = this.props;
    setStartingPoint(startingPlace);
  }

  onArriveBoxMounted = (ref) => {
    this.state.arriveBoxRefs = ref;
  }

  onArrivePointChanged = () => {
    const { arriveBoxRefs } = this.state;
    
    const places = arriveBoxRefs.getPlaces();

    const arrivePlaces = places.map(place => ({
      id: place.id,
      name: place.name,
      address: place.formatted_address,
      geometry: {
        position: place.geometry.location
      },
      image: {
        type: 'locationImage',
        imageFile: place.photos,
      },
    }));

    const arrivePlace = arrivePlaces[0];

    const { setArrivePoint } = this.props;
    setArrivePoint(arrivePlace);
  }

  render() {
    return (
      <React.Fragment>
        <div className='DirectionBox'>
          {/* <IconButton className='Icon__swap'><SwapVert /></IconButton> */}
          <div className="PlaceBox__input">
            {/* Starting Box */}
            <StandaloneSearchBox
              ref={this.onStartingBoxMounted}
              onPlacesChanged={this.onStaringPointChanged}
            >
              <input className="Place__input StartingPoint__input" type="text" placeholder="Nhập điểm xuất phát..." />
            </StandaloneSearchBox>

            {/* Arrive Box */}
            <StandaloneSearchBox
              ref={this.onArriveBoxMounted}
              onPlacesChanged={this.onArrivePointChanged}
              >
              <input className="Place__input ArrivePoint__input" type="text" placeholder="Nhập điểm đến..." />
            </StandaloneSearchBox>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

// const mapStateToProps = state => ({
//   // mapRefs: selectors.getMapRefs(state),
// });

const mapDispatchToProps = dispatch => ({
  setStartingPoint: startingPlace => dispatch(actions.setStartingPoint(startingPlace)),
  setArrivePoint: arrivePlace => dispatch(actions.setArrivePoint(arrivePlace)),
});

export default connect(null, mapDispatchToProps)(DirectionBox);