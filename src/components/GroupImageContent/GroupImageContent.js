import React from 'react';
import './GroupImageContent.scss';
import Dropzone from 'react-dropzone';
import { IconButton } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

import GroupImage from 'components/GroupImage';
import Backdrop from '@material-ui/core/Backdrop';
import ImageView from 'components/ImageView';

class GroupImageContent extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            dropActive: false,
            display: false,
            currentImage: ''
        }
    }

    closeDropZone = () => {
        this.setState({
            dropActive: false
        })
    }

    openDropZone = () => {
        this.setState({
            dropActive: true
        })
    }

    displayImage = (source) => {
        this.setState({
            display: true,
            currentImage: source
        })
    }

    closeDisplay = () => {
        this.setState({
            display: false
        })
    }

    renderImages = (images) => {
        return (
            <div className='imageGrid'>
                {
                    images.map((image, index) => {
                        return (
                            <GroupImage
                                key= {index}
                                source = {image}
                                viewImage = {this.displayImage} />
                        )
                    })
                }
            </div>
        )
    }

    onRemove = (photo) => {
        this.props.removeImage(photo);

        this.setState({
            display: false,
        });
    }

    render() {
        const photos = this.props.group ? this.props.group.photos : [];

        return (
            <div className='imageContentWrapper'>
                <div className='buttonContainer'>
                    <IconButton 
                        aria-label='add' 
                        color="secondary" 
                        size="medium" 
                        disabled={this.state.dropActive}
                        onClick = {this.openDropZone} >
                        <AddIcon />
                    </IconButton>
                </div>

                <Backdrop open={this.state.dropActive} onClick={this.closeDropZone}>
                    <Dropzone onDrop={acceptedfiles => this.props.upLoad(acceptedfiles)}>
                        {({getRootProps, getInputProps}) => (
                            <section>
                                <div {...getRootProps()}>
                                    <input {...getInputProps()} />
                                    <p style={{color: 'white', fontWeight: 'bold', fontSize: '20px', cursor: 'pointer'}}>
                                        Click to choose from explorer.
                                    </p>
                                </div>
                            </section>
                        )}
                    </Dropzone>
                </Backdrop>

                {
                    photos.length > 0 ?
                    this.renderImages(photos) : <div/>
                }

                <ImageView
                    open = {this.state.display}
                    handleClose = {this.closeDisplay}
                    image = {this.state.currentImage}
                    removeImage = {this.onRemove} />
            </div>
        )
    }
}

export default GroupImageContent;