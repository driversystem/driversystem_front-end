import React from 'react'
import './Footer.scss'

// import fontAwesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faYoutube, faFacebookF } from '@fortawesome/free-brands-svg-icons'
import { Button } from '@material-ui/core';

import { Link } from 'react-router-dom';

const Footer = (props) => {  
  const handleLogoClick = (e) => {
    const { history } = props;
    history.push('/');
  }

  return (
    <React.Fragment>
      <footer className="Footer">
        <div className="Footer__Content">

          {/* Content */ }
          <div className="Content__container">
            {/* footer bên trái */}
            <div className="Content__left">
              {/* Logo */}
              <img className="Footer__logo" src="/assets/ico/icon_GOVN.ico" alt="App Logo" onClick={handleLogoClick}/>

              {/* Information */}
              <div className="Footer__information">
                <hr />
                <div className="Footer__information--eachDetail"> 
                  <span className="eachDetail__title">Hotline: </span>
                  <span>0785402309</span>
                </div>
                <div className="Footer__information--eachDetail"> 
                  <span className="eachDetail__title">Email: </span>
                  <span>govngroup@gmail.com</span>
                </div>
                <div className="Footer__information--eachDetail"> 
                  <span className="eachDetail__title">Địa chỉ: </span>
                  <span>227 Nguyễn Văn Cừ, Phường 3, Quận 5, TP.Hồ Chí Minh</span>
                </div>
              </div>
            </div>

            {/* footer bên phải */}
            <div className="Content__right">
              <div className="Content__right--title">GO VN</div>
              {/* Link */}
              <Link className="route__link" to='/about'>Về chúng tôi</Link>
              <hr/>
              <Link className="route__link" to='/contact'>Liên hệ</Link>
            </div>
          </div>

          {/* bottom của footer */}
          <div className="Footer__bottom">
            <span>© GO VN 2020</span>
            <div className="Footer__bottom--right">
              <span>Theo dõi chúng tôi tại: </span>
              {/* icon */}
              <Button className="Icon__Social Icon__Social--Facebook" href="https://www.facebook.com/GoVNApp"><FontAwesomeIcon icon={faFacebookF} /></Button>
              <Button className="Icon__Social Icon__Social--Youtube" href="https://www.youtube.com/channel/UCLVT3ql_8_fczjeyEqNaeSQ/featured?view_as=public"><FontAwesomeIcon icon={faYoutube} /></Button>
            </div>
          </div>
        </div> 
      </footer>   
    </React.Fragment>
  )
}

export default Footer;
