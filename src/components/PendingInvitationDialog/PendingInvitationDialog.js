import React from 'react';
import './PendingInvitationDialog.scss';

// material UI
import Avatar from '@material-ui/core/Avatar'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

const PendingInvitationDialog = (props) => {
    return (
        <Dialog
            maxWidth='sm'
            fullWidth={true}
            open={props.invitationDialogActive} 
            onClose={(e) => props.closeDialog()} 
            aria-labelledby="form-dialog-title" >
            <DialogTitle>Lời mời chờ phản hồi</DialogTitle>
            <List>
                {
                    props.pendingInvitations.map(invitation => {
                        return (
                            <ListItem>
                                <ListItemAvatar>
                                    <Avatar alt={invitation.groupName} src={invitation.avatar} />
                                </ListItemAvatar>
                                <ListItemText primary={invitation.groupName} secondary={"Trưởng nhóm: " + invitation.groupOwner.name} />
                                
                                <Button 
                                    onClick={(event) => props.acceptInvitation(invitation._id)}
                                    size='small' 
                                    variant="contained"
                                    color='secondary' >Tham Gia</Button> :
                                <Button 
                                    onClick={(event) => props.rejectInvitation(invitation._id)}
                                    size='small' 
                                    variant="contained" >Hủy</Button>
                            </ListItem>
                        )
                    })
                }
            </List>
            <DialogActions>
                <Button onClick={(e) => props.closeDialog()} color="primary" >
                    Đóng
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default PendingInvitationDialog;