import React from 'react'

import './MenuDropdown.scss';

// import redux
import { connect } from 'react-redux';
import actions from 'actions';
import selectors from 'selectors';
// import react-router-dom
import { Link } from 'react-router-dom'

const MenuDropdown = (props) => {
  const { signOutUser, history, sticky, currentUser, toggleOpenDropDown } = props;

  const handleLogout = (e) => {
    toggleOpenDropDown();
    localStorage.removeItem('token');
    signOutUser();
    if(currentUser.authProvider === 'google') {
      // check Log Out Google
      const auth2 = window.gapi.auth2.getAuthInstance();
      if (auth2) {
        auth2.signOut().then(
          auth2.disconnect().then(this.props.onLogoutSuccess)
        )
      }
    }
    if(currentUser.authProvider === 'facebook') {

    }

    history.push('/');
  }

  return (
    <React.Fragment>
      <div id={`${sticky ? 'dropdown--border': ''}`} className='Information__Dropdown'>
        { currentUser ? 
          (
            <React.Fragment>
              <Link className="Dropdown__actions" to="/profile" onClick={toggleOpenDropDown}>Hồ Sơ</Link>
              <hr className={`${sticky ? 'sticky--line': ''}`}/>
      
              <Link className="Dropdown__actions hide-in-desktop" to="/groups" onClick={toggleOpenDropDown}>Quản Lý Nhóm</Link>
              <hr className={`hide-in-desktop ${sticky ? 'sticky--line': ''}`}/>
      
              <Link className="Dropdown__actions" to="/setting" onClick={toggleOpenDropDown}>Cài Đặt</Link>
              <hr className={`${sticky ? 'sticky--line': ''}`}/>
            </React.Fragment>
          ):
          (
            <React.Fragment>
              <Link className="Dropdown__actions hide-in-desktop" to="/authentication/signup" onClick={toggleOpenDropDown}>Đăng Ký</Link>
              <hr className={`hide-in-desktop ${sticky ? 'sticky--line': ''}`}/>
      
              <Link className="Dropdown__actions hide-in-desktop" to="/authentication/signin" onClick={toggleOpenDropDown}>Đăng Nhập</Link>
              <hr className={`hide-in-desktop ${sticky ? 'sticky--line': ''}`}/>
            </React.Fragment>
          )
        }
        <Link className="Dropdown__actions hide-in-desktop" to="/" onClick={toggleOpenDropDown}>Trang Chủ</Link>
        <hr className={`hide-in-desktop ${sticky ? 'sticky--line': ''}`}/>

        <Link className="Dropdown__actions hide-in-desktop" to="/about" onClick={toggleOpenDropDown}>Về Chúng Tôi</Link>
        <hr className={`hide-in-desktop ${sticky ? 'sticky--line': ''}`}/>

        <Link className="Dropdown__actions hide-in-desktop" to="/contact" onClick={toggleOpenDropDown}>Liên Hệ</Link>

        { currentUser ? 
          (
            <React.Fragment>
              <hr className={`hide-in-desktop ${sticky ? 'sticky--line': ''}`}/>
              <Link className="Dropdown__actions" to="/" onClick={handleLogout}>Đăng Xuất</Link>
            </React.Fragment>
          ):(<React.Fragment/>)
        }
      </div>
    </React.Fragment>
  )
}

const mapStateToProps = state => ({
  currentUser: selectors.getCurrentUser(state),
});

const mapDispatchToProps = dispatch => ({
  signOutUser: () => dispatch(actions.signOutUser()),
});

export default connect(mapStateToProps, mapDispatchToProps)(MenuDropdown);