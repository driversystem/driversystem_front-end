import React from 'react';
import './UserCard.scss';

// material ui
import { makeStyles } from '@material-ui/core/styles';
import { deepOrange, amber, cyan } from '@material-ui/core/colors/';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Avatar from '@material-ui/core/Avatar';

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
		'& > *': {
			margin: theme.spacing(1),
		},
	},
	large: {
		width: theme.spacing(20),
		height: theme.spacing(20),
		color: amber,
		backgroundColor: deepOrange[500],
		margin: 'auto'
	},
	button: {
		margin: 'auto'
	}
}));

const renderDeleteButton = (props, classes) => {
	if (props.myAuthority > props.focusMember.authority) {
		return (
			<Button className={classes.button} onClick={(e) => props.member_remove(props.focusMember._id)} color="primary" >
				Xóa thành viên
			</Button>
		);
	} else {
		return (
			<div/>
		);
	}
}

const renderAuthorityButton = (props, classes) => {
	if (props.myAuthority == 3) {
		if (props.focusMember.authority == 2) {
			return (
				<Button className={classes.button} onClick={(e) => props.member_demote(props.focusMember._id)} color="secondary" >
					Giáng chức
				</Button>
			);
		} else if (props.focusMember.authority == 1) {
			return (
				<Button className={classes.button} onClick={(e) => props.member_promote(props.focusMember._id)} color="secondary" >
					Thăng chức
				</Button>
			);
		} else {
			return (
				<div />
			)
		}
	} else {
		return (
			<div />
		)
	}
}

const getAuthorityTitle = (focusMember) => {
	if (focusMember.authority == 3) {
		return 'Trưởng nhóm';
	} else if (focusMember.authority == 2) {
		return 'Quản trị viên';
	} else {
		return 'Thành viên';
	}
}

const UserCard = (props) => {
	const classes = useStyles();

	let title = '';
	if (props.focusMember) {
		title = getAuthorityTitle(props.focusMember);
	}

	if (props.focusMember) {
		return (
			<Dialog 
				open={props.open}
				maxWidth='sm'
				onClose={(e) => props.closeDialog()} 
				aria-labelledby="form-dialog-title" >
				<DialogTitle id="new-group-dialog-title">{props.focusMember.name + ' - ' + title}</DialogTitle>
				<DialogContent>
					<Avatar 
						alt="Placce Holder"
						className={classes.large}
						src={props.focusMember.avatar}>
							{props.focusMember.name}
					</Avatar>
				</DialogContent>
				<DialogActions>
					{
						renderDeleteButton(props, classes)
					}
					{
						renderAuthorityButton(props, classes)
					}
				</DialogActions>
			</Dialog>
		)
	} else {
		return (
			<Dialog 
				open={props.open}
				maxWidth='sm'
				onClose={(e) => props.closeDialog()} 
				aria-labelledby="form-dialog-title" >
				<DialogTitle id="new-group-dialog-title">Jane Doe</DialogTitle>
				<DialogContent>
					<Avatar 
						alt="Placce Holder"
						className={classes.large}
						/>
				</DialogContent>
				<DialogActions>
					{props.myAuthority > 3 ?
						<Button onClick={(e) => props.closeDialog()} color="primary" >
							Xóa thành viên
						</Button> : <div/>}
				</DialogActions>
			</Dialog>
		)
	}
}

export default UserCard;