import React, { Component } from 'react'

import './ClusterMarker.scss';

// Import GoogleMap
import { MarkerClusterer } from 'react-google-maps/lib/components/addons/MarkerClusterer';

// import components
import Marker from 'components/Marker';
class ClusterMarker extends Component { 
  
  onMarkerClustererClick = (markerClusterer) => {
    const clickedMarkers = markerClusterer.getMarkers();
    // console.log("ClusterMarker -> onMarkerClustererClick -> clickedMarkers", clickedMarkers)
    // console.log(`Current clicked markers length: ${clickedMarkers.length}`)
    // console.log(clickedMarkers)
  }

  render() {
    const { Markers } = this.props;

    let MarkersArray = [];
    if(Markers) {
      MarkersArray = Object.values(Markers);
    }

    return (
      <React.Fragment>
        <div className='ClusterMarker'>
          { MarkersArray ? 
            (
              <div className='Search__marker'>
                <MarkerClusterer
                  // onClick={this.onMarkerClustererClick}
                  averageCenter
                  enableRetinaIcons
                  gridSize={60}
                >
                  {MarkersArray.map((marker) => (
                    <Marker marker={marker}/>
                  ))}
                </MarkerClusterer>
              </div>
            ) : (<React.Fragment/>) 
          }
        </div>
      </React.Fragment>
    )
  }
}

export default ClusterMarker;