import React, { useState } from 'react'

import './MapAuthenButton.scss';

import onClickOutside from "react-onclickoutside";

// import react-router-dom
import { Link } from 'react-router-dom';
// import redux
import { connect } from 'react-redux';
import actions from 'actions';
import selectors from 'selectors';

const MapAuthenButton = (props) => {
  const { currentUser } = props;

  const [openInfo, setOpenInfo] = useState(false);

  const openInfoDropDown = (e) => {setOpenInfo(!openInfo)};

  MapAuthenButton.handleClickOutside = () => {setOpenInfo(false)}
  
  const handleLogout = (e) => {
    const { signOutUser, currentUser } = props;
    localStorage.removeItem('token');
    signOutUser();
    if(currentUser.authProvider === 'google') {
      // check Log Out Google
      const auth2 = window.gapi.auth2.getAuthInstance();
      if (auth2) {
        auth2.signOut().then(
          auth2.disconnect().then(this.props.onLogoutSuccess)
        )
      }
    }
    if(currentUser.authProvider === 'facebook') {

    }
  }

  const openChangeAvatar = (e) => {
    
  }

  return (
    <React.Fragment>
      <div className='MapAuthenButton'>
      { currentUser ? (
          <React.Fragment>
            <img className='User__avatar' src={currentUser.avatar || `/assets/png/avatar.png`} alt='user avatar' onClick={openInfoDropDown} />
          </React.Fragment>
        ): (
          <Link className='AuthenticationButton__navigate' to='authentication/signin'>Đăng Nhập</Link>
        )
      }
      { (openInfo && currentUser) ? 
        (
          <React.Fragment>
            <div className='InfoDropDown'>
              <div className='User__info'>
                <img className='Info__avatar' src={currentUser.avatar || `/assets/png/avatar.png`} alt='user avatar'/>
                <div className='Info__name'>{currentUser.name}</div>
                <div className='Info__email'>{currentUser.email}</div>
              </div>
              <div className='Dropdown__action'>
                <Link className="Action" to="/">Trang Chủ</Link>
                <Link className="Action" to="/profile">Hồ Sơ Cá Nhân</Link>
                <Link className="Action" to="/group">Quản Lý Nhóm</Link>
              </div>
              <div className="LogOut__Button">
                <button className="Button" variant="outlined" onClick={handleLogout}>Đăng Xuất</button>
              </div>
            </div>  
          </React.Fragment>
        ):(<React.Fragment/>)
      }

      </div>
    </React.Fragment>
  )
}

const clickOutsideConfig = {
  handleClickOutside: () => MapAuthenButton.handleClickOutside
};

const mapStateToProps = state => ({
  currentUser: selectors.getCurrentUser(state),
});

const mapDispatchToProps = dispatch => ({
  signOutUser: () => dispatch(actions.signOutUser()),
});

export default connect(mapStateToProps, mapDispatchToProps)(onClickOutside(MapAuthenButton, clickOutsideConfig));