import React, { Component } from 'react'

import './Marker.scss';

// import google map 
import { Marker } from "react-google-maps"

// import components
import MarkerInfoWindow from 'components/MarkerInfoWindow';

class MapMarker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedMarker: null,
      icon: '',
    }
  }

  componentDidMount () { 
    const { marker } = this.props;
    switch(marker.type) {
      case 'traffic': {
        this.setState({
          icon: '/assets/png/traffic_bar_report_trafficjam.png',
        })
        break;
      }
      case 'crash': {
        this.setState({
          icon: '/assets/png/traffic_bar_report_accident.png',
        })
        break;
      }
      case 'hazard': {
        this.setState({
          icon: '/assets/png/traffic_bar_report_hazard.png',
        })
        break;
      }
      case 'help': {
        this.setState({
          icon: '/assets/png/traffic_bar_report_assistance.png',
        })
        break;
      }
      case 'other': {
        this.setState({
          icon: '/assets/png/traffic_bar_report_other.png',
        })
        break;
      }
      case 'parking' : {
        this.setState({
          icon: '/assets/png/parking_marker.png',
        })
        break;
      }
      case 'gas_station' : {
        this.setState({
          icon: '/assets/png/gas_station.png',
        })
        break;
      }   
      default: {
        this.setState({
          icon: marker.icon,
        })
        break;
      }
    }
  }

  handleOpenInfo = (marker) => {    
    this.setState({selectedMarker: marker});
  }

  render() {
    const { marker } = this.props;
    const { selectedMarker, icon } = this.state;

    return (
      <React.Fragment>
        <div className='Marker'>
          <Marker
            key={marker.id}
            position={marker.geometry.position}
            icon={icon}
            onClick={this.handleOpenInfo}
          >
            { ( selectedMarker && 
                ( 
                  (selectedMarker.latLng.lat() === marker.geometry.position.lat && selectedMarker.latLng.lng() === marker.geometry.position.lng) ||
                  (selectedMarker.latLng.lat === marker.geometry.position.lat && selectedMarker.latLng.lng === marker.geometry.position.lng)
                )
              ) ? 
              (
                <MarkerInfoWindow marker={marker} handleOpenInfo={this.handleOpenInfo}/>
              ) : (<React.Fragment/>)
            }
          </Marker>
        </div>
      </React.Fragment>
    )
  }
}

export default MapMarker;
