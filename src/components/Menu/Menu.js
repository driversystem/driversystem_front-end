import React, { useState } from 'react'

import './Menu.scss';

import onClickOutside from "react-onclickoutside";
// import react-router-dom
import { Link } from 'react-router-dom'
// import material-ui
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'
// import redux
import { connect } from 'react-redux';
import selectors from 'selectors';
//import Components
import ProfileButton from 'components/ProfileButton';
import MenuDropdown from 'components/MenuDropdown';

const Menu = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const toggleOpenDropDown = () => setIsOpen(!isOpen);

  Menu.handleClickOutside = () => setIsOpen(false);

  const { currentUser, history, sticky } = props;

  return (
    <React.Fragment>
      <div className="Menu">
        {/* check IF Đăng Nhập or not */}
        { currentUser ? 
          (<ProfileButton 
              className="Navbar__profile" 
              currentUser={currentUser} 
              history={history} 
              toggleOpenDropDown={toggleOpenDropDown}
              sticky={sticky}
            />):
          (
            <React.Fragment>
              <div className="Navbar__AuthenLink">
                <Link id={`${sticky ? 'signup--sticky' : ''}`} to="/authentication/signup" className="AuthenLink__Btn Btn--signup">Đăng Ký</Link>
                <Link id={`${sticky ? 'login--sticky' : ''}`} to="/authentication/signin" className="AuthenLink__Btn Btn--login">Đăng Nhập</Link>
              </div>
              <div className="Navbar__menu" onClick={toggleOpenDropDown}>
                <FontAwesomeIcon id={`${sticky ? 'menu--sticky' : ''}`} className="Menu__icon" icon={faBars} />
              </div>
            </React.Fragment>
          )
        }

       

        {isOpen ? (<MenuDropdown toggleOpenDropDown={toggleOpenDropDown} sticky={sticky} history={history}/>) : (<React.Fragment />)}
      </div>
    </React.Fragment>
  )
}

const mapStateToProps = state => ({
  currentUser: selectors.getCurrentUser(state),
});

const clickOutsideConfig = {
  handleClickOutside: () => Menu.handleClickOutside
};

export default connect(mapStateToProps, null)(onClickOutside(Menu,clickOutsideConfig));
