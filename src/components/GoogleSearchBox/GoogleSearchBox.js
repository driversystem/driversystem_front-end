import React, { Component } from 'react'

import './GoogleSearchBox.scss';

import { StandaloneSearchBox } from 'react-google-maps/lib/components/places/StandaloneSearchBox';
// import material-UI & FontAwesome
import { IconButton, Divider, Icon } from '@material-ui/core';
import { Menu, Directions, Search } from '@material-ui/icons';
// import Redux
import { connect } from 'react-redux';
import actions from 'actions';
// import selectors from 'selectors';

// import components

class GoogleSearchBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchBoxRefs: {},
    };
  }

  onSearchBoxMounted = (ref) => {
    this.state.searchBoxRefs = ref;
  }

  onPlacesChanged = () => {
    const { searchBoxRefs } = this.state;
    const { changeBoundsWhenSearch, setSearchMarkers, setPlaces } = this.props;
    
    const places = searchBoxRefs.getPlaces();
    const bounds = new google.maps.LatLngBounds();

    places.forEach(place => {
      if (place.geometry.viewport) {
        bounds.union(place.geometry.viewport)
      } else {
        bounds.extend(place.geometry.location)
      }
    });

    const arrayMarkers = places.map(place => ({
      id: place.place_id,
      name: place.name,
      address: place.formatted_address,
      geometry: {
        position: {
          lat: place.geometry.location.lat(),
          lng: place.geometry.location.lng(),
        }
      },
      image: place.photos,
      checkMarker: 'searchMarker',
      rating: place.rating
    }));
    
    setSearchMarkers(arrayMarkers);
    setPlaces(places);
    changeBoundsWhenSearch(bounds);
  }

  openSideBarSettings = (e) => {
    const { setOpenSidebar } = this.props;
    setOpenSidebar(true);
  }

  openDirections = (e) => {

  }

  render() {
    const { openDirectionBox } = this.props;
    return (
      <React.Fragment>
        <div className='GoogleSearchBox'>
          <IconButton className='SearchBox__icon icon--menu' style={{ fontSize: 80}}color="primary" aria-label="directions" onClick={this.openSideBarSettings}><Menu /> </IconButton>
          <StandaloneSearchBox
            ref={this.onSearchBoxMounted}
            onPlacesChanged={this.onPlacesChanged}
          >
            <input 
              className='Search__input' 
              type="text" 
              placeholder="Nhập địa chỉ cần tìm ..."
            />
          </StandaloneSearchBox>
          <Icon className='SearchBox__icon icon--search'><Search/></Icon>
          <Divider className='SearchBox__divider' orientation="vertical" />
          <IconButton className='GoogleSearchBox__direction' color="primary" aria-label="directions" onClick={openDirectionBox}><Directions /> </IconButton>
        </div>
      </React.Fragment>
    )
  }
}

// const mapStateToProps = state => ({
//   // mapRefs: selectors.getMapRefs(state),
// });

const mapDispatchToProps = dispatch => ({
  setSearchMarkers: searchMarkers => dispatch(actions.setSearchMarkers(searchMarkers)),
  setPlaces: places => dispatch(actions.setPlaces(places)),
  setOpenSidebar: openSidebar => dispatch(actions.setOpenSidebar(openSidebar)),
});

export default connect(null, mapDispatchToProps)(GoogleSearchBox);