import React from 'react';
import './GroupImage.scss';

const GroupImage = (props) => {
    return (
        <div className='imgWrapper' onClick={() => props.viewImage(props.source)}>
            <img src={props.source}/>
        </div>
    )
}

export default GroupImage;