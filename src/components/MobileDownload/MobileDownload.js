import React, { Component } from 'react';

import './MobileDownload.scss';

class MobileDownload extends Component {
  render() {
    return (
      <React.Fragment>
        <div className='MobileDownload'>

          <div className='Download__content'>
            <img className="Download__logo" src="/assets/ico/icon_GOVN.ico" alt="Logo"/>
            <span>Tải Ứng Dụng Tại</span>
          </div>

          <div className='Download__store'>
            <a className='Store__btn' aria-label='App store' rel="noreferrer" href='https://itunes.apple.com/vn' target='_blank'>
              <img src='/assets/svg/btn_appstore.svg' alt='App store logo'></img>
            </a>
            <a className='Store__btn' aria-label='Play store' rel="noreferrer" href='https://play.google.com/store' target='_blank'>
              <img src='/assets/svg/btn_playstore.svg' alt='Play store logo'></img>
            </a>
          </div>

        </div>
      </React.Fragment>
    )
  }
}

export default MobileDownload;
