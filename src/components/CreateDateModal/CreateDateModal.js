import React, { Component } from 'react'

import './CreateDateModal.scss';

// import package
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
// import fontAwesome & Material
import { Modal, Backdrop, Fade, TextField, MenuItem, Button, Switch } from '@material-ui/core';
import { MuiPickersUtilsProvider, KeyboardTimePicker, KeyboardDatePicker } from '@material-ui/pickers';
// Import Redux
import { connect } from 'react-redux';
import actions from 'actions';
import selectors from 'selectors';
// Import axios
import Axios from 'axiosConfig';
// import react-router-dom
import { Link } from 'react-router-dom';
// import components
import NotificationModal from 'components/NotificationModal';

class CreateDateModal extends Component {
  constructor(props) { 
    super(props);
    this.state = {
      loading: true,
      groups: [],

      idGroupChoose: '',
      selectedDate: new Date,
      message: '',
      groupTravel: false,

      error: '',
      success: false,
      openNotificationModal: false,
    };
  }

  // Set Open or Close Notification Modal
  setOpenNotificationModal = () => {
    this.setState({ openNotificationModal: true });
  }

  setDisableNotificationModal = () => {
    this.setState({ openNotificationModal: false });
  }
  // ----------------------------------------------------

  // -------------------------------------------------------------------------------------
  // React lifecycle
  componentDidMount = async() => {
    const groups = await this.getAllGroups();
    this.setState({
      loading: false,
      groups: groups
    })
  }

  // -------------------------------------------------------------------------------------
  // Call api
  getAllGroups = async() => {
    return new Promise((resolve, reject) => {
      Axios.axiosInstance.get(`/user/groups/isOwner`)
      .then(res => {
        const { groups } = res.data;
        resolve(groups);
      })
      .catch(err => {
        this.setState({
          error: err,
        });
      });
    })
  }


  // -------------------------------------------------------------------------------------
  // xử lí trong modal
  handleCloseModal = (e) => {
    const { setDisableAddDateModal } = this.props;
    setDisableAddDateModal();
  } 

  // Xử lí chọn nhóm
  handleSelectGroupChange = (e) => {
    this.setState({idGroupChoose: e.target.value});
  }

  // Xử lí chọn ngày & giờ
  handleDateChange = (date) => {
    this.setState({selectedDate: date});
  }

  handleSwitchChange = (e) => {
    const { name, checked } = e.target;
    this.setState({[name]: checked});
  }
  // Xử lí Lời nhắn
  handleMessageChange = (e) => {
    this.setState({message: e.target.value});
  }

  // xử lí gửi dữ liệu tạo lịch hẹn về back-end
  handleCreateAppointment = (e) => {
    const { idGroupChoose, selectedDate, message, groupTravel } = this.state;
    const { chooseMarker, setDisableAddDateModal } = this.props;

    // format Time dưới DB: 2020-09-17T12:56:00.000Z
    const selectTime = selectedDate.toISOString();

    let reminderTime = new Date (selectedDate);
    reminderTime.setHours(selectedDate.getHours() - 1);
    reminderTime = reminderTime.toISOString();

    const infoDate = {
      groupID: idGroupChoose,
      time: selectTime,
      reminderTime: reminderTime,
      reminderMsg: message,
      location: {
        coordinates: [
          chooseMarker.geometry.position.lng,
          chooseMarker.geometry.position.lat,
        ],
      },
      locationName: chooseMarker.name,
      locationAddress: chooseMarker.address,
      properties: {
        groupTravel: groupTravel,
      },
      status: 'Pending',
    }
    if(chooseMarker.name) {
      infoDate.locationName = chooseMarker.name;
    }
    Axios.axiosInstance.post(`/appointment/`, infoDate)
    .then(res => {
      this.setState({ success: true });
      this.setOpenNotificationModal();
      setDisableAddDateModal();
    })
    .catch(err => {
      this.setState({
        error: {
          response: err.response,
        },
      });
      this.setOpenNotificationModal();
    });
  }

  // -------------------------------------------------------------------------------------
  render() {
    const { openAddDateModal, chooseMarker } = this.props;
    const { groups, loading, selectedDate, message, idGroupChoose, groupTravel, openNotificationModal, success, error } = this.state;
  
    return (
      <React.Fragment>
        {openAddDateModal &&
        (<Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className="CreateDate__Modal"
          open={openAddDateModal}
          onClose={this.handleCloseModal}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={openAddDateModal}>
            { ( !loading && groups.length === 0 ) ?
              ( <React.Fragment>
                  <div id="Modal__box" className="Modal__Box">
                    <div className="Box__Content">
                      <h2 id="transition-modal-title" className="Box__Title">Tạo lịch hẹn</h2>
                      <div>Bạn chưa có nhóm nào hay bạn không đủ quyền?</div>
                      <Link className="Link__group" to='/group' onClick={this.handleCloseModal}>Đến tạo nhóm nào</Link>
                    </div>
                  </div>
                </React.Fragment>
              ):
              ( <React.Fragment>
                  <div id="Modal__box" className="Modal__Box">
                    <div className="Box__Content">

                      <h2 className="Box__Title">Tạo lịch hẹn</h2>

                      {/* Tên địa điểm */}
                      {chooseMarker.name && 
                        <TextField 
                          disabled 
                          multiline
                          className='Date__location location__name' 
                          id="standard-disabled" 
                          label="Tên Địa Điểm" 
                          defaultValue={chooseMarker.name} 
                        />
                      }

                      {/* Địa chỉ địa điểm */}
                      {chooseMarker.address && 
                        <TextField 
                          disabled 
                          multiline
                          className='Date__location location__address' 
                          id="standard-disabled" 
                          label="Địa Chỉ" 
                          defaultValue={chooseMarker.address} 
                        />
                      }

                      {/* Khung chọn nhóm */}
                      <TextField
                        select
                        required
                        className="Date__Group"
                        id="standard-select-group"
                        label="Chọn Nhóm"
                        helperText="*Bắt buộc"
                        value={idGroupChoose}
                        onChange={this.handleSelectGroupChange}
                      >
                        {groups.map((group) => (
                          <MenuItem key={group._id} value={group._id}>
                            {group.groupName}
                          </MenuItem>
                        ))}
                      </TextField>

                      {/* Chọn ngày và giờ */}
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        {/* Khung chọn ngày */}
                        <KeyboardDatePicker
                          className='Date__PickDate'
                          variant="inline"
                          format="MM/dd/yyyy"
                          margin="normal"
                          id="date-picker-inline"
                          label="Chọn Ngày"
                          value={selectedDate}
                          onChange={this.handleDateChange}
                          KeyboardButtonProps={{
                            'aria-label': 'change date',
                          }}
                        />

                        {/* Khung chọn giờ */}
                        <KeyboardTimePicker
                          className='Date__PickTime'
                          margin="normal"
                          id="time-picker"
                          label="Chọn Giờ"
                          value={selectedDate}
                          onChange={this.handleDateChange}
                          KeyboardButtonProps={{
                            'aria-label': 'change time',
                          }}
                        />
                      </MuiPickersUtilsProvider>
                      
                      {/* Khung input Lời Nhắn */}
                      <TextField
                        className='Date__msg'
                        id="outlined-multiline-static"
                        label="Lời Nhắn"
                        placeholder="Đặt lời nhắn..."
                        multiline
                        rows={4}
                        variant="outlined"
                        value={message}
                        onChange={this.handleMessageChange}
                      />

                      {/* Bật chế độ di chuyển nhóm */}
                      <div className='Date__groupTravel'>
                        <div className='GroupTravel__Title'>Bật Chế Độ Di Chuyển Nhóm: </div>
                        <Switch
                          checked={groupTravel}
                          onChange={this.handleSwitchChange}
                          name="groupTravel"
                          color="primary"
                          inputProps={{ 'aria-label': 'primary checkbox' }}
                        />
                      </div>
                      {/* Tổ hợp action thực hiện */}
                      <div className="Date__action">
                        <Button className='Action__btn Btn__cancel' onClick={this.handleCloseModal}>Hủy Bỏ</Button>
                        <Button 
                          className='Action__btn Btn__confirm' 
                          onClick={this.handleCreateAppointment} 
                          disabled={!idGroupChoose}>
                            Tạo Cuộc Hẹn
                        </Button>
                      </div>
                                            
                    </div>
                  </div>
                </React.Fragment>
              )
            }
          </Fade>
        </Modal>)}

        {/* Modal Thông Báo Lỗi */}
        { openNotificationModal && 
          <NotificationModal 
            success={success} 
            error={error} 
            history={history}
            typeCheck='CreateDate'
            openNotificationModal={openNotificationModal}
            setDisableNotificationModal={this.setDisableNotificationModal}
          />
        }
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  openAddDateModal: selectors.getOpenAddDateModal(state),
  chooseMarker: selectors.getChooseMarker(state),
});

const mapDispatchToProps = dispatch => ({
  setDisableAddDateModal: () => dispatch(actions.setDisableAddDateModal()),
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateDateModal);