import React from 'react';
import './Header.scss';

// import fontAwesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons'
import { faFacebookF, faYoutube } from '@fortawesome/free-brands-svg-icons'

const Header = () => {
  return (
    <React.Fragment>
      <header className="Header">
        {/* Information */}
        <div className="Header__Information">
          <a className="Information__email" href="mailto: driverteam6@gmail.com">
            <FontAwesomeIcon className="Email__icon" icon={faEnvelope} />
            <span className="Email__text">govngroup@gmail.com</span>
          </a>
          <div className="Information__social">
            <a className="social__icon" href="https://www.facebook.com/GoVNApp"><FontAwesomeIcon icon={faFacebookF}/></a>
            <a className="social__icon icon--margin" href="https://www.youtube.com/channel/UCLVT3ql_8_fczjeyEqNaeSQ/featured?view_as=public"><FontAwesomeIcon icon={faYoutube}/></a>
          </div>
        </div>
      </header>
    </React.Fragment>
  )
}

export default Header;