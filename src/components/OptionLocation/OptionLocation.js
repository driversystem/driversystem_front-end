import React, { useState } from 'react'

import './OptionLocation.scss';

// import material-ui
import { LocalGasStation, LocalParking, DeleteForever } from '@material-ui/icons';
import { Button } from '@material-ui/core';

import DeleteMarkerDropdown from 'components/DeleteMarkerDropdown';

const OptionLocation = (props) => {

  const [openDropdown, setDeleteDropdown] = useState(false);

  const openDeleteMarkerDropDown = (e) => {setDeleteDropdown(!openDropdown)};

  const handleClickGasLocation = (e) => {
    e.preventDefault();
    const { onClickOptionLocation } = props;
    onClickOptionLocation('gas_station');
  }

  const handleClickParkingLocation = (e) => {
    e.preventDefault();
    const { onClickOptionLocation } = props;
    onClickOptionLocation('parking');
  }

  const handleOpenDelete = (e) => {
    e.preventDefault();
    openDeleteMarkerDropDown();
  }

  return (
    <React.Fragment>
      <div className="OptionLocation">
        {/* Icon Trạm Xăng */}
        <Button className="Each__icon" name="gas_station" onClick={handleClickGasLocation}>
          <div className="Icon__circle gasStation">
            <LocalGasStation className='Icon'/>
          </div>
          <div className="Icon__description">Trạm xăng</div>
        </Button>
        {/* Icon bãi đỗ xe */}
        <Button className="Each__icon" name="parking" onClick={handleClickParkingLocation}>
          <div className="Icon__circle parking">
            <LocalParking className='Icon'/>
          </div>
          <div className="Icon__description">Bãi gửi xe</div>
        </Button>
        {/* Icon xóa địa điểm */}
        <Button className="Each__icon" name="delete" onClick={handleOpenDelete}>
          <div className="Icon__circle delete">
            <DeleteForever className='Icon'/>
          </div>
          <div className="Icon__description">Xóa Địa Điểm</div>
        </Button>

        {openDropdown && <DeleteMarkerDropdown setDeleteDropdown={setDeleteDropdown}/>}
      </div>
    </React.Fragment>
  )
}

export default OptionLocation;