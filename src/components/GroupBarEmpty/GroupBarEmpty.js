import React from 'react'
import './GroupBarEmpty.scss';
import { Link } from 'react-router-dom'

// Minor components
import GroupIcon from 'components/GroupIcon';

// import images and icons
import plus from 'assets/png/plus.png'

class GroupBarEmpty extends React.Component {
    render() {
        return (
            <div className='emptyBarWrapper'>
                <div className='groupSelection'>
                    <GroupIcon
                            isSelected = {false}
                            image = {plus}
                            handleClick = {(e) => this.props.newGroup()}/>
                </div>
                <div className='groupWrapper'>
                    <div className='wrapAgain'>
                        Nếu bạn là thành viên của ít nhât một hội nhóm, thông tin nhóm sẽ được hiển thị ở đây
                    </div>
                    <Link
                        className='btnBack'
                        to="/" >
                        Trở về
                    </Link>
                </div>
            </div>
        )
    }
}

export default GroupBarEmpty;