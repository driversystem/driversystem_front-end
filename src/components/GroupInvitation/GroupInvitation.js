import React from 'react';
import './GroupInvitation.scss';

import Button from '@material-ui/core/Button';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';

import avatar from 'assets/png/avatar.png';
import { Avatar } from '@material-ui/core';

const GroupInvitation = (props) => {
    return (
        <ListItem className='invitation'>
            <ListItemAvatar>
                <Avatar src={avatar} />
            </ListItemAvatar>
            <ListItemText primary={props.user.name} secondary={props.user.email} />
            <Button 
                color="secondary" 
                onClick={props.onClick}
                size='small' 
                variant="contained">
                Hủy
            </Button>
        </ListItem>
    )
}

export default GroupInvitation;