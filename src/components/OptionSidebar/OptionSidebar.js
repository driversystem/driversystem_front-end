import React, { Component } from 'react'

import './OptionSidebar.scss';

// import material-ui
import { Close } from '@material-ui/icons';
import { Switch, IconButton, Tooltip } from '@material-ui/core';

// Import Redux
import { connect } from 'react-redux';
import actions from 'actions';
import selectors from 'selectors';

class OptionSidebar extends Component {

  handleCloseSidebar = (e) => {
    // Đóng Side bar
    const { setOpenSidebar } = this.props;
    setOpenSidebar(false);
  }

  handleTrafficSwitchChange = (e) => {
    // Đóng Mở Traffic Layer
    const { setOpenTrafficLayer, openTrafficLayer } = this.props;
    setOpenTrafficLayer(!openTrafficLayer);
  }

  render() {
    const { openSidebar, openTrafficLayer } = this.props;
    return (
      <React.Fragment>
        { openSidebar && 
          ( <div className="OptionSidebar">
              <div className="Background" onClick={this.handleCloseSidebar} />
              <div className="Sidebar">
                {/* Logo and back */}
                <div className="Logo__Back">
                  <img className='App__Logo' src='/assets/ico/icon_logo_header_green.ico' alt='App logo'/>
                  <Tooltip title="Đóng" aria-label="close">
                    <IconButton className="Each__icon" name="close" onClick={this.handleCloseSidebar}>
                      <Close className='Icon'/>
                    </IconButton>
                  </Tooltip>
                </div>

                {/* divider */}
                <hr className="divider" />

                {/* Switch Traffic Layer */}
                <div className="Each__Switch">
                  <div className="Title">Hiển thị tình trạng giao thông</div>
                  <Switch
                    className="Switch"
                    checked={openTrafficLayer}
                    onChange={this.handleTrafficSwitchChange}
                    name="traffic"
                    color="primary"
                    inputProps={{ 'aria-label': 'primary checkbox' }}
                  />
                </div>

              </div>
            </div>
          )
        }
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  openSidebar: selectors.getOpenSidebar(state),
  openTrafficLayer: selectors.getOpenTrafficLayer(state),
});

const mapDispatchToProps = dispatch => ({
  setOpenSidebar: openSidebar => dispatch(actions.setOpenSidebar(openSidebar)),
  setOpenTrafficLayer: openTrafficLayer => dispatch(actions.setOpenTrafficLayer(openTrafficLayer)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OptionSidebar);
