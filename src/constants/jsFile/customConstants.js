export const maximumAvatarSize = {
  INDIVIDUAL: 3 * 1024 * 1024,
  GROUP: 4 * 1024 * 1024,
};