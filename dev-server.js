const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');

const webpack = require('webpack');
const config = require('./webpack.config.dev');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');

const app = express();
const compiler = webpack(config);

app.use(webpackDevMiddleware(compiler, {
  logLevel: 'warn',
	publicPath: config.output.publicPath,
	watchOptions: {
		aggregateTimeout: 300,
		poll: true
	}
}));

app.use(webpackHotMiddleware(compiler));

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.use('/assets', express.static(path.resolve(__dirname, 'assets')));

app.get('/', (req, res) => {
	res.sendFile(path.resolve(__dirname, 'src/index.html'));
});

app.get('*', (req, res) => {
	res.sendFile(path.resolve(__dirname, 'src/index.html'));
});

// Serve the files on port 5000.
app.listen(5000, function () {
  console.log('Example app listening on port 5000!\n');
});